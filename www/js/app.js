

angular.module('FieldEdge', ['ionic', 'FieldEdge.services', 'FieldEdge.controllers'])

//default function to include default features of ionic
.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if(window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        
    setTimeout(function() {
        navigator.splashscreen.hide();
    }, 2000);
    });
})

//to configure the navigation of pages and invoke corresponding controller
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    //to navigate to login page
    .state('login', {
        url: "/login",
        templateUrl: "views/login/Login.html",
        controller: 'LoginController'
    })

    //this state is for sidebar, it is an abstract state, so it cannot be directly called
    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "views/login/Menu.html",
        controller: 'AppCtrl'
    })

    .state('app.rolesPage', {
        url: "/rolesPage",
        views: {
          'menuContent' :{
            templateUrl: "views/login/Roles.html",
            controller: 'RolesController'
          }
        }
    })

    /*-----------------------------------------------------------------------------------------------------------------------------------------
        the states below are the loaded in the view named "menuContent" which is defined in Menu.html
        they are like child views of 'app' state, so every state below is defined as app.<stateName>
    --------------------------------------------------------------------------------------------------------------------------------------------*/

    //to navigate to homepage, displayed after login
    .state('app.homepage', {
        url: "/homepage",
        views: {
          'menuContent' :{
            templateUrl: "views/login/Home.html",
            controller: 'HomePageController'
          }
        }
    })

    /*-----------------------------------Relationship Module-----------------------------------*/

    /*to navigate to list page of relationship module (lead/prospect/customer).
    url consist of parameter 'category' which stores value LEAD/PROSPECT/CUSTOMER which is passed further to get corresponding list
    */
    .state('app.customer', {
      url: "/customer/{category}",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/CustomerList.html",
          controller: 'CustomerPageController'
        }
      }
    })

    /*to navigate to details page of relationship module.
    this page is displayed when user selects any lead/prospect/customer from the list
    */
    .state('app.customerDetails', {
      url: "/customerDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/CustomerDetail.html",
          controller: 'CustomerDetailController'
        }
      }
    })

    //to navigate to add lead/prospect/customer page
    .state('app.addCustomer', {
      url: "/addCustomer",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/AddNewRelationship.html",
          controller: 'AddRelationshipController'
        }
      }
    })

    //to navigate to edit lead/prospect/customer page
    .state('app.editCustomer', {
      url: "/editCustomer",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/EditRelationship.html",
          controller: 'EditRelationshipController'
        }
      }
    })

    //to navigate to page containing the list of addresses/contacts of particular lead/prospect/customer
    .state('app.addressContactList', {
      url: "/addressContactList",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/AddressContactList.html",
          controller: 'AddressContactListController'
        }
      }
    })

    //to navigate to add address/contact page
    .state('app.addAddressContact', {
      url: "/addAddressContact",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/AddAddresscContact.html",
          controller: 'AddAddressContactController'
        }
      }
    })

    //to navigate to details page of address/contact when particular address/contact is selected from the address/contact list
    .state('app.addressContactDetail', {
      url: "/addressContactDetail",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/AddressContactDetail.html",
          controller: 'AddressContactDetailController'
        }
      }
    })

    //to navigate to edit address/contact page
    .state('app.editAddressContact', {
      url: "/editAddressContact",
      views: {
        'menuContent' :{
          templateUrl: "views/relationship/EditAddressContact.html",
          controller: 'EditAddressContactController'
        }
      }
    })

    /*---------------------------------Relationship Module end---------------------------------*/

    /*-----------------------------------Transaction Module------------------------------------*/
    
    /*to navigate to list page of transaction module (opportunity/quote/order).
    url consist of parameter 'category' which stores value OPPORTUNITY/QUOTE/ORDER which is passed further to get corresponding list
    */
    .state('app.transaction', {
      url: "/transaction/{category}",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/Transaction.html",
          controller: 'TransactionController'
        }
      }
    })

    //to navigate to opportunity details page, when user selects any opportunity from opportunity list
    .state('app.transactionDetails', {
      url: "/transactionDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/TransactionDetails.html",
          controller: 'TransactionDetailsController'
        }
      }
    })

    //to navigate to quote details page, when user selects any quote from quote list
    .state('app.quoteDetails', {
      url: "/quoteDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/QuoteDetail.html",
          controller: 'QuoteDetailsController'
        }
      }
    })

    //to navigate to order details page, when user selects any order from order list
    .state('app.orderDetails', {
      url: "/orderDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/OrderDetail.html",
          controller: 'OrderDetailsController'
        }
      }
    })

    /*to navigate to opportunity/quote/order list page of a particular lead/prospect/customer
    this navigation is done through relationship module, from customer details page
    */
    .state('app.customerTransactionList', {
      url: "/customerTransactionList",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/CustomerTransactionList.html",
          controller: 'CustomerTransactionListController'
        }
      }
    })

    //to navigate to add opportunity page
    .state('app.addOpportunity', {
      url: "/addOpportunity",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/AddOpportunity.html",
          controller: 'AddOpportunityController'
        }
      }
    })

    //to navigate to edit opportunity page
    .state('app.editTransaction', {
      url: "/editTransaction",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/EditOpportunity.html",
          controller: 'EditTransactionController'
        }
      }
    })

    //to navigate to add quote page
    .state('app.addQuote', {
      url: "/addQuote",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/AddQuote.html",
          controller: 'AddQuoteController'
        }
      }
    })

    //to navigate to edit quote page
    .state('app.editQuote', {
      url: "/editQuote",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/EditQuote.html",
          controller: 'EditQuoteController'
        }
      }
    })

    //to navigate to add order page
    .state('app.addOrder', {
      url: "/addOrder",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/AddOrder.html",
          controller: 'AddOrderController'
        }
      }
    })

    //to navigate to edit order page
    .state('app.editOrder', {
      url: "/editOrder",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/EditOrder.html",
          controller: 'EditOrderController'
        }
      }
    })

    //to navigate to item details page, we navigate to this page from opportunity/quote/order details page
    .state('app.itemDetails', {
      url: "/itemDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/ItemDetails.html",
          controller: 'ItemDetailsController'
        }
      }
    })

    //to navigate to add item page, we navigate to this page when we add a new item
    .state('app.addItem', {
      url: "/addItem",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/AddItem.html",
          controller: 'AddItemController'
        }
      }
    })

    //to navigate to edit item page, we navigate to this page when we add/edit opportunity/quote/order(second page in add/edit sequence)
    .state('app.editItem', {
      url: "/editItem",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/EditItem.html",
          controller: 'EditItemController'
        }
      }
    })

    //to navigate to summary page, we navigate to this page when we add/edit opportunity/quote/order(last page in add/edit sequence)
    .state('app.summary', {
      url: "/summary",
      views: {
        'menuContent' :{
          templateUrl: "views/transaction/Summary.html",
          controller: 'SummaryController'
        }
      }
    })
    /*-------------------------------------Transaction Module end-------------------------------------*/

    /*-----------------------------------Support Management Module------------------------------------*/

    //to navigate to page containing the list of cases
    .state('app.caseList', {
      url: "/caseList/{category}",
      views: {
        'menuContent' :{
          templateUrl: "views/support/CaseList.html",
          controller: 'CaseListController'
        }
      }
    })

	//to navigate to page containing the details of case
    .state('app.caseDetail', {
      url: "/caseDetail",
      views: {
        'menuContent' :{
          templateUrl: "views/support/CaseDetails.html",
          controller: 'CaseDetailController'
        }
      }
    })

    //to navigate to add case page
    .state('app.caseAdd', {
      url: "/caseAdd",
      views: {
        'menuContent' :{
          templateUrl: "views/support/AddCase.html",
          controller: 'CaseAddController'
        }
      }
    })

    //to navigate to edit case page
    .state('app.caseEdit', {
      url: "/caseEdit",
      views: {
        'menuContent' :{
          templateUrl: "views/support/EditCase.html",
          controller: 'CaseEditController'
        }
      }
    })

    //to navigate to page containing the list of message, we navigate to this page through case details page
    .state('app.messageList', {
      url: "/messageList",
      views: {
        'menuContent' :{
          templateUrl: "views/support/MessageList.html",
          controller: 'MessageListController'
        }
      }
    })

    //to navigate to message details page
    .state('app.messageDetail', {
      url: "/messageDetail",
      views: {
        'menuContent' :{
          templateUrl: "views/support/MessageDetail.html",
          controller: 'MessageDetailController'
        }
      }
    })

    //to navigate to add message page
    .state('app.messageAdd', {
      url: "/messageAdd",
      views: {
        'menuContent' :{
          templateUrl: "views/support/MessageAdd.html",
          controller: 'MessageAddController'
        }
      }
    })

    //to navigate to edit message page
    .state('app.messageEdit', {
      url: "/messageEdit",
      views: {
        'menuContent' :{
          templateUrl: "views/support/EditMessage.html",
          controller: 'MessageEditController'
        }
      }
    })
    /*---------------------------------Support Management Module end---------------------------------*/

    /*------------------------------------------HR Module--------------------------------------------*/

    //to navigate to page containing list of weeks
    .state('app.Timesheet', {
      url: "/Timesheet",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/Timesheet.html",
          controller: 'TimesheetController'
        }
      }
    })

    //to navigate to page containing list of days in week
    .state('app.TimesheetList', {
      url: "/TimesheetList",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/TimesheetList.html",
          controller: 'TimesheetListController'
        }
      }
    })

    //to navigate to page containing list of hours of work
    .state('app.TimesheetRecord', {
      url: "/TimesheetRecord",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/TimesheetRecord.html",
          controller: 'TimesheetRecordController'
        }
      }
    })

    //to navigate to page containing details of the work hours
    .state('app.TimesheetDetails', {
      url: "/TimesheetDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/TimesheetDetails.html",
          controller: 'TimesheetDetailsController'
        }
      }
    })

    //to navigate to add timesheet page
    .state('app.AddTimesheet', {
      url: "/AddTimesheet",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/AddTimesheet.html",
          controller: 'AddTimesheetController'
        }
      }
    })

    //to navigate to edit timesheet page
    .state('app.EditTimesheet', {
      url: "/EditTimesheet",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/EditTimesheet.html",
          controller: 'EditTimesheetController'
        }
      }
    })
  
    //to navigate to page containing list of last 5 timesheet
    .state('app.Last5Timesheet', {
      url: "/Last5Timesheet",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/Last5Timesheet.html",
          controller: 'Last5TimesheetController'
        }
      }
    })

    //to navigate to page containing list of expenses
    .state('app.Expense', {
      url: "/Expense",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/Expense.html",
          controller: 'ExpenseController'
        }
      }
    })

    //to navigate to page containing list of top expenses
    .state('app.TopExpense', {
      url: "/TopExpense",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/TopExpense.html",
          controller: 'TopExpenseController'
        }
      }
    })

    //to navigate to page containing details of expense
    .state('app.ExpenseDetails', {
      url: "/ExpenseDetails",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/ExpenseDetails.html",
          controller: 'ExpenseDetailsController'
        }
      }
    })

    //to navigate to page containing list of items in expense
    .state('app.ExpenseList', {
      url: "/ExpenseList",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/ExpenseList.html",
          controller: 'ExpenseListController'
        }
      }
    })
  
    //to navigate edit expense page
    .state('app.EditExpense', {
      url: "/EditExpense",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/EditExpense.html",
          controller: 'EditExpenseController'
        }
      }
    })

    //to navigate to page where we can edit items in expense
    .state('app.ExpenseListEdit', {
      url: "/ExpenseListEdit",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/ExpenseListEdit.html",
          controller: 'ExpenseListEditController'
        }
      }
    })

    //to navigate to page to edit the item in expense, when the item is selected from the list
    .state('app.ExpenseListChange', {
      url: "/ExpenseListChange",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/ExpenseListChange.html",
          controller: 'ExpenseListChangeController'
        }
      }
    })

    //to navigate to page to add item in expense
    .state('app.EditExpenseListItem', {
      url: "/EditExpenseListItem",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/EditExpenseListItem.html",
          controller: 'EditExpenseListItemController'
        }
      }
    })

    //to navigate to add expense page
    .state('app.ExpenseAdd', {
      url: "/ExpenseAdd",
      views: {
        'menuContent' :{
          templateUrl: "views/hr/AddExpense.html",
          controller: 'ExpenseAddController'
        }
      }
    })

    .state('app.CallLogs', {
      url: "/CallLogs",
      views: {
        'menuContent' :{
          templateUrl: "views/activity/CallLogs.html",
          controller: 'CallLogsController'
        }
      }
    });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});


