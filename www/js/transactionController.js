/* Transaction Colntroller Start */
/*==============================================================================================================================================================*/

app.controller('TransactionController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, AppFactory,$filter ,$ionicPopup ,$ionicScrollDelegate, $rootScope) 
{
    if($stateParams.category == "LAST 5 QUOTES" || $stateParams.category == "LAST 5 ORDERS")
    {
        $rootScope.sideMenuEnabled = false;
    }
    else
    {  
        $rootScope.sideMenuEnabled = true;
    }

    $scope.opportunityListArray=[];
    $scope.quoteListArray=[];
    $scope.orderListArray=[];
    //used in server-search
    $scope.arrayLength=0;
    $scope.recordCount=0;

    /* Design Section START */
    $scope.buttons = [];
    $scope.sortText="";
    $scope.flag = 1;

       /*Clear Search Box*/
     $scope.clearSearch = function () {
        $scope.searchString="";
    }

    /* SIDE MENU SECTION -- HIDE OR UNHIDE*/
    console.log("customer controller appearing");
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* PAGE CATEGORY FOR --PAGE TITLE*/
    
    $scope.pageTitle = $stateParams.category;

    if($scope.pageTitle == "OPPORTUNITY")
    {
        $scope.title = "OPPORTUNITIES";
    }
    else if($scope.pageTitle == "ORDER")
    {
        $scope.title = "ORDERS";
    }
    else if($scope.pageTitle == "QUOTE")
    {
        $scope.title = "QUOTES";
    }
    else
    {
        $scope.title = $stateParams.category;
    }

    //console.log("Page Title : "+$scope.pageTitle);
    AppFactory.setCategory($stateParams.category);

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function()
    {
        if($stateParams.category == "OPPORTUNITY")
        {
            $state.go('app.transactionDetails');

        }else if($stateParams.category == "QUOTE" || $stateParams.category == "LAST 5 QUOTES")
        {
            AppFactory.setType("");
            $state.go('app.quoteDetails');

        }else if($stateParams.category == "ORDER" || $stateParams.category == "LAST 5 ORDERS")
        {
            $state.go('app.orderDetails');
        }
    }
   
    /***********SORTING FUNCTION***********************/
    var orderBy = $filter('orderBy');
    $scope.order = function(predicate) {        
    $scope.listArray = orderBy($scope.listArray, predicate);
    };
 
    /*****************************************/

    if(AppFactory.getCategory() == "OPPORTUNITY")
    {
        $scope.buttons = [
        { text: 'By Company Name', },
        { text: 'By Expected Close' },
        { text: 'By Projected Total' }];
    }
    else
    {
        $scope.buttons = [
        { text: 'By Name', },
        { text: 'By Status' },
        { text: 'By Amount' }];
    }

    $scope.show = function() {
        // Show the action sheet
        $ionicActionSheet.show({
            titleText: 'Sort By',
            buttons: $scope.buttons,
            /* Cancel Button */
            cancelText: 'Cancel',
            cancel: function() 
            {
                console.log('CANCELLED');
            },
            /* Button Click Functionality */
            buttonClicked: function(index) {
                /* Sorting By Company Name*/
                if (index==0) 
                {
                    
                    if($scope.pageTitle == "OPPORTUNITY")
                    {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to sort by Company Name?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    $scope.sortText="Company Name";
                                    $scope.loadSortedData();
                                    //$scope.order('companyName');
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    }
                    else
                    {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to sort by Name?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    $scope.sortText="Name";
                                    $scope.loadSortedData();
                                    //$scope.order('Name');
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    }

                   
                };

                /* Sorting By Expected Close*/
                if (index==1) 
                {
                    
                    if($scope.pageTitle == "OPPORTUNITY")
                    {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to sort by Expected Close?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    $scope.sortText="Expected Close";
                                    $scope.loadSortedData();
                                    //$scope.order('expectedClose');
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    }
                    else
                    {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to sort by Status?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    $scope.sortText="Status";
                                    $scope.loadSortedData();
                                    //$scope.order('Status');
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    }
                    
                };

                /* Sorting By Projected Total*/
                if (index==2) 
                {
                   
                    if($scope.pageTitle == "OPPORTUNITY")
                    {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to sort by Projected Total?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    $scope.sortText="Projected Total";
                                    $scope.loadSortedData();
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    }
                    else
                    {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to sort by Amount?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    $scope.sortText="Amount";
                                    //$scope.order('Amount');
                                    $scope.loadSortedData();
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    }
                   
                };

                return true;
            }   //buttonClicked 
        });
    };

     /*  Design Section END*/

    // SHIVAJI TANPURE

    //Function for setting Id of clicked item into factory
    $scope.setListItemId=function(id) {
        AppFactory.setItemId(id);
        if($scope.pageTitle == "OPPORTUNITY")
        {
            $state.go('app.transactionDetails');
        }

        else if($scope.pageTitle == "QUOTE" || $scope.pageTitle == "LAST 5 QUOTES")
        {
            $state.go('app.quoteDetails');
        }
        else if($scope.pageTitle == "ORDER" || $scope.pageTitle == "LAST 5 ORDERS")
        {
            $state.go('app.orderDetails');
        }
    }//end of setListItemId()

    //Function for displaying respected list data when page init
    $scope.listPage=function() {

        if($scope.pageTitle == "OPPORTUNITY")
        {
            $scope.listArray=$scope.opportunityListArray;//assign opportunity array to listarray            
        }
        if($scope.pageTitle == "QUOTE" || $scope.pageTitle == "LAST 5 QUOTES")
        {
            $scope.listArray=$scope.quoteListArray;//assign quoteListArray  to listarray            
        }

        if($scope.pageTitle == "ORDER" || $scope.pageTitle == "LAST 5 ORDERS")
        {
            $scope.listArray=$scope.orderListArray;//assign orderListArray to listarray            
        }

    } //end of listPage

    $scope.loadSortedData = function() 
    {
        
        $scope.mainArray = [];
        $scope.showMessage = false;
        $ionicScrollDelegate.scrollTop();

        console.log("Page :"+$scope.pageTitle);
        if($scope.pageTitle == "OPPORTUNITY")
        {
            $scope.opportunityListArray = [];
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });   

            var parameters={
                "id":"customsearch_aavz_28",
                "customer_type":"opportunity",
                "search":"",    
                "searchtype":"opprtnty",
                "customerid":"",
                "sortfieldname":$scope.sortText
            };
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=95&deploy=1",null,parameters,function(results) { //get result from json
                //console.log("Response Data:"+JSON.stringify(results));
                $scope.mainArray=results;
                for(i in $scope.mainArray)
                {
                    var mainObj = $scope.mainArray[i]; //get single object of array
                    //console.log(JSON.stringify(mainObj));
                    for(key in mainObj)
                    {                          
                        var singleObject=mainObj[key];  //create a single object by using key
                        singleObject.id=key;//Add key as attribute in singleObj
                        var amt=singleObject['Projected Total'];
                        singleObject['Projected Total']=parseFloat(amt);
                        //console.log("singleObject:"+JSON.stringify(singleObject));
                        $scope.opportunityListArray.push(singleObject); //insert data into array of objects
                    } //end of key forloop
                }//end of mainArray
                //console.log(JSON.stringify($scope.opportunityListArray)); //display array of object
                $scope.arrayLength=$scope.opportunityListArray.length;//get opportunity array length used for server-search
                $ionicLoading.hide();
            });
        }

        if($scope.pageTitle == "QUOTE")
        {
            $scope.quoteListArray = [];
            //Qoute List start
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
                });   

            var parameters={
                "id":"customsearch_aavz_25",
                "noofrecords":"100",  
                "customer_type":"estimate",
                "sortfieldname":$scope.sortText
            };
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=120&deploy=1",null,parameters,function(results) { //get result from json

                //console.log("Response Data:"+JSON.stringify(results));
                $scope.mainArray=results;

                for(item in $scope.mainArray)
                {
                    var innerObject=$scope.mainArray[item];
                    //console.log(JSON.stringify(mainArray[item]));
                    for(key in innerObject)
                    {
                        var singleObj=innerObject[key];
                        singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                        var amt=singleObj['Amount'];
                        singleObj['Amount']=parseFloat(amt);
                        console.log("singleObject:"+JSON.stringify(singleObj));
                        $scope.quoteListArray.push(singleObj);    
                    }
                }
                //console.log(JSON.stringify($scope.quoteListArray));
                $scope.arrayLength=$scope.quoteListArray.length;//get quoteListArray length used for server-search 
                $ionicLoading.hide();
            }); //end of sendHttpRequest

        }

        if($scope.pageTitle == "ORDER")
        {
            $scope.orderListArray = [];
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
                });   

            var parameters={
                "id":"customsearch_aavz_20",
                "searchtype":"SalesOrd",
                "noofrecords":"100",   
                "customer_type":"salesorder",
                "sortfieldname":$scope.sortText
            };     
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",null,parameters,function(results) { //get result from json

                $scope.mainArray=results;

                for(item in $scope.mainArray)
                {
                    var innerObject=$scope.mainArray[item];
                    //console.log(JSON.stringify(mainArray[item]));
                    for(key in innerObject)
                    {
                        var singleObj=innerObject[key];
                        singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                        var amt=singleObj['Amount'];
                        singleObj['Amount']=parseFloat(amt);//changed for sorting purpose
                        $scope.orderListArray.push(singleObj);    
                    }
                }
                //console.log(JSON.stringify($scope.orderListArray));
                $scope.arrayLength=$scope.orderListArray.length;//get orderListArray length used for server-search  
                $ionicLoading.hide();
            }); //end of sendHttpRequest

        }

        else if($scope.pageTitle == "LAST 5 ORDERS")
        {
            // to store the objects where case data is present
            $scope.orderListArray=[];
            // function will call on clicking refresh button..
            $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                    });
            // parameters to send with the requrest of getting data of case list
            var parameters ={ 
                "id":"customsearch_aavz_21",
                "customer_type":"salesorder",
                "sortfieldname":$scope.sortText
            };
                            
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=118&deploy=1",null,parameters,function(results)
            {
                for(mainArray in  results)
                {
                    obj =  results[mainArray];                                    
                    for(objKey in obj)
                    {
                        var objValue = obj[objKey];
                        objValue.id=objKey;
                        // pushing the case data into the valuearray
                        $scope.orderListArray.push(objValue);
                    } 
                }
                console.log(" result of top 5 order list ="+JSON.stringify($scope.orderListArray));
                // to hide the loading indicator 
                $ionicLoading.hide(); 
            });
        }

        else if($scope.pageTitle == "LAST 5 QUOTES")
        {   
            $scope.quoteListArray = [];
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
                });              

            var parameters={
                "id":"customsearch_aavz_29",
                "customer_type":"estimate",
                "sortfieldname":$scope.sortText
                };

            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=124&deploy=1",null,parameters,function(results) { //get result from json

                //console.log("Response Data:"+JSON.stringify(results));
                var mainArray=results;

                for(item in mainArray)
                {
                    var innerObject=mainArray[item];
                    //console.log(JSON.stringify(mainArray[item]));
                    for(key in innerObject)
                    {
                        var singleObj=innerObject[key];
                        singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                        $scope.quoteListArray.push(singleObj);    
                    }
                }
                console.log(JSON.stringify($scope.quoteListArray)); 
                $ionicLoading.hide();
            }); //end of sendHttpRequest

        }
        $scope.listPage();
    }//end of loadNextData()

    $scope.mainArray = [];
    $scope.showMessage = false;
    $scope.loadNextData1 = function() 
    {
        //checking whether index is between last 5
        /*if(index > ($scope.listArray.length-5) && index <=($scope.listArray.length-1))
        {
            //if restlet is not called for the range
            if($scope.flag==1)
            {*/

                if($scope.pageTitle == "OPPORTUNITY")
                {
                    $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                    });

                    var temp= $scope.listArray.length;
                    var parameters={
                                "id":"customsearch_aavz_28",
                                "customer_type":"opportunity",
                                "recordcount":temp,
                                "search":"",    
                                "searchtype":"opprtnty",
                                "customerid":""
                                };

                    //Request for get data
                    AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=95&deploy=1",null,parameters,function(results) { //get result from json
                        //console.log("Response Data:"+JSON.stringify(results));
                        $scope.mainArray=results;

                        if($scope.mainArray.length == 0)
                        {
                            $scope.showMessage = true;
                        }

                        for(i in $scope.mainArray)
                        {
                            var mainObj = $scope.mainArray[i]; //get single object of array
                            //console.log(JSON.stringify(mainObj));
                            for(key in mainObj)
                            {  
                                //console.log(JSON.stringify(mainObj[key])); //get single inner object / record

                                var singleObject=mainObj[key];  //create a single object by using key

                                singleObject.id=key;//Add key as attribute in singleObj

                                $scope.opportunityListArray.push(singleObject); //insert data into array of objects
                            } //end of key forloop
                        }//end of mainArray
                        //console.log(JSON.stringify($scope.opportunityListArray)); //display array of object
                        $ionicLoading.hide();
                    });

                    $scope.flag=0;
                }
                else if($scope.pageTitle == "QUOTE")
                {
                    $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                    });

                    var temp= $scope.listArray.length;
                    var parameters={
                            "id":"customsearch_aavz_25",
                            "noofrecords":"100",
                            "recordcount":temp,  
                            "customer_type":"estimate"
                                    };

                    //Request for get data
                    AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=120&deploy=1",null,parameters,function(results) { //get result from json

                        //console.log("Response Data:"+JSON.stringify(results));
                        $scope.mainArray=results;

                        for(item in $scope.mainArray)
                        {
                            var innerObject=$scope.mainArray[item];
                            //console.log(JSON.stringify(mainArray[item]));
                            for(key in innerObject)
                            {
                                var singleObj=innerObject[key];
                                singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                                $scope.quoteListArray.push(singleObj);    
                            }
                        }
                        //console.log(JSON.stringify($scope.quoteListArray)); 
                        $ionicLoading.hide();

                    }); //end of sendHttpRequest
                }
                else if($scope.pageTitle == "ORDER")
                {
                    $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                    });

                    var temp= $scope.listArray.length;

                    var parameters={
                    "id":"customsearch_aavz_20",
                    "searchtype":"SalesOrd",
                    "noofrecords":"100", 
                    "recordcount":temp,  
                    "customer_type":"salesorder"
                    };
                     
                    //Request for get data
                    AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",null,parameters,function(results) { //get result from json
                     
                        $scope.mainArray=results;
                        for(item in $scope.mainArray)
                        {
                            var innerObject=$scope.mainArray[item];
                            //console.log(JSON.stringify(mainArray[item]));
                            for(key in innerObject)
                            {
                                var singleObj=innerObject[key];
                                singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                                $scope.orderListArray.push(singleObj);    
                            }
                        }
                        //console.log(JSON.stringify($scope.orderListArray)); 
                        $ionicLoading.hide();
                    }); //end of sendHttpRequest
                }
                
            /*}
        }
        else
        {
             $scope.flag=1;
        }*/
    }//end of loadNextData()

    /**************SEARCH FROM SERVER START******************************************/
    
    
    //Code for Search data filter from server
    $scope.newflag="false"; //check when to display message
    $scope.searchString=""; // text you want search
    $scope.listflag =1; //initially  check item is present in list or not
    $scope.counter=1;  //count if the item is found in existing list or not   
    $scope.messageFlag="false";
    $scope.$watch("searchString", function(query){
        $scope.counter = $filter("filter")($scope.listArray, query).length;             
        if($scope.listflag == 1) 
        {
            $scope.listflag = 0;  
            $scope.newflag="false";       
        }
        else if($scope.counter==0 && $scope.listflag ==0 && $scope.pageTitle != "LAST 5 ORDERS" && $scope.pageTitle != "LAST 5 QUOTES")
        {             
            $scope.newflag="true";
            $scope.messageFlag="false";           
        } 
        else if($scope.counter == 0 && $scope.listflag == 0 && ($scope.pageTitle == "LAST 5 ORDERS" || $scope.pageTitle == "LAST 5 QUOTES"))
        {    
            $scope.message="No results found.";  
            $scope.newflag="true";             
        }
        else
        {
            $scope.newflag="false";
            $scope.messageFlag="false";
            $ionicScrollDelegate.scrollTop();
        }

        if($scope.searchString.length == 0)
        {
            console.log("Count : "+$scope.counter+" search string:"+$scope.searchString);
            $scope.counter = 0;
        }

        if($scope.recordCount>0)
        {                
            $scope.listArray.splice($scope.arrayLength,$scope.recordCount);
        }

    });//end of $watch()

    //Calling web services for server search
    $scope.mySearchList=function(){
        if($scope.pageTitle=="OPPORTUNITY")
        {
            $scope.recordCount=0;
            //display loading indicator
            $ionicLoading.show({
                            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showdelay: 300
                            });
            
                var parameters={
                    "id":"customsearch_aavz_28",
                    "customer_type":"opportunity",                
                    "search":$scope.searchString,    
                    "searchkey":"Company Name"               
                             };
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=95&deploy=1",null,parameters,function(results) { //get result from json
                    //console.log("Response Data:"+results);
                    if(results.length ==0 )
                    {
                        $scope.message="No results found on server."; 
                        $scope.newflag="false";
                        $scope.messageFlag="true";                       
                    }
                    else
                    {
                       $scope.newflag="false";
                       $scope.messageFlag="false"; 
                       $scope.counter=results.length;
                    }
                    var mainArray=results;
                    for(i in mainArray)
                    {
                        var mainObj = mainArray[i]; //get single object of array
                        //console.log(JSON.stringify(mainObj));
                        for(key in mainObj)
                        { 
                        $scope.recordCount+=1; 
                            //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                            var singleObject=mainObj[key];  //create a single object by using key
                            singleObject.id=key;//Add key as attribute in singleObj
                            $scope.listArray.push(singleObject); //insert data into array of objects
                        } //end of key forloop
                    }//end of mainArray                           
                    $ionicLoading.hide();
            });//end of sendHttpRequest()
        }
        if($scope.pageTitle=="QUOTE")
        {   
            $scope.recordCount=0;
            //display loading indicator
            $ionicLoading.show({
                            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showdelay: 300
                            });          

                var parameters={
                    "id":"customsearch_aavz_25",
                    "customer_type":"estimate",                
                    "search":$scope.searchString,    
                    "searchkey":"Name"               
                             };

                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=120&deploy=1",null,parameters,function(results) { //get result from json
                    //console.log("Response Data:"+JSON.stringify(results));
                    if(results.length ==0 )
                    {
                        $scope.message="No results found on server.";
                        $scope.newflag="false";
                        $scope.messageFlag="true"; 
                    }
                    else
                    {
                        $scope.newflag="false";
                        $scope.messageFlag="false";
                        $scope.counter=results.length;  
                    }
                    var mainArray=results;
                    for(i in mainArray)
                    {
                        var mainObj = mainArray[i]; //get single object of array
                        //console.log(JSON.stringify(mainObj));
                        for(key in mainObj)
                        {  
                            $scope.recordCount+=1;
                            //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                            var singleObject=mainObj[key];  //create a single object by using key
                            singleObject.id=key;//Add key as attribute in singleObj
                            $scope.listArray.push(singleObject); //insert data into array of objects
                        } //end of key forloop
                    }//end of mainArray                           
                $ionicLoading.hide();
            });//end of sendHttpRequest()
        }

        if($scope.pageTitle=="ORDER")
        {   
            $scope.recordCount=0;
            //display loading indicator
            $ionicLoading.show({
                            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showdelay: 300
                            });               

                var parameters={
                    "id":"customsearch_aavz_20",
                    "customer_type":"salesorder",                
                    "search":$scope.searchString,    
                    "searchkey":"Name"               
                             };

                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",null,parameters,function(results) { //get result from json
                    //console.log("Response Data:"+JSON.stringify(results));
                    if(results.length ==0 )
                    {
                        $scope.message="No results found on server.";
                        $scope.newflag="false";
                        $scope.messageFlag="true";                         
                    }
                    else
                    {
                        $scope.newflag="false";
                        $scope.messageFlag="false"; 
                        $scope.counter=results.length;
                    }
                    var mainArray=results;
                    for(i in mainArray)
                    {
                        var mainObj = mainArray[i]; //get single object of array
                        //console.log(JSON.stringify(mainObj));
                        for(key in mainObj)
                        {  
                            $scope.recordCount+=1;
                            //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                            var singleObject=mainObj[key];  //create a single object by using key
                            singleObject.id=key;//Add key as attribute in singleObj
                            $scope.listArray.push(singleObject); //insert data into array of objects
                        } //end of key forloop
                    }//end of mainArray                            
                $ionicLoading.hide();
            });//end of sendHttpRequest()
        }
    }//end of mySearchList()
    /**************SEARCH FROM SERVER END******************************************/

    //LIST's start
   
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  */
    $scope.init = function ()
    {
        
        $scope.mainArray = [];
        $scope.showMessage = false;
        $ionicScrollDelegate.scrollTop();

        console.log("Page :"+$scope.pageTitle);
        if($scope.pageTitle == "OPPORTUNITY")
        {
            $scope.opportunityListArray = [];
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });   

            var parameters={
                "id":"customsearch_aavz_28",
                "customer_type":"opportunity",
                "search":"",    
                "searchtype":"opprtnty",
                "customerid":""
            };
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=95&deploy=1",null,parameters,function(results) { //get result from json
                //console.log("Response Data:"+JSON.stringify(results));
                $scope.mainArray=results;
                for(i in $scope.mainArray)
                {
                    var mainObj = $scope.mainArray[i]; //get single object of array
                    //console.log(JSON.stringify(mainObj));
                    for(key in mainObj)
                    {                          
                        var singleObject=mainObj[key];  //create a single object by using key
                        singleObject.id=key;//Add key as attribute in singleObj
                        var amt=singleObject['Projected Total'];
                        singleObject['Projected Total']=parseFloat(amt);
                        //console.log("singleObject:"+JSON.stringify(singleObject));
                        $scope.opportunityListArray.push(singleObject); //insert data into array of objects
                    } //end of key forloop
                }//end of mainArray
                //console.log(JSON.stringify($scope.opportunityListArray)); //display array of object
                $scope.arrayLength=$scope.opportunityListArray.length;//get opportunity array length used for server-search
                $ionicLoading.hide();
            });
        }

        if($scope.pageTitle == "QUOTE")
        {
            $scope.quoteListArray = [];
            //Qoute List start
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
                });   

            var parameters={
                "id":"customsearch_aavz_25",
                "noofrecords":"100",  
                "customer_type":"estimate"
            };
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=120&deploy=1",null,parameters,function(results) { //get result from json

                //console.log("Response Data:"+JSON.stringify(results));
                $scope.mainArray=results;

                for(item in $scope.mainArray)
                {
                    var innerObject=$scope.mainArray[item];
                    //console.log(JSON.stringify(mainArray[item]));
                    for(key in innerObject)
                    {
                        var singleObj=innerObject[key];
                        singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                        var amt=singleObj['Amount'];
                        singleObj['Amount']=parseFloat(amt);
                        console.log("singleObject:"+JSON.stringify(singleObj));
                        $scope.quoteListArray.push(singleObj);    
                    }
                }
                //console.log(JSON.stringify($scope.quoteListArray));
                $scope.arrayLength=$scope.quoteListArray.length;//get quoteListArray length used for server-search 
                $ionicLoading.hide();
            }); //end of sendHttpRequest

        }

        if($scope.pageTitle == "ORDER")
        {
            $scope.orderListArray = [];
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
                });   

            var parameters={
                "id":"customsearch_aavz_20",
                "searchtype":"SalesOrd",
                "noofrecords":"100",   
                "customer_type":"salesorder"
            };     
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",null,parameters,function(results) { //get result from json

                $scope.mainArray=results;

                for(item in $scope.mainArray)
                {
                    var innerObject=$scope.mainArray[item];
                    //console.log(JSON.stringify(mainArray[item]));
                    for(key in innerObject)
                    {
                        var singleObj=innerObject[key];
                        singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                        var amt=singleObj['Amount'];
                        singleObj['Amount']=parseFloat(amt);//changed for sorting purpose
                        $scope.orderListArray.push(singleObj);    
                    }
                }
                //console.log(JSON.stringify($scope.orderListArray));
                $scope.arrayLength=$scope.orderListArray.length;//get orderListArray length used for server-search  
                $ionicLoading.hide();
            }); //end of sendHttpRequest

        }

        else if($scope.pageTitle == "LAST 5 ORDERS")
        {
            // to store the objects where case data is present
            $scope.orderListArray=[];
            // function will call on clicking refresh button..
            $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                    });
            // parameters to send with the requrest of getting data of case list
            var parameters ={ 
                "id":"customsearch_aavz_21",
                "customer_type":"salesorder"
            };
                            
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=118&deploy=1",null,parameters,function(results)
            {
                for(mainArray in  results)
                {
                    obj =  results[mainArray];                                    
                    for(objKey in obj)
                    {
                        var objValue = obj[objKey];
                        objValue.id=objKey;
                        // pushing the case data into the valuearray
                        $scope.orderListArray.push(objValue);
                    } 
                }
                console.log(" result of top 5 order list ="+JSON.stringify($scope.orderListArray));
                // to hide the loading indicator 
                $ionicLoading.hide(); 
            });
        }

        else if($scope.pageTitle == "LAST 5 QUOTES")
        {   
            $scope.quoteListArray = [];
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
                });              

            var parameters={
                "id":"customsearch_aavz_29",
                "customer_type":"estimate"
                };

            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=124&deploy=1",null,parameters,function(results) { //get result from json

                //console.log("Response Data:"+JSON.stringify(results));
                var mainArray=results;

                for(item in mainArray)
                {
                    var innerObject=mainArray[item];
                    //console.log(JSON.stringify(mainArray[item]));
                    for(key in innerObject)
                    {
                        var singleObj=innerObject[key];
                        singleObj.id=key; //create new key with name quoteInternalId and form a single internal object
                        $scope.quoteListArray.push(singleObj);    
                    }
                }
                console.log(JSON.stringify($scope.quoteListArray)); 
                $ionicLoading.hide();
            }); //end of sendHttpRequest

        }
        $scope.listPage();
    }//end of init
    $scope.init(); //invoke init()
    
    //to back to home page from last 5 quotes/orders page
    $scope.goHome = function ()
    {
        $state.go('app.homepage');
    }
});

/* Transaction Controller End */
/*==============================================================================================================================================================*/

/* Transaction Details Colntroller Start */
/*==============================================================================================================================================================*/

app.controller('TransactionDetailsController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $state, $ionicActionSheet, AppFactory, $ionicPopup, $rootScope) 
{
    $scope.showPage = false;
    $rootScope.sideMenuEnabled = false;
    $scope.opportunityDataObj={};
    //section wise decalration
    $scope.headerArray=[];
    $scope.primaryInfoArray=[];
    $scope.forecastingArray=[];
    //section-wise display information
    $scope.headerData=[];
    $scope.primaryInfoData=[];
    $scope.forecastingData=[];

    /* Design Section START */
    /* SIDE MENU SECTION -- HIDDEN*/
    console.log("customer controller appearing");
    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    if(AppFactory.getCategory() == "CUSTOMER" || AppFactory.getCategory() == "PROSPECT" || AppFactory.getCategory() == "LEAD")
    {
        $scope.pageTitle = AppFactory.getType() + " DETAIL";
        /* GOTO_TYPE FOR NAVIGATION*/
        $scope.goto_type= AppFactory.getType();
    }
    else
    {
        $scope.pageTitle = AppFactory.getCategory() + " DETAIL";
        /* GOTO_TYPE FOR NAVIGATION*/
        $scope.goto_type= AppFactory.getCategory();
    }

    /* ACTION BAR BUTTONS START*/
    /* LEFT BUTTONS START*/
    
    $scope.goBack = function() 
    {
        var temp={};
        AppFactory.setItemData(temp);
        if(AppFactory.getCategory() == "CUSTOMER" || AppFactory.getCategory() == "PROSPECT" || AppFactory.getCategory() == "LEAD")
        {
          $state.go('app.customerTransactionList');
        }
        else
        {
          $state.go('app.transaction',{category:AppFactory.getCategory()});
        }
    }

    /* RIGHT BUTTONS START*/    
    $scope.goToEdit = function() 
    {
        /* IF OPPORTUNITY */
        if ($scope.goto_type=="OPPORTUNITY")
        {
            AppFactory.setFlag("");
            $state.go('app.editTransaction');
        }
        /* IF QUOTE*/
        if ($scope.goto_type=="QUOTE") 
        {
            $state.go('app.editQuote');
        }
        /*IF ORDER*/
        if ($scope.goto_type=="ORDER") 
        {
            $state.go('app.editOrder');
        }
    }

    /* RIGHT BUTTONS END*/ 
    /* ACTION BAR BUTTONS END*/

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };



    $scope.goto = function()
    {
        //AppFactory.setFlag("EditTransaction");
        $state.go('app.itemDetails');
    }
  
    $scope.show = function()
    {
        /*OPPORTUNITY ACTIONSHEET*/
        if ($scope.goto_type=="OPPORTUNITY")
        {
            $ionicActionSheet.show({
             buttons: [
               { text: 'To Sales Order' },
               { text: 'To Quote' },
                        ],
         
                /* CANCEL BUTTON */
                cancelText: 'Cancel',
                buttonClicked: function(index) {
                if (index==0) {

                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to create a Sales Order using this Opportunity?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    //alert("To Sales Order");
                                    var parameters={
                                        "to":"salesorder",                            
                                        "internalid":AppFactory.getItemId(),  
                                        "from":"opportunity"
                                        };  
                                    //Call the Web service for convert opportunity to sales order               
                                    $ionicLoading.show({
                                        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Converting...",
                                        animation: 'fade-in',
                                        showBackdrop: true,
                                        maxWidth: 200,
                                        showdelay: 300
                                    });
                                    AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",parameters,null,function(results) 
                                    {   
                                        //get result from json                  
                                        console.log("Response Data:"+JSON.stringify(results)); 
                                     
                                        var internalId=JSON.parse(results);
                                        //console.log("Result:"+parseInt(internalId));
                                        
                                        if(parseInt(internalId))
                                        {   
                                            $ionicLoading.hide(); 
                                            window.plugins.toast.show("Record converted successfully.","short","center");
                                            //console.log("It's number");
                                            AppFactory.setItemId(JSON.parse(results));
                                            AppFactory.setType('ORDER');
                                            $state.go('app.orderDetails');
                                             
                                        }   
                                        else
                                        {   
                                            $ionicLoading.hide(); 
                                            //console.log("It's string");
                                            $scope.showAlert(internalId);
                                            return;
                                        }

                                       
                                    });
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });


                    };
                if (index==1) {
                        $ionicPopup.confirm({
                            title: 'Alert',
                            content: 'Do you want to create a Quote using this Opportunity?'
                            }).then(function(res) {
                                if(res) 
                                {
                                    //alert("To Quote");
                                    var parameters={
                                        "to":"estimate",                            
                                        "internalid":AppFactory.getItemId(),  
                                        "from":"opportunity"
                                        };  
                                    //Call the Web service for convert opportunity to quote                  
                                    $ionicLoading.show({
                                        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Converting...",
                                        animation: 'fade-in',
                                        showBackdrop: true,
                                        maxWidth: 200,
                                        showdelay: 300
                                    });
                                    AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",parameters,null,function(results) 
                                    {   
                                        //get result from json                  
                                        console.log("Response Data:"+JSON.stringify(results));  

                                         var internalId=JSON.parse(results);
                                        //console.log("Result:"+parseInt(internalId));
                                        
                                        if(parseInt(internalId))
                                        {   
                                            $ionicLoading.hide(); 
                                            window.plugins.toast.show("Record converted successfully.","short","center");
                                            //console.log("It's number");
                                            AppFactory.setItemId(JSON.parse(results));
                                            AppFactory.setType('QUOTE');
                                            $state.go('app.quoteDetails');  
                                             
                                        }   
                                        else
                                        {   
                                            $ionicLoading.hide(); 
                                            //console.log("It's string");
                                            $scope.showAlert(internalId);
                                            return;
                                        }

 
                                    });
                                } 
                                else 
                                {
                                    console.log('You are not sure');
                                }
                        });
                    };//end if
                return true;
                }
            });
        };

        /*QUOTE ACTIONSHEET*/
        if ($scope.goto_type=="QUOTE")
        {
            if (confirm("Do you want to create a sales order using this quote?")) {
                alert("Sales Order created");
            };
        };
   
    }; //end of show()   

    /*  Design Section END*/

    /* fUNCTIONALITY START*/

    //Web Service for Opportunity details


    //fetch id from factory which item we have clicked from list
    var internalId=AppFactory.getItemId();
    //opportunity details
    $scope.keyArray=[];
      
    $ionicLoading.show({
        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showdelay: 300
            });
                           
    var parameters={
      "id":"customsearch_aavz_26",
      "internalid":internalId,
      "customer_type":"opportunity",
      "noofrecords":"100",    
      "searchtype":"opportunity",
      "display":"7,9,1,3,5,6,2,4,10,8",
      "groupname":"Primary Information,Primary Information,Forecasting,Header,Primary Information,Primary Information,Forecasting,Header,Primary Information,Primary Information",
      "fieldname":"entitystatus,expectedclosedate,forecasttype,entity,title,salesrep,weightedtotal,projectedtotal,internalid,probability"
          };       
    //Request for get data
    AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=121&deploy=1",null,parameters,function(results) { //get result from json

        var mainArray=results;
        for(item in mainArray)
        {
          var mainObj = mainArray[item]; //get single object of array
          //console.log(JSON.stringify(mainObj));        
          for(innerObj in mainObj)
            {
              var internalId=innerObj; //hold key 353
              var keyObj=mainObj[innerObj]; //hold entire object       
              for(key in keyObj)
                {
                    $scope.keyArray.push(key); //hold key 0 to 9
                    // section wise distribution
                    var sectionHeader=keyObj[key].section_header; //get the section header
                    var priority=keyObj[key].priority;
                    //console.log("Priority:"+JSON.stringify(priority));
                    if(sectionHeader=="Header")
                      {
                         $scope.headerArray.push(keyObj[key]); //hold array of objects having section header is Header
                      }    
                    if(sectionHeader=="Primary Information")
                      {
                        $scope.primaryInfoArray.push(keyObj[key]); //hold array of objects having section header is Header
                      }
            
                    if(sectionHeader=="Forecasting")
                      {
                        $scope.forecastingArray.push(keyObj[key]); //hold array of objects having section header is Header
                      }      
                }//end of 3rd for 
            }//end of 2nd for 

        }//end of 1st for

        //Sorting PrimaryInfoArray
        $scope.primaryInfoArray.sort(function(a, b){
          return a.priority - b.priority;
        });

        //Sorting PrimaryInfoArray
        $scope.headerArray.sort(function(a, b){
          return a.priority - b.priority;
        });

        //Sorting PrimaryInfoArray
        $scope.forecastingArray.sort(function(a, b){
          return a.priority - b.priority;
        });


        var projectedTotal={};
        //Code for display dynamic column values of labels    
        for(i=0;i<$scope.headerArray.length;i++)
        {
          var obj=$scope.headerArray[i];
            for(key in  obj)
            {
                if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                {
                  var object={}; //create new object and store into an array
                  object.label=$scope.headerArray[i].label;
                  object.value=obj[key];
                  object.index=$scope.headerArray[i].value;
                  if(object.label=="Projected Total")
                    {
                        projectedTotal.label=object.label;
                        projectedTotal.value=object.value;
                    }
                  $scope.headerData.push(object);
                  //console.log("Column="+JSON.stringify(object.value));  
                }
            }
        }
        //console.log("Column="+JSON.stringify($scope.headerData));
        //console.log("Projected total:"+JSON.stringify(projectedTotal));

        //Code for display dynamic column values of labels    
        for(i=0;i<$scope.primaryInfoArray.length;i++)
          {
            var obj=$scope.primaryInfoArray[i];
              for(key in  obj)
                  {
                      if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                      {
                        var object={}; //create new object and store into an array
                        object.label=$scope.primaryInfoArray[i].label;
                        object.value=obj[key];
                        object.index=$scope.primaryInfoArray[i].value;
                        $scope.primaryInfoData.push(object);
                        //console.log("Column="+JSON.stringify(object.value));  
                      }
                }
          }
        //console.log("Column="+JSON.stringify($scope.primaryInfoData));

        //Code for display dynamic column values of labels 
        $scope.forecastingData.push(projectedTotal);//add projected total field into this section   
        for(i=0;i<$scope.forecastingArray.length;i++)
          {
            var obj=$scope.forecastingArray[i];
              for(key in  obj)
                {
                    if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                    {
                      var object={}; //create new object and store into an array
                      object.label=$scope.forecastingArray[i].label; //it becomes key
                      object.value=obj[key]; //it becomes value
                      object.index=$scope.forecastingArray[i].value;
                      $scope.forecastingData.push(object);
                      //console.log("Column="+JSON.stringify(object.value));  
                    }
                }
          }

        //console.log("Column="+JSON.stringify($scope.forecastingData));

        //Code for set object to factory for Edit Opportunity
        $scope.opportunityDataObj.header=$scope.headerData;
        $scope.opportunityDataObj.primary=$scope.primaryInfoData;
        $scope.opportunityDataObj.forecasttype=$scope.forecastingData;
        //console.log("Object:"+JSON.stringify($scope.opportunityDataObj));
        AppFactory.setOpportunityEditData($scope.opportunityDataObj);

        $scope.showPage = true;
        $ionicLoading.hide();

    }); //end of sendHttpRequest

    /* fUNCTIONALITY END*/

});


/* Transaction Details Colntroller End */
/*==============================================================================================================================================================*/


/* Quote Details Colntroller Start */
/*==============================================================================================================================================================*/

app.controller('QuoteDetailsController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, AppFactory, $state, $ionicActionSheet, $ionicPopup, $rootScope) 
{
    $scope.showPage = false;

    $rootScope.sideMenuEnabled = false;
    $scope.customerList = [];
    //section wise declaraion
    $scope.headerArray=[];
    $scope.primaryInfoArray=[];
    $scope.totalAmountArray=[];
    //section-wise display information
    $scope.headerData=[]; 
    $scope.primaryInfoData=[]; 
    $scope.totalAmountData=[];


  
    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/
    console.log("customer controller appearing");
    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    if(AppFactory.getCategory() == "CUSTOMER" || AppFactory.getCategory() == "PROSPECT" || AppFactory.getCategory() == "LEAD")
    {
        $scope.pageTitle = AppFactory.getType() + " DETAIL";
        /* GOTO_TYPE FOR NAVIGATION*/
        $scope.goto_type= AppFactory.getType();
    }
    else
    {
        $scope.pageTitle = AppFactory.getCategory() + " DETAIL";
        /* GOTO_TYPE FOR NAVIGATION*/
        $scope.goto_type= AppFactory.getCategory();
    }

    $scope.type=AppFactory.getType();
    $scope.pageName=AppFactory.getCategory();//to display convert button on quote details


    /* ACTION BAR BUTTONS START*/
    /* LEFT BUTTONS START*/
     $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };


    $scope.goBack = function() 
    {
        var temp={};
        AppFactory.setItemData(temp);
        if(AppFactory.getCategory() == "CUSTOMER" || AppFactory.getCategory() == "PROSPECT" || AppFactory.getCategory() == "LEAD")
        {
            $state.go('app.customerTransactionList');
        }
        else if(AppFactory.getPreviousCategory() == "LAST 5 QUOTES")
        {
            $state.go('app.transaction',{category:AppFactory.getPreviousCategory()});
        }
        else if(AppFactory.getCategory()=="OPPORTUNITY")
        {
            $state.go('app.transaction',{category:AppFactory.getCategory()});
        }
        else
        {
            $state.go('app.transaction',{category:AppFactory.getCategory()});
        }
        //$state.go('Transaction',{category:AppFactory.getCategory()});

    }
    /* LEFT BUTTONS END*/
    /* RIGHT BUTTONS START*/
    $scope.goToEdit = function() {
        if ($scope.goto_type=="OPPORTUNITY") 
        {
            $state.go('app.editTransaction');
        }

        if ($scope.goto_type=="QUOTE" || $scope.type == "QUOTE") 
        {
            AppFactory.setFlag("");
            $state.go('app.editQuote');
        }

        if ($scope.goto_type=="ORDER") {

          $state.go('app.editOrder');
        }
    }//end of goToEdit()

    /* RIGHT BUTTONS END*/ 
    /* ACTION BAR BUTTONS END*/

    $scope.goto = function()
    {       
        AppFactory.setFlag('quoteDetails');
        $state.go('app.itemDetails');
    }
    $scope.ArrayList = [
      {
        id:1, 
        firstName: "John Doe",
        Status: "Closed Won", 
        Title:"xxxxx", 
        SalesRep:"Krista Barton", 
        Probability:"100.0%", 
        ExpectedClose:"10/08/2013", 
        ProjectedTotal:"12.00", 
        ForCastingType:"Omitted", 
        WeightedTotal:"12.00"}
    ];  

    //function to convert quote to sales order
     $scope.show = function() {
       $ionicPopup.confirm({
            title: 'Alert',
            content: 'Do you want to create a Sales Order using this Quote?'
            }).then(function(res) {
                if(res) 
                {
                    //alert("To Sales Order");
                    var parameters={
                        "to":"salesorder",                            
                        "internalid":AppFactory.getItemId(),  
                        "from":"estimate"
                        };  
                    //Call the Web service for convert quote to sales order               
                    $ionicLoading.show({
                        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Converting...",
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showdelay: 300
                    });
                    AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",parameters,null,function(results) 
                    {   
                        //get result from json                  
                        console.log("Response Data:"+JSON.stringify(results));   

                        var internalId=JSON.parse(results);
                        //console.log("Result:"+parseInt(internalId));
                        
                        if(parseInt(internalId))
                        {   
                            $ionicLoading.hide(); 
                            window.plugins.toast.show("Record converted successfully.","short","center");
                            //console.log("It's number");
                            AppFactory.setItemId(JSON.parse(results));
                            AppFactory.setType('ORDER');
                            $state.go('app.orderDetails'); 
                        }   
                        else
                        {   
                            $ionicLoading.hide(); 
                            //console.log("It's string");
                            $scope.showAlert(internalId);
                            return;
                        }

                       
                    });                  

                } 
                else 
                {
                    console.log('You are not sure');
                }
        });
       
    };//end of show()




    /*  Design Section END*/
    
    // SHIVAJI TANPURE
    //code for quote details start


    //fetch id from factory which item we have clicked from list
    var internalId=AppFactory.getItemId(); 


  
    $ionicLoading.show({
        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showdelay: 300
    });
    var parameters={
        "id":"customsearch_aavz_24",
        "noofrecords":"100",
        "internalid":internalId,
        "display":"4,10,6,5,7,11,9,1,3,8,12,2",
        "groupname":"Invisible Section~Total Amount,Primary Information,Primary Information,Invisible Section~Total Amount,Primary Information,Primary Information,Primary Information,Header,Invisible Section~Total Amount,Primary Information,Primary Information,Header",
        "fieldname":"shippingamount,expectedclosedate,trandate,amount,customform,duedate,probability,entity,taxtotal,entitystatus,internalid,memo",
        "customer_type":"estimate"
            };
     
    //Request for get data
    AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=98&deploy=1",null,parameters,function(results) { //get result from json

        var mainArray=results;
        for(item in mainArray)
        {
              var mainObj = mainArray[item];
              for(innerObj in mainObj)
              {
                var id=innerObj; //hold key 241
                var keyObj=mainObj[innerObj]; //hold entire object 
                //console.log(JSON.stringify(keyObj));  
                for(key in keyObj)
                {
                  //console.log(key);
                  var singleObject=keyObj[key];
                  //singleObject.id=id;  //add key into object
                  
                  // section wise distribution
                  var sectionHeader=singleObject.section_header;
                  //console.log(JSON.stringify(sectionHeader));
                  
                  
                  if(sectionHeader=="Header")
                  {   
                    $scope.headerArray.push(keyObj[key]); //hold array of objects having section header is Header
                  }
                    
                  if(sectionHeader=="Primary Information")
                  {
                    $scope.primaryInfoArray.push(keyObj[key]); //hold array of objects having section header is Header
                  }
                    
                  if(sectionHeader=="Total Amount")
                  {
                    $scope.totalAmountArray.push(keyObj[key]); //hold array of objects having section header is Header
                  }
                  
                  
                }//end of 3rd for
                
            }//end of 2nd for   
          
        }//end of 1 st for      
        //console.log(JSON.stringify($scope.primaryInfoArray));       

        var memo={};
        //Code for display dynamic column values of labels    
        for(i=0;i<$scope.headerArray.length;i++)
        {
            var singleObj=$scope.headerArray[i];
            for(key in singleObj)
            {
              if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
              {
                //console.log(key);
                var object={};
                object.label=$scope.headerArray[i].label;
                object.value=singleObj[key];
                if(object.label=="Memo")
                {
                    memo.label=object.label;
                    memo.value=object.value;
                }
                $scope.headerData.push(object);
              }            
            }            
        }
        //console.log("Column="+JSON.stringify($scope.headerData)); 
        //console.log("Memo:"+JSON.stringify(memo));
          
        for(i=0;i<$scope.primaryInfoArray.length;i++)
        {
            var singleObj=$scope.primaryInfoArray[i];
            for(key in singleObj)
            {
              if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
              {
                //console.log(key);
                var object={};
                object.label=$scope.primaryInfoArray[i].label;
                object.value=singleObj[key];
                object.index=$scope.primaryInfoArray[i].value;
                $scope.primaryInfoData.push(object);
              }            
            }            
        }
        $scope.primaryInfoData.push(memo);
        //console.log("Column="+JSON.stringify($scope.primaryInfoData));

        for(i=0;i<$scope.totalAmountArray.length;i++)
        {
            var singleObj=$scope.totalAmountArray[i];
            for(key in singleObj)
            {
              if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
              {
                //console.log(key);
                var object={};
                object.label=$scope.totalAmountArray[i].label;
                object.value=singleObj[key];
                $scope.totalAmountData.push(object);
              }            
            }            
        }
        //console.log("Column="+JSON.stringify($scope.totalAmountData));
          
        //SET object for factory when EDIT Quote 
        $scope.quoteObj={};
        $scope.quoteObj.header=$scope.headerData;
        $scope.quoteObj.primary=$scope.primaryInfoData;
        $scope.quoteObj.totalAmt=$scope.totalAmountData;
        //console.log("object:"+JSON.stringify($scope.quoteObj));
        AppFactory.setQuoteEdit($scope.quoteObj);
        
        $scope.showPage = true;
        $ionicLoading.hide();

    }); //end of sendHttpRequest
    /* fUNCTIONALITY END*/

});

/*==============================================================================================================================================================*/
/* Quote Details Colntroller End */


/* Order Details Colntroller Start */
/*==============================================================================================================================================================*/

app.controller('OrderDetailsController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, AppFactory, $state, $ionicActionSheet, $rootScope) 
{
    $scope.showPage = false;

    $rootScope.sideMenuEnabled = false;
    $scope.customerList = [];
    //section-wise declaration    
    $scope.headerArray=[];
    $scope.primaryInfoArray=[];
    $scope.totalAmountArray=[];
    //section-wise display information
    $scope.headerData=[];
    $scope.primaryInfoData=[];
    $scope.totalAmountData=[];

    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/
    //console.log("customer controller appearing");
    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    if(AppFactory.getCategory() == "CUSTOMER" || AppFactory.getCategory() == "PROSPECT" || AppFactory.getCategory() == "LEAD")
    {
        $scope.pageTitle = AppFactory.getType() + " DETAIL";
        /* GOTO_TYPE FOR NAVIGATION*/
        $scope.goto_type= AppFactory.getType();
    }
    else
    {
        $scope.pageTitle = AppFactory.getCategory() + " DETAIL";
        /* GOTO_TYPE FOR NAVIGATION*/
        $scope.goto_type= AppFactory.getCategory();
    } 

    /* ACTION BAR BUTTONS START*/
    /* LEFT BUTTONS START*/ 
    $scope.goBack = function() 
    {
        var temp={};
        AppFactory.setItemData(temp);
        if(AppFactory.getCategory() == "CUSTOMER" || AppFactory.getCategory() == "PROSPECT" || AppFactory.getCategory() == "LEAD")
        {
            $state.go('app.customerTransactionList');
        }
        else if(AppFactory.getPreviousCategory() == "LAST 5 ORDERS")
        {
            $state.go('app.transaction',{category:AppFactory.getPreviousCategory()});
        }
        else if(AppFactory.getCategory()=="QUOTE" || AppFactory.getCategory()=="OPPORTUNITY")
        {
            $state.go('app.transaction',{category:AppFactory.getCategory()});
        }
        else
        {
            $state.go('app.transaction',{category:AppFactory.getCategory()});
        }
        //$state.go('Transaction',{category:AppFactory.getCategory()});
    }

    /* LEFT BUTTONS END*/
    /* RIGHT BUTTONS START*/  
    $scope.goToEdit = function() {
        console.log(" edit button clicked" +$scope.goto_type);
        if ($scope.goto_type=="OPPORTUNITY")
        {
            $state.go('app.editTransaction');
        }

        if ($scope.goto_type=="QUOTE")
        {
            $state.go('app.editQuote');
        }
        if ($scope.goto_type=="ORDER" || AppFactory.getType() == "ORDER")
        {
            AppFactory.setFlag("");
            $state.go('app.editOrder');
        }
    }//end of goToEdit()

    /* RIGHT BUTTONS END*/ 
    /* ACTION BAR BUTTONS END*/
    $scope.goto = function()
    {
        AppFactory.setFlag('orderDetails');
        $state.go('app.itemDetails');
    }

    $scope.ArrayList = [
      {
        id:1, 
        firstName: "John Doe",
        Status: "Closed Won", 
        Title:"xxxxx", 
        SalesRep:"Krista Barton", 
        Probability:"100.0%", 
        ExpectedClose:"10/08/2013", 
        ProjectedTotal:"12.00", 
        ForCastingType:"Omitted", 
        WeightedTotal:"12.00"}
    ]; 

    $scope.show = function() {
        //console.log("show function called()");

        if ($scope.goto_type=="OPPORTUNITY")
        {

            $ionicActionSheet.show({
            buttons: [
               { text: 'To Sales Order' },
               { text: 'To Quote' },
             ],
     
            cancelText: 'Cancel',
                buttonClicked: function(index) {

                if (index==0) {

                    alert("To Sales Order");
                };
                if (index==1) {

                    alert("To Quote");
                };
                return true;
                }
            });
        };

        if ($scope.goto_type=="QUOTE") 
        {

            if (confirm("Do you want to create a sales order using this quote?")) {
                alert("Sales Order created");
            };
        };  

    };//end of show()    
    
    /*  Design Section END*/

    // Order Details starts
    //fetch id from factory which item we have clicked from list
    var internalId=AppFactory.getItemId(); 

    $ionicLoading.show({
        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showdelay: 300
    });
    var parameters={
        "id":"customsearch_aavz_19",
        "noofrecords":"100",
        "internalid":internalId,
        "display":"8,1,7,2,6,3,4,5,1",
        "groupname":"Primary Information,Default,Primary Information,Header,Primary Information,Invisible Section~Total Amount,Invisible Section~Total Amount,Invisible Section~Total Amount,Header",
        "fieldname":"internalid,orderstatus,salesrep,memo,trandate,taxtotal,shippingamount,amount,entity",
        "customer_type":"salesorder"
            };
     
    //Request for get data
    AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",null,parameters,function(results) { //get result from json
      
        var mainArray=results;
        for(item in mainArray)
            {
              var mainObj = mainArray[item];
              for(innerObj in mainObj)
              {
                var id=innerObj; //hold key 241
                var keyObj=mainObj[innerObj]; //hold entire object 
                //console.log(JSON.stringify(keyObj));  
                for(key in keyObj)
                {
                  //console.log(key);
                  var singleObject=keyObj[key];
                  //singleObject.id=id;  //add key into object
                  
                  // section wise distribution
                  var sectionHeader=singleObject.section_header;
                  //console.log(JSON.stringify(sectionHeader));
                  
                  
                  if(sectionHeader=="Header")
                  {   
                    $scope.headerArray.push(keyObj[key]); //hold array of objects having section header is Header
                  }
                    
                  if(sectionHeader=="Primary Information")
                  {
                    $scope.primaryInfoArray.push(keyObj[key]); //hold array of objects having section header is Header
                  }
                    
                  if(sectionHeader=="Total Amount")
                  {
                    $scope.totalAmountArray.push(keyObj[key]); //hold array of objects having section header is Header
                  }
                  
                  
                }//end of 3rd for
                
              }//end of 2nd for   
              
            }//end of 1 st for          
        //console.log(JSON.stringify($scope.primaryInfoArray)); 
         
        //Code for display dynamic column values of labels    
        for(i=0;i<$scope.headerArray.length;i++)
        {
            var singleObj=$scope.headerArray[i];
            for(key in singleObj)
            {
              if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
              {
                //console.log(key);
                var object={};
                object.label=$scope.headerArray[i].label;
                object.value=singleObj[key];
                $scope.headerData.push(object);
              }            
            }            
        }
        //console.log("Column="+JSON.stringify($scope.headerData));
  
        for(i=0;i<$scope.primaryInfoArray.length;i++)
        {
            var singleObj=$scope.primaryInfoArray[i];
            for(key in singleObj)
            {
              if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
              {
                //console.log(key);
                var object={};
                object.label=$scope.primaryInfoArray[i].label;
                object.value=singleObj[key];
                $scope.primaryInfoData.push(object);
              }            
            }            
        }
        //console.log("Column="+JSON.stringify($scope.primaryInfoData));
                  
        for(i=0;i<$scope.totalAmountArray.length;i++)
        {
            var singleObj=$scope.totalAmountArray[i];
            for(key in singleObj)
            {
              if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
              {
                //console.log(key);
                var object={};
                object.label=$scope.totalAmountArray[i].label;
                object.value=singleObj[key];
                $scope.totalAmountData.push(object);
              }            
            }            
        }
        //console.log("Column="+JSON.stringify($scope.totalAmountData));
  
        $scope.orderObj={};
        $scope.orderObj.header=$scope.headerData;
        $scope.orderObj.primary=$scope.primaryInfoData;
        $scope.orderObj.totalAmt=$scope.totalAmountData;
        //console.log("object:"+JSON.stringify($scope.quoteObj));
        AppFactory.setOrderEditData($scope.orderObj);
      
        $scope.showPage = true;
        $ionicLoading.hide();

    }); //end of sendHttpRequest
    //End of Order Details

    /* fUNCTIONALITY END*/


});

/* Order Details Colntroller End */
/*==============================================================================================================================================================*/

/* Add Transaction page Colntroller Start*/
/*=============================================================================================================================*/

app.controller('AddOpportunityController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, AppFactory, $state, $ionicActionSheet,  $ionicModal,$ionicPopup)
{  
    $scope.opportunity={};
    $scope.Data=AppFactory.getCustomerNameStatus();
    $scope.opportunity.entityvalue=$scope.Data.custName;
    $scope.opportunity.entity=($scope.Data.internalId).toString();
    $scope.opportunity.personaltype="opportunity";

    $scope.customerList = [];
    $scope.Flag="";
    //modal window's array list
    $scope.SalesRepp=[];
    $scope.ForecastType=[];
    $scope.Statuses=[];

    /* Design Section START */
    /* SIDE MENU SECTION -- HIDDEN*/
    
    var object=AppFactory.getOpportunityData();
    //console.log("Object:"+JSON.stringify(object))
    if(object.edited=="true")
    {
         $scope.opportunity=object;
    }
    
    //clear previous item data
    AppFactory.setItemData("");

    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }
  
 
    $scope.ArrayList = [
        {
        id:1, 
        firstName: "John Doe",
        Status: "Closed Won", 
        Title:"xxxxx", 
        SalesRep:"Krista Barton", 
        Probability:"100.0%", 
        ExpectedClose:"10/08/2013", 
        ProjectedTotal:"12.00", 
        ForCastingType:"Omitted", 
        WeightedTotal:"12.00"}
    ];


    $scope.showConfirm = function() {
        $ionicPopup.confirm({
                    title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.customerDetails');
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };//end of showConfirm()

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

            });
    };

    /* RIGHT BUTTONS START*/ 
    $scope.saveData = function() 
    {
        if($scope.opportunity.probability == undefined || $scope.opportunity.probability == "")
        {
            $scope.showAlert("Please fill Probability field");
            return;
        }

        if($scope.opportunity.expectedclosedate == undefined || $scope.opportunity.expectedclosedate == "")
        {
            $scope.showAlert("Please fill Expected Close Date field");
            return;
        }

        $scope.Flag="addOpportunity";
        AppFactory.setFlag($scope.Flag);

        //set opportunity object to factory for later use
        $scope.opportunity.edited="true";
        AppFactory.setOpportunityData($scope.opportunity);

        $state.go('app.editItem');
    }//end of saveData()

    /* RIGHT BUTTONS END*/
    /* ACTION BAR BUTTONS END*/


    /*------------------------- code for the "date" is started here----------------------------------*/
	$ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
	   $scope.datemodal = modal;
	   },
        {
    	// Use our scope for the scope of the modal to keep it simple
    	scope: $scope, 
    	// The animation we want to use for the modal entrance
    	animation: 'slide-in-up'

    	});
	
    	$scope.$on('$destroy', function() 
    	{
    	    $scope.datemodal.remove();
    	});
    	
    	$scope.opendateModal = function() {
    	  $scope.datemodal.show();
    	};
    	$scope.cancel = function()
    	{
    		$scope.datemodal.hide();
    	};
    	$scope.closedateModal = function(model) 
    	{
    	if(model == undefined || model == "")
    	{
    		$scope.showAlert("Please select Date.");
    	}
    	else
    	{
    		$scope.datemodal.hide();
    		$scope.date = model;   		  
    			
			var myDate = new Date($scope.date);
			var month = myDate.getMonth()+1;
			$scope.no1=month;
			
			var day = myDate.getDate();
			$scope.no=day;
			var year = myDate.getFullYear();
			
			$scope.no2=year;
			$scope.opportunity.expectedclosedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    	}
    };
    	
	/*------------------------- code for the "date" is ended here----------------------------------*/

    $scope.show = function() {
        console.log("show function called()");
        // Show the action sheet
       $ionicActionSheet.show({
         buttons: [
           { text: 'Share' },
           { text: 'Move' },
         ],
         destructiveText: 'Delete',
         titleText: 'Modify your Album',
         cancelText: 'Cancel',
         buttonClicked: function(index) {
           return true;
            }
        });
    };

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('ForcastType.html', function(forecastModal) {
        $scope.forecastModal = forecastModal;
      }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    }); 
   
    $scope.fetchForcastTypeList = function() 
    {
        $scope.forecastModal.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Forcast Type...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        //call webservice for display drop-down list of forecasttype
        $scope.ForecastType=[];
        var parameters={
              "field_name":"forecasttype",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"opportunity",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
               
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.ForecastType.push(object);          
            }
            $ionicLoading.hide();
             
        }); //end of sendHttpRequest
        //populate the drop-down window       
    };

    $scope.closeForcastTypeModal = function() {
        $scope.forecastModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.forecastModal.remove();
    });

    $scope.setForcastType=function(typedata)
    {       
        $scope.opportunity.forecasttypevalue=typedata.value;
        $scope.opportunity.forecasttype=typedata.index;
        $scope.forecastModal.hide();
    }
    
    /* Sales Repp */
    $ionicModal.fromTemplateUrl('salesrepp.html', function(salesReppModal) {
        $scope.salesReppModal = salesReppModal;
        }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    }); 
     
    
    $scope.fetchSalesReppList = function() 
    {   
        $scope.SalesRepp=[];
        $scope.salesReppModal.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Sales Rep...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        //re-init empty each time button click
      
        var parameters={
          "field_name":"salesrep",
          "field_type":"select",
          "field_subtext":"",
          "customer_type":"opportunity",
          "field_dependent":"",
          "field_dependentvalue":""
              };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json
            console.log("Response:"+JSON.stringify(results));
            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.SalesRepp.push(object);          
            }
            $ionicLoading.hide();
           
        }); //end of sendHttpRequest
        //populate the drop-down window    
    };

    $scope.closeSalesReppMadal = function() {
        $scope.salesReppModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.salesReppModal.remove();
    });

    $scope.setSalesRepresaative=function(salesrep)
    {
        $scope.opportunity.salesrep=salesrep.index;
        $scope.opportunity.salesrepvalue=salesrep.value;
        $scope.salesReppModal.hide();
    }

    /* Status Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('Status.html', function(Status) {
        $scope.Status = Status;
      }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
  
   
    $scope.fetchStatusList = function() 
    {   
        $scope.Statuses=[];
        $scope.Status.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Status...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });        
        var parameters={
              "field_name":"entitystatus",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"opportunity",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.Statuses.push(object);          
            }
            $ionicLoading.hide();
           
        }); //end of sendHttpRequest
        //populate the drop-down window    
    };
    $scope.closeStatusModal = function() {
        $scope.Status.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.Status.remove();
    });

    $scope.setStatus=function(status)
    {
        $scope.opportunity.entitystatusvalue=status.value; 
        $scope.opportunity.entitystatus=status.index;     
        $scope.Status.hide();
    }

    $scope.ArrayList = [
      {
        id:1, 
        firstName: "John Doe",
        Status: "Closed Won", 
        Title:"xxxxx", 
        SalesRep:"Krista Barton", 
        Probability:"100.0%", 
        ExpectedClose:"10/08/2013", 
        ProjectedTotal:"12.00", 
        ForCastingType:"Omitted", 
        WeightedTotal:"12.00"},
    ];
   
    /* fUNCTIONALITY END*/

});


/* Add Transaction page Colntroller End*/
/*=============================================================================================================================*/


/* Edit Transaction page Colntroller Start*/
/*=============================================================================================================================*/

app.controller('EditTransactionController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, AppFactory, $state, $ionicActionSheet, $ionicModal,$ionicPopup) 
{
 
    $scope.opportunityObject={};

    $scope.Flag="";  
    $scope.customerList = [];

    //modal window's array list
    $scope.ForecastType=[];
    $scope.SalesRepp=[];
    $scope.Statuses=[];
     
    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/
    console.log("customer controller appearing");
    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    $scope.pageTitle = "EDIT " + AppFactory.getCategory(); 
    
    $scope.ArrayList = [
        {
        Title:"Aashna",
        SalesRep:"",
        Status:"",
        Probability:"100%",
        ExpectedClose:"28/03/2014",
        ForCastingType:"['Omitted', 'Worst Case']",
        WeightedTotal:"1996.00" 
       }
    ];

    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.transactionDetails');
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };//end of showConfirm

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }
    /* LEFT BUTTONS END*/
    /* RIGHT BUTTONS START*/

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {
        });
    };


    /* RIGHT BUTTONS START*/ 
    $scope.saveData = function() 
    {
        if($scope.opportunityObject.probability == undefined || $scope.opportunityObject.probability == "")
        {
            $scope.showAlert("Please fill Probability field");
            return;
        }
        if($scope.opportunityObject.expectedclosedate == undefined || $scope.opportunityObject.expectedclosedate == "")
        {
            $scope.showAlert("Please fill Expected Close Date field");
            return;
        }
        $scope.Flag="editTransaction";
        AppFactory.setFlag($scope.Flag);
         
        //set final Updated data to factory
        AppFactory.setUpdatedOpportunityData($scope.opportunityObject);
            console.log("FinalObject:"+JSON.stringify($scope.opportunityObject));
            $state.go('app.editItem');
    }//end of saveData()
    
    /* RIGHT BUTTONS END*/
    /* ACTION BAR BUTTONS END*/ 

    $scope.show = function() {
        console.log("show function called()");

        // Show the action sheet
        $ionicActionSheet.show({
         buttons: [
           { text: 'Share' },
           { text: 'Move' },
         ],
         destructiveText: 'Delete',
         titleText: 'Modify your album',
         cancelText: 'Cancel',
        buttonClicked: function(index) {
            return true;
            }
        });

    };//end of show()

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('ForcastType.html', function(forecastModal) {
        $scope.forecastModal = forecastModal;
         }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    $scope.fetchForcastTypeList = function() {
        //call webservice for display drop-down list of forecasttype
        $scope.ForecastType=[];
        $scope.forecastModal.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching forcast type...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        var parameters={
              "field_name":"forecasttype",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"opportunity",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
               
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
                var objectArray=mainArray[obj];          
                //console.log(JSON.stringify(objectArray));
                var object={};
                object.index=objectArray[0];
                object.value=objectArray[1];
                $scope.ForecastType.push(object);          
            }            
            $ionicLoading.hide();
        }); //end of sendHttpRequest
        //populate the drop-down window   
    };

    $scope.closeForcastTypeModal = function() {
        $scope.forecastModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.forecastModal.remove();
    });

    $scope.forecasttypevalue="";
    $scope.setForcastType=function(typedata)
    {
        $scope.opportunityObject.forecasttypevalue=typedata.value;
        $scope.opportunityObject.forecasttype=typedata.index;
        $scope.forecastModal.hide();
    }

    /* Sales Repp*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('salesrepp.html', function(salesReppModal) {
        $scope.salesReppModal = salesReppModal;
        }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
        }); 

   
    $scope.fetchSalesReppList=function() {
        //re-init empty each time button click      
        $scope.SalesRepp=[];
        $scope.salesReppModal.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Sales Rep...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        var parameters={
              "field_name":"salesrep",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"opportunity",
              "field_dependent":"",
              "field_dependentvalue":""
                };         
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.SalesRepp.push(object);          
            }            
            $ionicLoading.hide();
        }); //end of sendHttpRequest
       
    };
    $scope.closeSalesReppMadal = function() {
        $scope.salesReppModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.salesReppModal.remove();
    });

    $scope.salesrepName="";
    $scope.setSalesRepresaative=function(salesrep)
    {
         $scope.opportunityObject.salesrep=salesrep.index;
         $scope.opportunityObject.salesrepvalue=salesrep.value;
         $scope.salesReppModal.hide();
    }

    /* Status Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('Status.html', function(Status) {
        $scope.Status = Status;
      }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
      });  

    /* Status Modal */
    $scope.fetchStatusList = function() {
        $scope.Statuses=[];
        $scope.Status.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching status list...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        var parameters={
              "field_name":"entitystatus",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"opportunity",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
               
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.Statuses.push(object);          
            }
            $ionicLoading.hide();            
        }); //end of sendHttpRequest
      
    };
    $scope.closeStatusModal = function() {
        $scope.Status.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.Status.remove();
    });

    $scope.statusValue="";
    $scope.setStatus=function(status)
    {
        $scope.opportunityObject.entitystatusvalue=status.value; 
        $scope.opportunityObject.entitystatus=status.index; 
        $scope.Status.hide();
    }

    /*  Design Section END*/
    
    //Existing data
    //get opportunity data object from factory which was set in transactiondetails controller
 
    $scope.opportunityObject.personaltype="opportunity";
    $scope.opportunityObject.entity=AppFactory. getItemId();
    $scope.opportunityObject.internalid=AppFactory. getItemId();

    if(AppFactory.getFlag()!="editTransaction")
    {
        $scope.oppotunityObj=AppFactory.getOpportunityEditData();
        // console.log("obj:"+JSON.stringify($scope.oppotunityObj));
        $scope.headerArray=$scope.oppotunityObj.header;
        $scope.primaryInfoArray=$scope.oppotunityObj.primary;
        $scope.forecastTypeArray=$scope.oppotunityObj.forecasttype;

          //console.log("obj:"+JSON.stringify($scope.headerArray));
        for(obj in $scope.headerArray)
        {
              var data=$scope.headerArray[obj];
              for(key in data)
              {
                 // console.log(JSON.stringify(data[key]));
                 if(data[key]=="Company Name")
                 {
                  $scope.opportunityObject.entityvalue=data.value;
                  //console.log("CN:"+data.value);
                 }
              }
        }

        for(obj in $scope.forecastTypeArray)
        {
          var data=$scope.forecastTypeArray[obj];
          for(key in data)
          {
             // console.log(JSON.stringify(data[key]));
             if(data[key]=="Weighted Total")
             {
              $scope.opportunityObject.weightedtotal=data.value;
              //console.log("WT:"+data.value);
             }
             else if(data[key]=="Forecast Type")
             {
              $scope.opportunityObject.forecasttype=data.index;
              $scope.opportunityObject.forecasttypevalue=data.value;
              //console.log("WT:"+data.value);
             }         
          }
        }

        for(obj in $scope.primaryInfoArray)
        {
            var data=$scope.primaryInfoArray[obj];
            for(key in data)
            {
                 // console.log(JSON.stringify(data[key]));
                if(data[key]=="Title")
                {
                  $scope.opportunityObject.title=data.value;
                  //console.log("Title:"+data.value);
                }
                else if(data[key]=="Sales Rep")
                {
                  $scope.opportunityObject.salesrep=data.index;
                  $scope.opportunityObject.salesrepvalue=data.value;
                  //console.log("Sales Rep:"+data.value);
                }
                else if(data[key]=="Status")
                {
                  $scope.opportunityObject.entitystatus=data.index;
                  $scope.opportunityObject.entitystatusvalue=data.value;
                  //console.log("Sales Rep:"+data.value);
                }
                else if(data[key]=="Probability")
                {
                  $scope.opportunityObject.probability=data.value;
                  //console.log("Sales Rep:"+data.value);
                }
                else if(data[key]=="Expected Close")
                {
    		 
                    //$scope.opportunityObject.expectedclosedate=data.value;
    		        var myDate = new Date(data.value);
    					var month = myDate.getMonth()+1;
    						console.log(" month is "+ month);
    						$scope.no1=month;
    						
    						var day = myDate.getDate();
    						console.log(" day is "+ day);
    						$scope.no=day;
    						var year = myDate.getFullYear();
    						console.log(" year is "+ year);
    						$scope.no2=year;
    						$scope.opportunityObject.expectedclosedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    						console.log("existing date="+$scope.opportunityObject.expectedclosedate);
                    //console.log("Date:"+data.value);
                }        
            }
        }

    }//end if
    else
    {
        $scope.opportunityObject=AppFactory.getUpdatedOpportunityData();
    }



    /*------------------------- code for the "date" is started here----------------------------------*/
	$ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
	    $scope.datemodal = modal;
			},
			{
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope, 
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'

		});
				
		$scope.$on('$destroy', function() 
		{
			$scope.datemodal.remove();
		});
				
		$scope.opendateModal = function() {
			$scope.datemodal.show();
		};
		$scope.cancel = function()
		{
			$scope.datemodal.hide();
		};
		$scope.closedateModal = function(model) 
		{
			if(model == undefined || model == "")
			{
				$scope.datemodal.hide();
			}
			else
			{
		    	$scope.datemodal.hide();
				$scope.date = model;

				var myDate = new Date($scope.date);
				var month = myDate.getMonth()+1;
				$scope.no1=month;
				
				var day = myDate.getDate();
				$scope.no=day;
				var year = myDate.getFullYear();
				
				$scope.no2=year;
				$scope.opportunityObject.expectedclosedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			}
		};
			
	/*------------------------- code for the "date" is ended here----------------------------------*/

    /* fUNCTIONALITY END*/

});

/* Edit Transaction page Colntroller Start*/
/*=============================================================================================================================*/


/* Add Order Colntroller Start*/
/*=============================================================================================================================*/

app.controller('AddOrderController', function($scope, $rootScope, $state, AppFactory, $ionicModal, $ionicLoading,$ionicPopup) 
{
  
    $scope.order={};
    $scope.Data=AppFactory.getCustomerNameStatus();
    $scope.order.entityvalue=$scope.Data.custName;
    $scope.order.entity=($scope.Data.internalId).toString();
    $scope.order.personaltype="salesorder";
    //modal window array-list
    $scope.SalesRepp=[];

    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/

    var object=AppFactory.getOrderData();
    console.log("Object:"+JSON.stringify(object));
    if(object.edited == "true")
    {
        $scope.order=object;
    }

     //clear previous selected item data
    AppFactory.setItemData("");

    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* Sales Repp*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('salesrepp.html', function(salesReppModal) {
        $scope.salesReppModal = salesReppModal;
          }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
    });  

    
    $scope.fetchSalesReppList = function() 
    {   
        $scope.SalesRepp=[];
        $scope.salesReppModal.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Sales Rep...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        //re-init empty each time button click
        
        var parameters={
              "field_name":"salesrep",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"salesorder",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json
       
            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.SalesRepp.push(object);          
            }
            $ionicLoading.hide();
           
        }); //end of sendHttpRequest
    
    };
    $scope.closeSalesReppMadal = function() {
        $scope.salesReppModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.salesReppModal.remove();
    });
  
    $scope.setSalesRepresaative=function(salesrep)
    {
        $scope.order.salesrep=salesrep.index;
        $scope.order.salesrepvalue=salesrep.value;
        $scope.salesReppModal.hide();
    }
    
    /* ACTION BAR BUTTONS START*/
    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.customerDetails', {category:AppFactory.getCategory()});
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
          content: message
        }).then(function(res) {

        });
    };
    /* RIGHT BUTTONS START*/
    $scope.saveData = function() 
    {

        if($scope.order.trandate == undefined || $scope.order.trandate == "")
        {
            $scope.showAlert("Please fill Date field");
            return;
        }
        $scope.Flag="addOrder";
        AppFactory.setFlag($scope.Flag);
        //set order object to factory for later use
        $scope.order.edited="true";
        AppFactory.setOrderData($scope.order);
        //console.log("order object Data:"+JSON.stringify($scope.order));
        $state.go('app.editItem');
    }
    
    /*------------------------- code for the "date" is started here----------------------------------*/
	$ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
		$scope.datemodal = modal;

		},
		{
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope, 
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'

		});
		
		$scope.$on('$destroy', function() 
		{
		    $scope.datemodal.remove();
		});
		
		$scope.opendateModal = function() {
		  $scope.datemodal.show();
		};
		$scope.cancel = function()
		 {
			$scope.datemodal.hide();
		 };
	$scope.closedateModal = function(model) 
	{
		    if(model == undefined || model == "")
			{
				$scope.showAlert("please fill Date field");
			}
			else
			{
				  $scope.datemodal.hide();
				  $scope.date = model;
				  
				
				var myDate = new Date($scope.date);
				var month = myDate.getMonth()+1;
				$scope.no1=month;
				
				var day = myDate.getDate();
				$scope.no=day;
				var year = myDate.getFullYear();
				
				$scope.no2=year;
				$scope.order.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			}	
	};
	
    /*------------------------- code for the "date" is ended here----------------------------------*/

    /* fUNCTIONALITY END*/



});

/*=============================================================================================================================*/
/* Add Order Colntroller End*/


/* Edit Order Colntroller Start*/
/*=============================================================================================================================*/


app.controller('EditOrderController', function($scope, $rootScope, $state, AppFactory, $ionicModal,$ionicPopup,$ionicLoading) 
{

    $scope.order={};
    //model -window array-list
     $scope.SalesRepp=[];

    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* Sales Repp*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('salesrepp.html', function(salesReppModal) {
            $scope.salesReppModal = salesReppModal;
      }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
      });    
  
   
    $scope.fetchSalesReppList = function() {
        //re-init empty each time button click
        $scope.SalesRepp=[];
        $scope.salesReppModal.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Sales Rep...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        var parameters={
              "field_name":"salesrep",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"salesorder",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
               
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.SalesRepp.push(object);          
            }       
            $ionicLoading.hide();
        }); //end of sendHttpRequest
    };
    $scope.closeSalesReppMadal = function() {
        $scope.salesReppModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.salesReppModal.remove();
    });

    $scope.setSalesRepresaative=function(salesrep)
    {
        $scope.order.salesrep=salesrep.index;
        $scope.order.salesrepvalue=salesrep.value;
        $scope.salesReppModal.hide();
    }

    /* ACTION BAR BUTTONS START*/
    $scope.showConfirm = function() {
        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.orderDetails');
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {
        
        $scope.showConfirm();
    }
           

    $scope.showAlert = function(message) {
            $ionicPopup.alert({
          title: 'Validation Error',
          content: message
        }).then(function(res) {

        });
    };
    /* RIGHT BUTTONS START*/ 

    $scope.saveData = function() 
    {

        if($scope.order.trandate == undefined || $scope.order.trandate == "")
        {
            $scope.showAlert("Please fill Date field");
            return;
        }
            $scope.Flag="editOrder";
            AppFactory.setFlag($scope.Flag);

        //set Final Updated order object to factory
        AppFactory.setUpdatedOrderData($scope.order);
        console.log("Updated order object:"+JSON.stringify($scope.order));
               
        $state.go('app.editItem');
    }
    /*  Design Section END*/


    /* SPACE TO ADD FUNCTIONS*/

    //Existing data 
    //get order data object from factory which was set in orderdetails controller
 

    $scope.order.personaltype="salesorder";
    $scope.order.entity=AppFactory. getItemId();
    $scope.order.internalid=AppFactory. getItemId();

    if(AppFactory.getFlag()!="editOrder")
    {
   
        //get from fctory which was set at quotedetails controller
        $scope.orderObj=AppFactory.getOrderEditData();
        $scope.headerArray=$scope.orderObj.header;
        $scope.primaryInfoArray=$scope.orderObj.primary;

        for(obj in $scope.headerArray)
        {
            var data=$scope.headerArray[obj];
            for(key in data)
            {
                // console.log(JSON.stringify(data[key]));
                if(data[key]=="Name")
                {
                    $scope.order.entityvalue=data.value;
                    //console.log("CN:"+data.value);
                }
                else if(data[key]=="Memo")
                {
                    $scope.order.memo=data.value;
                    //console.log("CN:"+data.value);
                }
            }
        }
        //console.log("obj He:"+JSON.stringify($scope.headerArray));


        for(obj in $scope.primaryInfoArray)
        {
            var data=$scope.primaryInfoArray[obj];
            for(key in data)
            {
                // console.log(JSON.stringify(data[key]));
                if(data[key]=="Date")
                {
                    //$scope.order.trandate=data.value;
            	    var myDate = new Date(data.value);
    					 var month = myDate.getMonth()+1;
    						console.log(" month is "+ month);
    						$scope.no1=month;
    						
    						var day = myDate.getDate();
    						console.log(" day is "+ day);
    						$scope.no=day;
    						var year = myDate.getFullYear();
    						console.log(" year is "+ year);
    						$scope.no2=year;
    						$scope.order.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    						console.log("existing date="+$scope.order.trandate);
                      //console.log("Title:"+data.value);
                }
                else if(data[key]=="Sales Rep")
                {
                    $scope.order.salesrep=data.index;
                    $scope.order.salesrepvalue=data.value;
                    //console.log("Custom Form index:"+data.index);
                }               
            }
        }

    }//end if
    else
    {
        $scope.order=AppFactory.getUpdatedOrderData();
    } 
 
    /*------------------------- code for the "date" is started here----------------------------------*/
	$ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
		$scope.datemodal = modal;

		},
		{
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope, 
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'

		});
		
		$scope.$on('$destroy', function() 
		{
		    $scope.datemodal.remove();
		});
		$scope.cancel = function()
		 {
			$scope.datemodal.hide();
		 };
		
		$scope.opendateModal = function() {
		  $scope.datemodal.show();
		};
		$scope.closedateModal = function(model) 
		{
		  if(model == undefined || model == "")
			{
				$scope.datemodal.hide();
			}
			else
			{
    			$scope.datemodal.hide();
    			$scope.date = model;
    			  
    			
    			var myDate = new Date($scope.date);
    			var month = myDate.getMonth()+1;
    			$scope.no1=month;
    			
    			var day = myDate.getDate();
    			$scope.no=day;
    			var year = myDate.getFullYear();
    			
    			$scope.no2=year;
    			$scope.order.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
		    }
	    };
	
	/*------------------------- code for the "date" is ended here----------------------------------*/
    /* fUNCTIONALITY END*/
    

});

/*=============================================================================================================================*/
/* Edit Order Colntroller End*/


/* Add Quote Colntroller Start*/
/*=============================================================================================================================*/

app.controller('AddQuoteController', function($scope, $rootScope, $state, AppFactory, $ionicModal, $ionicLoading,$ionicPopup) 
{
  
    $scope.quote={};
  
    $scope.Data=AppFactory.getCustomerNameStatus();
    $scope.quote.entityvalue=$scope.Data.custName;
    $scope.quote.entity=($scope.Data.internalId).toString();
    $scope.quote.personaltype="estimate";
    //model window array list
    $scope.CustomForms=[];
    $scope.Statuses = [];
      
    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/

    var object=AppFactory.getQuoteData();
    console.log("Object:"+JSON.stringify(object));
    if(object.edited == "true")
    {
        $scope.quote=object;
    }

     //clear previous item data
    AppFactory.setItemData("");

    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.customerDetails',{category:AppFactory.getCategory()});
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {              
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
          title: 'Validation Error',
          content: message
        }).then(function(res) {

        });
    };


    /* RIGHT BUTTONS START*/ 
    $scope.saveData = function() 
    {
        if($scope.quote.trandate == undefined || $scope.quote.trandate == "")
        {
            $scope.showAlert("Please fill Date field");
            return;
        }

        if($scope.quote.customformvalue == undefined || $scope.quote.customformvalue == "")
        {
            $scope.showAlert("Please fill Custom Form field");
            return;
        }

        if($scope.quote.probability == undefined || $scope.quote.probability == "")
        {
            $scope.showAlert("Please fill Probability field");
            return;
        }

        if($scope.quote.expectedclosedate == undefined || $scope.quote.expectedclosedate == "")
        {
            $scope.showAlert("Please fill Expected Close Date field");
            return;
        }

        $scope.Flag="addQuote";
        AppFactory.setFlag($scope.Flag);

        //set quote object to factory for later use
        $scope.quote.edited="true";
        AppFactory.setQuoteData($scope.quote);

        $state.go('app.editItem');
    }
    /* RIGHT BUTTONS END*/ 
    /* ACTION BAR BUTTONS END*/

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('CustomForm.html', function(CustomForm) {
        $scope.CustomForm = CustomForm;
        }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });  
   
  
    $scope.fetchCustomFormList = function() 
    {   
        $scope.CustomForms=[];
        $scope.CustomForm.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching custom forms...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
       
        var parameters={
            "field_name":"customform",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"estimate",
            "field_dependent":"",
            "field_dependentvalue":""
                  };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.CustomForms.push(object);          
            }
            $ionicLoading.hide();
           
        }); //end of sendHttpRequest    
    };
    $scope.closeCustomFormModal = function() {
        $scope.CustomForm.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.CustomForm.remove();
    });
    $scope.setCustomForms=function(form)
    {
        $scope.quote.customformvalue=form.value;
        $scope.quote.customform=form.index;
        $scope.CustomForm.hide();
    }

    /* Status Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('Status.html', function(Status) {
        $scope.Status = Status;
         }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
   
    $scope.fetchStatusList = function() 
    {   
        $scope.Statuses=[];
        $scope.Status.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Status...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
       
        var parameters={
              "field_name":"entitystatus",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"estimate",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
                var objectArray=mainArray[obj];          
                //console.log(JSON.stringify(objectArray));
                var object={};
                object.index=objectArray[0];
                object.value=objectArray[1];
                $scope.Statuses.push(object);          
            }
            $ionicLoading.hide();
           
        }); //end of sendHttpRequest   
    };

    $scope.closeStatusModal = function() {
        $scope.Status.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.Status.remove();
    });

    $scope.setStatus=function(status)
    {
        $scope.quote.entitystatusvalue=status.value; 
        $scope.quote.entitystatus=status.index;
        $scope.Status.hide();
    }

    /*  Design Section END*/

    $scope.dateType="";
    /*------------------------- code for the "date" is started here----------------------------------*/
	$ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
	$scope.datemodal = modal;

    		},
    		{
    		// Use our scope for the scope of the modal to keep it simple
    		scope: $scope, 
    		// The animation we want to use for the modal entrance
    		animation: 'slide-in-up'

    		});		
    		$scope.$on('$destroy', function() 
    		{
    		    $scope.datemodal.remove();
    		});
		
		$scope.opendateModal = function(str) {		
			//console.log("Param:"+str);
			$scope.dateType=str;
			$scope.datemodal.show();
		};
		$scope.cancel = function()
		{
			$scope.datemodal.hide();
		};
		$scope.closedateModal = function(model) 
		{
			if(model == undefined || model == "")
			{
				$scope.showAlert("Please fill Date field");
			}
			else
			{
				$scope.datemodal.hide();
				$scope.date = model;
				  				
				var myDate = new Date($scope.date);
				var month = myDate.getMonth()+1;
				$scope.no1=month;
				
				var day = myDate.getDate();
				$scope.no=day;
				var year = myDate.getFullYear();
				
				$scope.no2=year;
				if($scope.dateType=="trandate"){
				$scope.quote.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
				}
				if($scope.dateType=="expectedclosedate"){
				$scope.quote.expectedclosedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
				}
				if($scope.dateType=="duedate"){
				$scope.quote.duedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
				}				
			}
		};
	
	/*------------------------- code for the "date" is ended here----------------------------------*/
	
    /* fUNCTIONALITY END*/

});

/*=============================================================================================================================*/
/* Add Quote Colntroller End*/


/* Edit Ouote Colntroller Start*/
/*=============================================================================================================================*/


app.controller('EditQuoteController', function($scope, $rootScope, $state, AppFactory, $ionicModal,$ionicPopup, $ionicLoading) 
{
    $scope.quote={};
    //model window array list
    $scope.CustomForms=[];
    $scope.Statuses = [];

    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/
    $rootScope.enableSideBar = false;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.quoteDetails');
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {
        $scope.showConfirm();    
    }
    
    /* LEFT BUTTONS END*/
    /* RIGHT BUTTONS START*/

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
          title: 'Validation Error',
          content: message
        }).then(function(res) {

        });
    };

    /* RIGHT BUTTONS START*/  
    $scope.saveData = function() 
    {
        if($scope.quote.trandate == undefined || $scope.quote.trandate == "")
        {
            $scope.showAlert("Please fill Date field");
            return;
        }

        if($scope.quote.customformvalue == undefined || $scope.quote.customformvalue == "")
        {
            $scope.showAlert("Please fill Custom Form field");
            return;
        }

        if($scope.quote.probability == undefined || $scope.quote.probability == "")
        {
            $scope.showAlert("Please fill Probability field");
            return;
        }

        if($scope.quote.expectedclosedate == undefined || $scope.quote.expectedclosedate == "")
        {
            $scope.showAlert("Please fill Expected Close Date field");
            return;
        }

        $scope.Flag="editQuote";
        AppFactory.setFlag($scope.Flag);

        //set Final Updated Quote object to factory
        AppFactory.setUpdatedQuoteData($scope.quote);

        $state.go('app.editItem');
    }//end of saveData()
   
    /* RIGHT BUTTONS END*/ 
    /* ACTION BAR BUTTONS END*/

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('CustomForm.html', function(CustomForm) {
        $scope.CustomForm = CustomForm;
        }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
        });

  
    $scope.fetchCustomFormList = function() {
        $scope.CustomForms=[];
        $scope.CustomForm.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching custom form...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        var parameters={
              "field_name":"customform",
              "field_type":"select",
              "field_subtext":"",
              "customer_type":"estimate",
              "field_dependent":"",
              "field_dependentvalue":""
                  };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.CustomForms.push(object);          
            }
            $ionicLoading.hide();
        }); //end of sendHttpRequest    
    };
    $scope.closeCustomFormModal = function() {
        $scope.CustomForm.hide();
    };
    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.CustomForm.remove();
    });

    $scope.setCustomForms=function(form)
    {
        $scope.quote.customformvalue=form.value;
        $scope.quote.customform=form.index;
        $scope.CustomForm.hide();
    }

    /* Status Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('Status.html', function(Status) {
        $scope.Status = Status;
        }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
  
  
    $scope.fetchStatusList = function() {
        $scope.Statuses = [];
        $scope.Status.show();
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching status list...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
        var parameters={
            "field_name":"entitystatus",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"estimate",
            "field_dependent":"",
            "field_dependentvalue":""
              };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
              var objectArray=mainArray[obj];          
             //console.log(JSON.stringify(objectArray));
              var object={};
              object.index=objectArray[0];
              object.value=objectArray[1];
              $scope.Statuses.push(object);          
            }
            $ionicLoading.hide();
        }); //end of sendHttpRequest

    };
    $scope.closeCustomFormModal = function() {
        $scope.Status.hide();
    };
    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.Status.remove();
    });
    $scope.setStatus=function(status)
    {
        $scope.quote.entitystatusvalue=status.value; 
        $scope.quote.entitystatus=status.index;
        $scope.Status.hide();
    }

    /*  Design Section END*/   
    //Existing data 
    //get opportunity data object from factory which was set in quotedetails controller
 
    $scope.quote.personaltype="estimate";
    $scope.quote.entity=AppFactory. getItemId();
    $scope.quote.internalid=AppFactory. getItemId();

    if(AppFactory.getFlag()!="editQuote")
    {

        //get from fctory which was set at quotedetails controller
        $scope.quoteObj=AppFactory.getQuoteEdit();  
        $scope.headerArray=$scope.quoteObj.header;
        $scope.primaryInfoArray=$scope.quoteObj.primary;
      
        for(obj in $scope.headerArray)
        {
              var data=$scope.headerArray[obj];
              for(key in data)
              {
                 // console.log(JSON.stringify(data[key]));
                 if(data[key]=="Name")
                 {
                  $scope.quote.entityvalue=data.value;
                  //console.log("CN:"+data.value);
                 }
                 else if(data[key]=="Memo")
                 {
                  $scope.quote.memo=data.value;
                  //console.log("CN:"+data.value);
                 }
              }
        }




        for(obj in $scope.primaryInfoArray)
        {
              var data=$scope.primaryInfoArray[obj];
              for(key in data)
              {
                 // console.log(JSON.stringify(data[key]));
                 if(data[key]=="Date")
                 {
                  //$scope.quote.trandate=data.value;
        		  var myDate = new Date(data.value);
        					 var month = myDate.getMonth()+1;
        						console.log(" month is "+ month);
        						$scope.no1=month;
        						
        						var day = myDate.getDate();
        						console.log(" day is "+ day);
        						$scope.no=day;
        						var year = myDate.getFullYear();
        						console.log(" year is "+ year);
        						$scope.no2=year;
        						$scope.quote.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
        						console.log("existing date="+$scope.quote.trandate);
                  //console.log("Title:"+data.value);
                 }
                 else if(data[key]=="Custom Form")
                 {
                  $scope.quote.customform=data.index;
                  $scope.quote.customformvalue=data.value;
                  console.log("Custom Form index:"+data.index);
                 }
                 else if(data[key]=="Status")
                 {
                  $scope.quote.entitystatus=data.index;
                  $scope.quote.entitystatusvalue=data.value;
                  console.log("Status index:"+data.index);
                 }
                 else if(data[key]=="Probability")
                 {
                  $scope.quote.probability=data.value;
                  //console.log("Sales Rep:"+data.value);
                 }
                 else if(data[key]=="Expected Close")
                 {
                    //$scope.quote.expectedclosedate=data.value;
        		    var myDate = new Date(data.value);
    					 var month = myDate.getMonth()+1;
    						console.log(" month is "+ month);
    						$scope.no1=month;
    						
    						var day = myDate.getDate();
    						console.log(" day is "+ day);
    						$scope.no=day;
    						var year = myDate.getFullYear();
    						console.log(" year is "+ year);
    						$scope.no2=year;
    						$scope.quote.expectedclosedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    						console.log("existing date="+$scope.quote.expectedclosedate);
                  //console.log("Date:"+data.value);
                 }
                 else if(data[key]=="Due Date/Receive By")
                 {
                    // $scope.quote.duedate=data.value;
            		var myDate = new Date(data.value);
    					var month = myDate.getMonth()+1;
    					console.log(" month is "+ month);
    					$scope.no1=month;
    					
    					var day = myDate.getDate();
    					console.log(" day is "+ day);
    					$scope.no=day;
    					var year = myDate.getFullYear();
    					console.log(" year is "+ year);
    					$scope.no2=year;
    					$scope.quote.duedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    					console.log("existing date="+$scope.quote.duedate);
                        //console.log("Date:"+data.value);
                }           
            }
        }
        

    }//end if
    else
    {
        $scope.quote=AppFactory.getUpdatedQuoteData();
    }




    $scope.dateType="";
    /*------------------------- code for the "date" is started here----------------------------------*/
	$ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
		$scope.datemodal = modal;

		},
		{
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope, 
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'

		});
		
		$scope.$on('$destroy', function() 
		{
		    $scope.datemodal.remove();
		});
		
		$scope.opendateModal = function(str) {
		
			//console.log("Param:"+str);
			$scope.dateType=str;
			$scope.datemodal.show();
		};
		$scope.cancel = function()
		{
			$scope.datemodal.hide();
		};
		$scope.closedateModal = function(model) 
		{
		   if(model == undefined || model == "")
			{
				$scope.datemodal.hide();
			}
			else
			{
			    $scope.datemodal.hide();
			    $scope.date = model;
			  			
    			var myDate = new Date($scope.date);
    			var month = myDate.getMonth()+1;
    			$scope.no1=month;
    			
    			var day = myDate.getDate();
    			$scope.no=day;
    			var year = myDate.getFullYear();
    			
    			$scope.no2=year;
    			if($scope.dateType=="trandate"){
    			$scope.quote.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    			}
    			if($scope.dateType=="expectedclosedate"){
    			$scope.quote.expectedclosedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    			}
    			if($scope.dateType=="duedate"){
    			$scope.quote.duedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
    			}				
		    }
		};
	
	/*------------------------- code for the "date" is ended here----------------------------------*/
	
    /* fUNCTIONALITY END*/


});

/*=============================================================================================================================*/
/* Edit Quote Colntroller End*/

/* Add Item page Colntroller Start*/
/*=============================================================================================================================*/

app.controller('AddItemController', function($scope,$ionicModal, $rootScope, $state,AppFactory, $ionicLoading,$ionicPopup) 
{  
    $scope.item={};
    //item array list declaration
     $scope.Items = [];
    /* Item List*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('ItemModal.html', function(ItemModal) {
        $scope.ItemModal = ItemModal;
        }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });  
  
   
    $scope.fetchItemList = function() 
    {   
        $scope.Items = [];
        $scope.ItemModal.show();
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Items...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });   
        
        var parameters={
            "field_name":"item",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"inventoryadjustment",
            "field_dependent":"",
            "field_dependentvalue":""
                  }; 
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) { //get result from json

            var mainArray=results;
            for(obj in mainArray)
            {
                var objectArray=mainArray[obj];          
                //console.log(JSON.stringify(objectArray));
                var object={};
                object.index=objectArray[0];
                object.value=objectArray[1];
                object.rate=objectArray[2];          
                $scope.Items.push(object);          
            }
            $ionicLoading.hide();
            
        }); //end of sendHttpRequest
        //populate the drop-down window    
    };
    $scope.closeItemModal = function() {
        $scope.ItemModal.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() {
        $scope.ItemModal.remove();
    });
    $scope.setItem=function(item1)
    {
        $scope.item.itemvalue=item1.value;
        $scope.item.item=item1.index;
        $scope.rate=item1.rate;
        $scope.item.rate=item1.rate; 

        $scope.ItemModal.hide();
    }
    $scope.setAmount=function()
    {
        //console.log("QTY:"+$scope.item.quantity);
        $scope.item.amount=(parseFloat($scope.item.rate)*parseFloat($scope.item.quantity)).toString();
    }

    $scope.item.custcol_custtaxamt="0.00";

    /* Design Section START */
    /* SIDE MENU SECTION -- HIDDEN*/
    $rootScope.enableSideBar = false;
    
    /* ACTION BAR BUTTONS START*/
    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.editItem');
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };


    $scope.goBack = function()
    {
        $scope.showConfirm();
    }
    /* LEFT BUTTONS START*/

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
          title: 'Validation Error',
          content: message
        }).then(function(res) {

        });
    };

    $scope.saveData = function()
    {
        if($scope.item.itemvalue == undefined || $scope.item.itemvalue == "")
        {
            $scope.showAlert("Please fill Item field");
            return;
        }
        //set item object to factory         
         $scope.temp=AppFactory.getItemData();

         if(AppFactory.getFlag() == "editTransaction" || AppFactory.getFlag() == "editQuote" || AppFactory.getFlag() == "editOrder")
         {
            $scope.item.line="0";
         }
         else
         {
            $scope.item.line=($scope.temp.length+1).toString();
         }
        AppFactory.setItemData($scope.item);
        $state.go('app.editItem');
    }//end of saveData()
   
    /* fUNCTIONALITY END*/


});

/*=============================================================================================================================*/
/* Add Item page Colntroller End*/


/* Edit Item Colntroller Start*/
/*=============================================================================================================================*/

app.controller('EditItemController', function($scope, $rootScope, $state, AppFactory,$ionicPopup, $ionicLoading) 
{
  
    //get Item object from factory
    $scope.Item=[];  
    $scope.Item=AppFactory.getItemData();

    /* Design Section START */ 

    /* SIDE MENU SECTION -- HIDDEN*/
    $rootScope.enableSideBar = false;
    $scope.Flag = AppFactory.getFlag();

    $scope.goTo = function() 
    {   
        AppFactory.setItemData("");//set empty Item list when press back button
        $state.go('app.'+$scope.Flag);
    }

    //custome function for display pop-up message window
    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

            });
    };


    $scope.saveData = function() 
    {
        if($scope.Item.length == 0)
        {               
            $scope.showAlert("Please add Item to continue");
            return;
        }
        $state.go('app.summary');
    }

    $scope.show = function()
    {
      $state.go('app.addItem');
    }

    /***************START CODE FOR DELETE checked selected Items ***************************/

    $scope.selectedItemsId=[];
    $scope.counter=0;   
    $scope.selectItems=function(id,itemvalue)
    {        
        if(itemvalue==true)
        {               
            $scope.counter+=1;
            $scope.selectedItemsId.push(id);           
            //console.log("After Add:"+$scope.selectedItemsId);            
        }   

        if(itemvalue==false)
        {   
            $scope.counter-=1;           
            for(var i=0;i<$scope.selectedItemsId.length;i++)
            {
                //console.log("All Id's"+$scope.selectedItemsId[i]);
                if( $scope.selectedItemsId[i]==id)
                {
                $scope.selectedItemsId.splice(i, 1);
                //console.log("After delete:"+$scope.selectedItemsId);
                }
            }                   
        }
        
        //console.log("ID's:"+$scope.selectedItemsId);
    }//end of selectItems()

    $scope.showFinalList=function()
    {
        $ionicPopup.confirm({
                title: 'Alert',
                content: 'Do you want to delete Item?'
                }).then(function(res) {
                    if(res) 
                    {
                        for(var i=0;i<$scope.Item.length;i++)
                        {
                            for(var j=0;j<$scope.selectedItemsId.length;j++)
                            {
                                if($scope.Item[i].item==$scope.selectedItemsId[j])
                                {
                                    $scope.Item.splice(i, 1);
                                    $scope.counter-=1
                                }
                            }                     
                        }
                    } 
                    else 
                    {
                        console.log('You are not sure');
                        $scope.itemvalue=false;
                    }
        });

        //console.log("Final Item Array:;"+JSON.stringify($scope.Item));

    }//end of showFinalList()

    /************************************************CODE END OF ITEM DELETE **********/
    /*  Design Section END*/

    //send request for display existing items list
    if((AppFactory.getFlag() == "editTransaction" &&  $scope.Item.length==0) || (AppFactory.getFlag() == "editQuote" &&  $scope.Item.length==0) || (AppFactory.getFlag() == "editOrder" &&  $scope.Item.length==0))
    {   
        //fetch id from factory which item we have clicked from list
        var internalId=AppFactory.getItemId();
        console.log("Internalid:"+internalId);        
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching items...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        var parameters={
            "id":"customsearch_aavz_23",
            "noofrecords":"100",
            "internalid":internalId,  
            "customer_type":"inventoryadjustment"
                    };  

        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=113&deploy=1",null,parameters,function(results) { //get result from json
            console.log("Existing Item Data:"+JSON.stringify(results));
            var mainArray=results;
            for(item in mainArray)
            {
                var innerObject=mainArray[item];
                for(obj in  innerObject)
                {
                    var itm = innerObject[obj];
                    itm.custcol_custtaxamt = "";
                    $scope.Item.push(itm);
                    //console.log("OBJ:"+JSON.stringify(innerObject[obj]));  
                }
            }
            $ionicLoading.hide();
            //console.log("Item:"+JSON.stringify($scope.Item)); 
        });//end of sendHttpRequest
    }//if end


    //function to display message if item is not selected
    $scope.messageFlag="false";
    $scope.message="";    
    if((AppFactory.getFlag()=="addOpportunity") && ($scope.Item.length==0))
    {   
        $scope.messageFlag="true";
        $scope.message="Please tap the + button to add items to the OPPORTUNITY.";            
    }
    else if((AppFactory.getFlag()=="addQuote") && ($scope.Item.length==0))
    {
        $scope.messageFlag="true";
        $scope.message="Please tap the + button to add items to the QUOTE.";
    }
    else if((AppFactory.getFlag()=="addOrder") && ($scope.Item.length==0))
    {
        $scope.messageFlag="true";
        $scope.message="Please tap the + button to add items to the ORDER.";
    }
    else
    {
        $scope.messageFlag="false";
    }

   console.log("Item length:"+$scope.Item.length);
    /* fUNCTIONALITY END*/

});


/*=============================================================================================================================*/
/* Edit Item Colntroller END*/

/* SUMMARY Colntroller Start*/
/*=============================================================================================================================*/

app.controller('SummaryController', function($scope, $rootScope, $state, AppFactory,$ionicLoading ,$ionicPopup) 
{   
    /*var screen = cordova.plugins.screenorientation;
    screen.setOrientation(screen.Orientation.LANDSCAPE);*/
    //get objects from factory if Add/Edit opportunity
    $scope.opportunityData=[]; 
    //for disp opportunity data on summery page array name must be as in add opportunity time 
    $scope.opportunityArray={};  
    //console.log("Summary page data:"+JSON.stringify($scope.opportunityArray));

    if(AppFactory.getFlag() == "addOpportunity")
    {
        $scope.opportunityData.push(AppFactory.getOpportunityData());
        $scope.opportunityArray=AppFactory.getOpportunityData();
        //console.log("AddNew time:"+JSON.stringify($scope.opportunityData));
    }
  
    if(AppFactory.getFlag() == "addOrder")
    {    
        $scope.opportunityData.push(AppFactory.getOrderData());
        $scope.opportunityArray=AppFactory.getOrderData();
        //console.log("AddNew time:"+JSON.stringify($scope.opportunityData));
    }

    if(AppFactory.getFlag() == "editOrder")
    {    
        $scope.opportunityData.push(AppFactory.getUpdatedOrderData());
        $scope.opportunityArray=AppFactory.getUpdatedOrderData();    
    }

    if(AppFactory.getFlag() == "editTransaction")
    {
        $scope.opportunityData.push(AppFactory.getUpdatedOpportunityData());
        $scope.opportunityArray=AppFactory.getUpdatedOpportunityData();
        console.log("Edit time:"+JSON.stringify($scope.opportunityData));
    }
  
    if(AppFactory.getFlag() == "addQuote")
    {    
        $scope.opportunityData.push(AppFactory.getQuoteData());
        $scope.opportunityArray=AppFactory.getQuoteData();
        //console.log("AddNew time:"+JSON.stringify($scope.opportunityData));
    }

    if(AppFactory.getFlag() == "editQuote")
    {    
        $scope.opportunityData.push(AppFactory.getUpdatedQuoteData());
        $scope.opportunityArray=AppFactory.getUpdatedQuoteData();
        console.log("AddNew time:"+JSON.stringify($scope.opportunityData));
    }
 
    //get Array of object item new added
    $scope.itemData=AppFactory.getItemData();
    //console.log("Item:"+JSON.stringify($scope.itemData));

    //read item object 1 by 1 from $scope.itemData Array for sending request puspose
    for(obj in $scope.itemData)
    {
        $scope.opportunityData.push($scope.itemData[obj]);
    }  
    //console.log("Opportunity Final array:"+JSON.stringify($scope.opportunityData));
  
    /* Design Section START */ 
    /* SIDE MENU SECTION -- HIDDEN*/
    $rootScope.enableSideBar = false;
    $scope.title = AppFactory.getFlag();
    console.log("Title:"+$scope.title);

    //console.log("title:"+$scope.title);
    if($scope.title == "AddOrder" || $scope.title == "EditOrder")
    {
        $scope.pageTitle = "ORDER SUMMARY";
    }else if($scope.title == "AddQuote" || $scope.title == "EditQuote")
    {
        $scope.pageTitle = "QUOTE SUMMARY";
    }else if($scope.title == "AddOpportunity" || $scope.title == "EditTransaction")
    {
        $scope.pageTitle = "OPPORTUNITY SUMMARY";
    }
    
    /* ACTION BAR BUTTONS START*/
    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
    {
        $state.go('app.editItem');
        //screen.setOrientation(screen.Orientation.PORTRAIT);
    }
    /* LEFT BUTTONS END*/
    /* RIGHT BUTTONS START*/  

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    $scope.saveData = function() 
    {
        //Call the Web service for edit opportunity
        if(AppFactory.getFlag() == "editTransaction")
        {
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",$scope.opportunityData,null,function(results) 
            {   
                //get result from json                 
                  console.log("Response Data:"+JSON.parse(results));
                  //To navigate the OPPORTUNITY list page  
                  if(results =="\"Record edited successfully.\"")
                    {
                        window.plugins.toast.show("Record edited successfully","short","center");
                        AppFactory.setCategory('OPPORTUNITY');                        
                        $state.go('app.transaction',{category:AppFactory.getCategory()});
                        //screen.setOrientation(screen.Orientation.PORTRAIT);
                    }
                    else
                    {
                         $scope.showAlert(results);
                         return;
                    }                    
            $ionicLoading.hide();        
            });//end of send http request}
        }
        else if(AppFactory.getFlag() == "addOpportunity")
        {
            //Call the Web service for adding New opportunity                   
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",$scope.opportunityData,null,function(results) 
            {   
                //get result from json                  
                console.log("Response Data:"+JSON.stringify(results));
                if(results =="\"Record saved successfully.\"")
                {   
                    //window.plugins.toast.show("Record saved successfully","short","center");                    
                    AppFactory.setCategory('OPPORTUNITY');                                            
                    $state.go('app.transaction',{category:AppFactory.getCategory()});
                    //screen.setOrientation(screen.Orientation.PORTRAIT);
                }
                else
                {
                    $scope.showAlert(results);
                    return;
                }
            $ionicLoading.hide();               
            });//end of send http request}
        } 
        else if(AppFactory.getFlag() == "addQuote")
        {
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",$scope.opportunityData,null,function(results) 
            {   
                //get result from json                          
                console.log("Response Data:"+JSON.stringify(results));
                if(results =="\"Record saved successfully.\"")
                {
                    window.plugins.toast.show("Record saved successfully","short","center");
                    AppFactory.setCategory('QUOTE');                        
                    $state.go('app.transaction',{category:AppFactory.getCategory()});
                    //screen.setOrientation(screen.Orientation.PORTRAIT);
                }
                else
                {
                    $scope.showAlert(results);
                    return;
                }
            $ionicLoading.hide();                           
            });//end of send http request  
        }
        else if(AppFactory.getFlag() == "addOrder")
        {
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",$scope.opportunityData,null,function(results) 
            {
                console.log("Response:"+JSON.stringify(results));
                //get result from json
                if(results =="\"Record saved successfully.\"")
                {
                    window.plugins.toast.show("Record saved successfully","short","center");
                    AppFactory.setCategory('ORDER');                        
                    $state.go('app.transaction',{category:AppFactory.getCategory()});
                    //screen.setOrientation(screen.Orientation.PORTRAIT);

                }
                else
                {  
                    $scope.showAlert(results);
                    return;
                }
            $ionicLoading.hide();                        
            });//end of send http request  
        }

        else if(AppFactory.getFlag() == "editOrder")
        {
            //Call the Web service for Edit order
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",$scope.opportunityData,null,function(results) 
            {   
                //get result from json                          
                if(results =="\"Record edited successfully.\"")
                {
                    window.plugins.toast.show("Record edited successfully","short","center");
                    AppFactory.setCategory('ORDER');                        
                    $state.go('app.transaction',{category:AppFactory.getCategory()});
                    //screen.setOrientation(screen.Orientation.PORTRAIT);

                }
                else
                {   
                    $scope.showAlert(results);
                    return;
                }
            $ionicLoading.hide();                          
            });//end of send http request  
        }
        else if(AppFactory.getFlag() == "editQuote")
        {
            //Call the Web service for Edit Quote
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=108&deploy=1",$scope.opportunityData,null,function(results) 
            {  
                //get result from json                          
                console.log("Response Data:"+JSON.stringify(results));
                //To navigate the OPPORTUNITY list page                           
                if(results =="\"Record edited successfully.\"")
                {
                    window.plugins.toast.show("Record edited successfully","short","center");
                    AppFactory.setCategory('QUOTE');                        
                    $state.go('app.transaction',{category:AppFactory.getCategory()});
                    //screen.setOrientation(screen.Orientation.PORTRAIT);
                }
                else
                {   
                    $scope.showAlert(results);
                    return;
                }
            $ionicLoading.hide();             
            });//end of send http request  
        }

    }//END OF saveData()    

    $scope.show = function()
    {
        $state.go('app.editItem');
    }

    /* fUNCTIONALITY END*/

});

/*=============================================================================================================================*/
/* SUMMARY Colntroller End*/


/* Item Details Colntroller Start*/
/*=============================================================================================================================*/

app.controller('ItemDetailsController', function($scope, $rootScope, $state, AppFactory,$ionicLoading) 
{
    $scope.showPage = false;

    $scope.projectedTotal = "";
    $scope.weightedAmount = "";
    $scope.amount = "";
    $scope.amountTasTotal = "";
    $scope.amountShipping = "";

    /* Design Section START */    
    $rootScope.enableSideBar = false;
    $scope.page = AppFactory.getCategory();
    /* ACTION BAR BUTTONS START*/

    $scope.valueArray = [];
    var internalId=AppFactory.getItemId(); 
    $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
    //parameters to send with the requrest of getting data of case list
    var parameters =
        { 
        "id":"customsearch_aavz_23",
         "noofrecords":"100",
         "internalid":internalId,
         "customer_type":"inventoryadjustment"  
        };   
   
    // sending the request to the server to get data 
    AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=113&deploy=1",null,parameters,function(results)
    { 
        for(mainArray in  results)
        {
            obj =  results[mainArray];
            for(objKey in obj)
            {     
                var objValue = obj[objKey];
                objValue.id=objKey;
                // pushing the case data into the valuearray
                $scope.valueArray.push(objValue);                      
            }
        }
        $scope.showPage = true;
        $ionicLoading.hide();
    
       // console.log("result of item list is =="+JSON.stringify($scope.valueArray));
    });

    
    if($scope.page == "OPPORTUNITY")
    {
        $scope.transactionObj=AppFactory.getOpportunityEditData();
        $scope.headerArray=$scope.transactionObj.header;
        $scope.forecastTypeArray=$scope.transactionObj.forecasttype;  
        //console.log("Data : "+JSON.stringify($scope.forecastTypeArray));

        for(obj in $scope.headerArray)
        {
            var data=$scope.headerArray[obj];
            for(key in data)
            {
                // console.log(JSON.stringify(data[key]));
                if(data[key]=="Projected Total")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.projectedTotal = data.value;
                    console.log("CN:"+data.value);
                }
             }
        } 

        for(obj in $scope.forecastTypeArray)
        {
            var data=$scope.forecastTypeArray[obj];
            for(key in data)
            {
                // console.log(JSON.stringify(data[key]));
                if(data[key]=="Weighted Total")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.weightedAmount = data.value;
                    console.log("CN:"+data.value);
                }
             }
        } 

    }
    else if($scope.page == "QUOTE")
    {
        $scope.transactionObj=AppFactory.getQuoteEdit();
        $scope.amountArray=$scope.transactionObj.totalAmt;
        console.log("Quote Data : "+JSON.stringify($scope.amountArray));

        for(obj in $scope.amountArray)
        {
            var data=$scope.amountArray[obj];
            for(key in data)
            {
                // console.log(JSON.stringify(data[key]));
                if(data[key]=="Amount")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.amount = data.value;
                    //console.log("Projected Total:"+data.value);
                }
                else if(data[key]=="Amount (Tax Total)")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.amountTaxTotal = data.value;
                    //console.log("Projected Total:"+data.value);
                }
                else if(data[key]=="Amount (Shipping)")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.amountShipping = data.value;
                    //console.log("Projected Total:"+data.value);
                }

             }
        } 
    }
    else if($scope.page == "ORDER")
    {
        $scope.transactionObj=AppFactory.getOrderEditData();
        $scope.amountArray=$scope.transactionObj.totalAmt;
        //console.log("Data : "+JSON.stringify($scope.forecastTypeArray));
        for(obj in $scope.amountArray)
        {
            var data=$scope.amountArray[obj];
            for(key in data)
            {
                // console.log(JSON.stringify(data[key]));
                if(data[key]=="Amount")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.amount = data.value;
                    //console.log("Projected Total:"+data.value);
                }
                else if(data[key]=="Amount (Tax Total)")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.amountTaxTotal = data.value;
                    //console.log("Projected Total:"+data.value);
                }
                else if(data[key]=="Amount (Shipping)")
                {
                    //$scope.opportunityObject.entityvalue=data.value;
                    $scope.amountShipping = data.value;
                    //console.log("Projected Total:"+data.value);
                }

             }
        } 
    }
    
    var previousPage=AppFactory.getType();
    console.log("previous Page:"+previousPage);
    $scope.goBack = function()
    {
        if(previousPage=="ORDER")
        {   
            $scope.page=previousPage;
        }
        if(previousPage=="QUOTE")
        {   
            $scope.page=previousPage;
        }


        if($scope.page == "OPPORTUNITY")
        {
            $state.go('app.transactionDetails');
        }
        else if($scope.page == "QUOTE")
        {
            $state.go('app.quoteDetails');
        }
        else if($scope.page == "ORDER")
        {
            $state.go('app.orderDetails');
        }
    }

    /* fUNCTIONALITY END*/

});

/*=============================================================================================================================*/
/* Item Details Colntroller End*/

