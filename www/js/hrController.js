/*==============================================================================================================================================================*/
/* Timesheet Controller Start */
/*==============================================================================================================================================================*/

app.controller('TimesheetController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory ,$ionicScrollDelegate) 
{
	// title to the page ..
    $scope.pageTitle="TIMESHEET LIST";

	// hiding sidebar...
    $rootScope.sideMenuEnabled = true;

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value,item)
    {
        //send selected item to factory
        AppFactory.setStartEndDate(item);
        $state.go('app.'+value);
    }

    $scope.init=function(){
        $ionicScrollDelegate.scrollTop();
        $scope.TimeSheetList=[];
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

    $scope.day="";
    var todate;
    var fromdate;

    $scope.weekStartDate="";
    $scope.weekStartDateArray=[];
    $scope.weekEndDateArray=[];
    $scope.dateDurationArray=[];
  

    //get today's date    
    var d = new Date(); // today!
    switch (d.getDay()) {
        case 0:
            $scope.day = "Sunday";
            d.setDate((d.getDate()-6));
            break;
        case 1:
            $scope.day = "Monday";        
            break;
        case 2:
            $scope.day = "Tuesday";
            d.setDate((d.getDate()-1));     
            break;
        case 3:
            $scope.day = "Wednesday";
            d.setDate((d.getDate()-2));     
            break;
        case 4:
            $scope.day = "Thursday";
            d.setDate((d.getDate()-3));     
            break;
        case 5:
            $scope.day = "Friday";
            d.setDate((d.getDate()-4));     
            break;
        case 6:
            $scope.day = "Saturday";
            d.setDate((d.getDate()-5));
            break;
    }

    //set Finally week start date of first/current week
    var currentWeekDate=d.getDate();
    var currentWeekMonth=(d.getMonth()+1);
    if(currentWeekDate < 10)
    {
        currentWeekDate=("0"+currentWeekDate);
    }
    if(currentWeekMonth < 10)
    {
         currentWeekMonth=("0"+currentWeekMonth); 
    }
    $scope.weekStartDate=currentWeekMonth+"/"+currentWeekDate+"/"+d.getFullYear();
    
    //first/current week start date
    $scope.weekStartDateArray.push($scope.weekStartDate);

    //first/current week end date
    var dt = new Date(d);    
    dt.setDate((dt.getDate()+6));

    var currentWeekedDate=dt.getDate();
    var currentWeekedMonth=(dt.getMonth()+1);
    if(currentWeekedDate < 10)
    {
        currentWeekedDate=("0"+currentWeekedDate);
    }
    if(currentWeekedMonth < 10)
    {
         currentWeekedMonth=("0"+currentWeekedMonth); 
    }
    var currentWeekEndDate=currentWeekedMonth+"/"+currentWeekedDate+"/"+dt.getFullYear();
    //console.log("currentWeekEndDate:"+currentWeekEndDate);
    $scope.weekEndDateArray.push(currentWeekEndDate);
    //store todate of weeks range   
    todate=currentWeekEndDate;


    ///////////////////////Code for form week////////////
    for(var i=0;i<23;i++)
    {
        var d1 = new Date($scope.weekStartDate); // today!        
        var x = (i+1)*7; // go back 5 days!
        d1.setDate(d1.getDate() - x); 
        var date=d1.getDate();
        var month=(d1.getMonth()+1); 

        if(date < 10)
        {
            date=("0"+date);
        }
        if(month < 10)
        {
             month=("0"+month); 
        }
        var startDate=(month+"/"+date+"/"+d1.getFullYear()); 
        $scope.weekStartDateArray.push(startDate);

        //code for weekEndDate array
        var d2=new Date(d1);         
        //go back 5 days!
        d2.setDate((d2.getDate()+6));
        var date2=d2.getDate();
        var month2=(d2.getMonth()+1);
        if(date2 < 10)
        {
            date2=("0"+date2);
        }
        if(month2 < 10)
        {
             month2=("0"+month2); 
        }
        var endDate=month2+"/"+date2+"/"+d2.getFullYear();
        $scope.weekEndDateArray.push(endDate);
    }
    //console.log("weekEndDateArray:"+JSON.stringify($scope.weekEndDateArray));

    //store fromdate of weeks range
    var myDate=$scope.weekStartDateArray[23];
    var newdate=new Date(myDate);
    fromdate=((newdate.getMonth()+1)+"/"+(newdate.getDate()-1)+"/"+newdate.getFullYear());
    
    //call web-service for get date and duration
    var employeevalue;//used in add Timesheet
    var employee;

    console.log("fromdate:"+fromdate);
    console.log("todate:"+todate);
    var obj={};
    obj.fromdate=fromdate;
    obj.todate=todate;
    //set from and todate used in next timesheetlist controller
    AppFactory.setFromAndTodate(obj);
    
        var parameters={
          "id":"customsearch_aavz_35",
          "customer_type":"timebill",
          "noofrecords":"100",
          "fromdate":fromdate,
          "todate":todate   
              };  

        //Request for get data
        AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=110&deploy=1",parameters,null,function(results)  
    	{ 
    			//console.log("Response:"+JSON.stringify(results));
                var mainArray=results;

    			for(item in mainArray)
    			{
    				var innerObject=mainArray[item];    
    				for(key in innerObject)
    				{
    					//set Employee value and id
    					if(key=="employee")
    					{
    					employeevalue=innerObject[key];
    					}
    					else
    					{
    					employee=key;
    					}
    					//console.log("key:"+key);
    					var object=innerObject[key];
    					//console.log("Object:"+JSON.stringify(object));
    					$scope.dateDurationArray.push(object);
    				}

    			}
                //console.log("dateDurationArray:"+JSON.stringify($scope.dateDurationArray));
    			//set complete array to factory for used on TimeSheetListcontroller
    			//AppFactory.setDateDurationArray( $scope.dateDurationArray);

    			$scope.durationTotal=0;
    			$scope.TimeSheetList=[];
    			$scope.minutes=0;
    			$scope.hours=0;
    	  
    	  //Now Prepare Final Object of TimeSheet
    	  
    		for(var k=0;k<$scope.weekStartDateArray.length;k++)
    		{
    			$scope.durationTotal=0;
    			$scope.minutes=0;
    			$scope.hours=0;
    			
    			var d1 = Date.parse($scope.weekStartDateArray[k]);
    			var d2 = Date.parse($scope.weekEndDateArray[k]);

    			for(var i=0;i<$scope.dateDurationArray.length-1;i++)
    			{ 
    				var date=$scope.dateDurationArray[i].date;
    				var duration=$scope.dateDurationArray[i].duration;
    				var d3 = Date.parse(date);

    				if ( (d3>=d1) && (d3<=d2) )
    				{
    					//convert minutes into hrs.
    					var res = duration.split(":");
    					$scope.minutes+=parseInt(res[1]);
    					$scope.hours+=parseInt(res[0]);       
    				}
    			}
    			$scope.hours+=parseInt($scope.minutes/60);
    			$scope.minutes=($scope.minutes%60);

                if($scope.hours < 10 )
                {
                    $scope.hours=("0"+$scope.hours);                
                }
                if($scope.minutes < 10 )
                {
                    $scope.minutes=("0"+$scope.minutes);                
                }

    			//final total duration of the week  
    			$scope.durationTotal=($scope.hours+":"+$scope.minutes);
    		    //console.log("totalduration:"+$scope.durationTotal);
    			//prepare final object and put into an array
    			var object={};
    			object.startdate=$scope.weekStartDateArray[k];
    			object.enddate=$scope.weekEndDateArray[k];            
    			object.totalduration=$scope.durationTotal;
    			$scope.TimeSheetList.push(object);

    		}//end of main for

    		//employeevalue and employee set to factory used while add timesheet
    		var emp={};
    		emp.employeevalue=employeevalue;
    		emp.employee=employee;
    		AppFactory.setEmployeeIdValue(emp);

        $ionicLoading.hide();
        });//end of sendHttpRequest

    }//end of init()
});

/*==============================================================================================================================================================*/
/* Timesheet Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* TimesheetList Controller Start */
/*==============================================================================================================================================================*/

app.controller('TimesheetListController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, AppFactory) 
{
    //get selected item start-end date and total duration of week
        $scope.item="";
        $scope.weekDateDurationArray=[];
        $scope.TimeSheetListArray=[];
        $scope.weekDatesArray=[];
        $scope.hours=0;
        $scope.minutes=0;
        $scope.durationTotal=0;
        $scope.day="";
        $scope.item=AppFactory.getStartEndDate();

	$scope.PageTitle="TIMESHEET LIST";
    $scope.sortText="";

    // hiding sidebar...
    $rootScope.sideMenuEnabled = false;

    /* PAGE CATEGORY FOR --PAGE TITLE*/
    $scope.pageTitle = $stateParams.category;
    AppFactory.setCategory($stateParams.category);

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value,item)
    {   
        AppFactory.setType('TimesheetList');
        //set selected date and day to factory
        AppFactory.setDayDate(item);
        $state.go('app.'+value);
    }

    /* LEFT BUTTONS START*/
    $scope.goBack = function() 
    {
        $scope.gotoPage('Timesheet');
    }/* LEFT BUTTONS END*/


       
    	//get selected Array start-end date and total duration of week
    	//$scope.weekDateDurationArray=AppFactory.getDateDurationArray();
        //console.log("weekDateDurationArray:"+JSON.stringify($scope.weekDateDurationArray));
    var obj=AppFactory.getFromAndTodate();    
    //Array start-end date and total duration of week days       
    $scope.init=function()
    {   
        $scope.weekDateDurationArray=[];
        $scope.TimeSheetListArray=[];
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        }); 
        var parameters={
          "id":"customsearch_aavz_35",
          "customer_type":"timebill",
          "noofrecords":"100",
          "fromdate":obj.fromdate,
          "todate":obj.todate   
              };  

        //Request for get data
        AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=110&deploy=1",parameters,null,function(results)  
        { 
                //console.log("Response:"+JSON.stringify(results));
                var mainArray=results;

                for(item in mainArray)
                {
                    var innerObject=mainArray[item];    
                    for(key in innerObject)
                    {
                        //set Employee value and id
                        if(key=="employee")
                        {
                        employeevalue=innerObject[key];
                        }
                        else
                        {
                        employee=key;
                        }
                        //console.log("key:"+key);
                        var object=innerObject[key];
                        //console.log("Object:"+JSON.stringify(object));
                        $scope.weekDateDurationArray.push(object);
                    }
                }
            //console.log("weekDateDurationArray:"+JSON.stringify($scope.weekDateDurationArray));
                var startDate=$scope.item.startdate;
        var endDate=$scope.item.enddate;

        //create week dates and push into an array
        for(var i=0;i<7;i++)
        {
            var d=new Date(startDate);
            d.setDate(d.getDate()+i);
            var weekDates=(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear();
            $scope.weekDatesArray.push(weekDates);
        }

       for(var j=0;j<$scope.weekDatesArray.length;j++)
        {
            $scope.hours=0;
            $scope.minutes=0;
            $scope.durationTotal=0;
            $scope.day="";
            var date1=$scope.weekDatesArray[j];
            for(var k=0;k<$scope.weekDateDurationArray.length-1;k++)
            {
                var date2=$scope.weekDateDurationArray[k].date;
                var duration=$scope.weekDateDurationArray[k].duration;
                if(date1==date2)
                {
                    //convert minutes into hrs.
                    var res = duration.split(":");
                    $scope.minutes+=parseInt(res[1]);
                    $scope.hours+=parseInt(res[0]); 
                }
            }
            $scope.hours+=parseInt($scope.minutes/60);
            $scope.minutes=($scope.minutes%60);

            if($scope.hours < 10 )
            {
                $scope.hours=("0"+$scope.hours);                
            }
            if($scope.minutes < 10 )
            {
                $scope.minutes=("0"+$scope.minutes);                
            }
            //final total duration of the week  
            $scope.durationTotal=($scope.hours+":"+$scope.minutes);
            //console.log("Date:"+date1);

            //Convert date into Prefixes
            var dayDate=new Date(date1);
            var convertedDate=dayDate.getDate();
            var convertedMonth=(dayDate.getMonth()+1);
            if(convertedDate < 10)
            {
                convertedDate=("0"+convertedDate);
            }
            if(convertedMonth < 10)
            {
                 convertedMonth=("0"+convertedMonth); 
            }
            var newWeekDayDate=convertedMonth+"/"+convertedDate+"/"+dayDate.getFullYear();

            //prepare final object and put into an array
            var object={};
            object.date=newWeekDayDate;
            object.duration=$scope.durationTotal;
            var d = new Date(date1); // today!

            //Create day's
            switch (d.getDay()) {
                case 0:
                    $scope.day = "Sunday";        
                    break;
                case 1:
                    $scope.day = "Monday";        
                    break;
                case 2:
                    $scope.day = "Tuesday";        
                    break;
                case 3:
                    $scope.day = "Wednesday";       
                    break;
                case 4:
                    $scope.day = "Thursday";       
                    break;
                case 5:
                    $scope.day = "Friday";        
                    break;
                case 6:
                    $scope.day = "Saturday";       
                    break;
            }

            object.day=$scope.day;
            //push Final Object into an array
            $scope.TimeSheetListArray.push(object);         
        }//end of main for


        $ionicLoading.hide();   
        }); 

    }//end of init()
    $scope.init();
});

/*==============================================================================================================================================================*/
/* TimesheetList Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Timesheet Record Controller Start */
/*==============================================================================================================================================================*/

app.controller('TimesheetRecordController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, AppFactory , $ionicPopup ,$filter) 
{
    $scope.weekDayDurationArray=[];

    $scope.showPage = false;

	$scope.page=AppFactory.getType();
	$scope.pageTitleName="TIMESHEET LIST";
    $scope.sortText="";

    // sidebar hiding here...
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* PAGE CATEGORY FOR --PAGE TITLE*/
    $scope.pageTitle = $stateParams.category;
    AppFactory.setCategory($stateParams.category);

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value,id)
    {
        //set the internal id here..
        AppFactory.setInternalId(id);
        $state.go('app.'+value);
    }

    /* LEFT BUTTONS START*/
    $scope.goBack = function() 
    {
        //console.log("pageTitle:"+AppFactory.getType());
        if(AppFactory.getType()=="Last5Timesheet")
        {
            $scope.gotoPage('Last5Timesheet');
        }
        else
        {
            $scope.gotoPage('TimesheetList');   
        }
        
    }/* LEFT BUTTONS ends*/

    /* RIGHT BUTTONS START*/
    $scope.goTo = function() 
    {
        $state.go('app.AddTimesheet');
    }


    /***********SORTING FUNCTION***********************/
    var orderBy = $filter('orderBy');
    $scope.order = function(predicate) {        
    $scope.weekDayDurationArray = orderBy($scope.weekDayDurationArray, predicate);
    };
 
    /*****************************************/

    
  /* RIGHT BUTTONS END*/ 
  $scope.showSortList = function() 
    {

        console.log("show function called()");

        // Show the action sheet
        $ionicActionSheet.show(
        {
            titleText: 'Sort By',
            buttons: [
                { text: 'By Duration' }          
            ],

            /* Cancel Button */
            cancelText: 'Cancel',
           
            cancel: function() 
            {
                console.log('CANCELLED');
            },

            /* Button Click Functionality */
            buttonClicked: function(index) 
            {
                if (index==0) 
                {
                    $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Duration?'
                    }).then(function(res) {
                        if(res) 
                        {
                            //$scope.orderString = 'hours';
                            $scope.order('hours');
                        } 
                        else 
                        {
                            console.log('You are not sure');
                        }
                    });
                };
                
            return true;
            }
        });
    };//end of showSortList()
    
     //get day and date when chilked from Timesheetlist 
    var item=AppFactory.getDayDate();
    $scope.todate=item.date;
    $scope.day=item.day;
    $scope.messageFlag="false";
    $scope.message="";
    $scope.init=function(){

        $scope.weekDayDurationArray=[];  
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
        //call web-service to get duration details hrs wise
        var parameters={
    					"id":"customsearch_aavz_35",
    					"customer_type":"timebill",
    					"noofrecords":"100",
    					"todate":$scope.todate
                    };
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=110&deploy=1",null,parameters,function(results) 
        { 
            var mainArray=results; 
            if(mainArray.length==0)
            {
                $scope.messageFlag="true";
                $scope.message="No record found.";
            }
            else
            {
                $scope.messageFlag="false";
            }
            for(item in mainArray)
            {
                var innerObject=mainArray[item];
                for(i in innerObject)
                {
                  var id=i;
                  var object={};
                  object.internalId=i;
                  object.date=innerObject[i].date;                 
                  object.hours=innerObject[i].hours;
                  object.key=innerObject[i].key;
                  console.log("Object:"+JSON.stringify(object));
                  $scope.weekDayDurationArray.push(object);                  
                }       
            }
            $scope.showPage = true;
            $ionicLoading.hide();
        });//end of sendHttpRequest
    }//end of init()
    $scope.init();

});

/*==============================================================================================================================================================*/
/* TimesheetRecord Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Last5TimesheetController Controller starts */
/*==============================================================================================================================================================*/

app.controller('Last5TimesheetController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory) 
{
    $scope.pageTitle="LAST 5 TIMESHEET";
	// hide side bar ...
    $rootScope.sideMenuEnabled = false;
    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value,item) 
    {    
        //set selected date and day to factory
        AppFactory.setDayDate(item);
        AppFactory.setType("Last5Timesheet");
        $state.go('app.'+value);
    }

    $scope.day="";
    var todate;
    var fromdate;
    $scope.weekStartDate="";

    $scope.weekStartDateArray=[];
    $scope.weekEndDateArray=[];
    $scope.dateDurationArray=[];
  
    //get today's date    
    var d = new Date(); // today!
    switch (d.getDay()) {
        case 0:
            $scope.day = "Sunday";
            d.setDate((d.getDate()-6));
            break;
        case 1:
            $scope.day = "Monday";        
            break;
        case 2:
            $scope.day = "Tuesday";
            d.setDate((d.getDate()-1));     
            break;
        case 3:
            $scope.day = "Wednesday";
            d.setDate((d.getDate()-2));     
            break;
        case 4:
            $scope.day = "Thursday";
            d.setDate((d.getDate()-3));     
            break;
        case 5:
            $scope.day = "Friday";
            d.setDate((d.getDate()-4));     
            break;
        case 6:
            $scope.day = "Saturday";
            d.setDate((d.getDate()-5));
            break;
    }

    //set Finally week start date
    $scope.weekStartDate=(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getFullYear();

    //store todate of weeks range
    todate=((d.getMonth()+1)+"/"+(d.getDate()+6)+"/"+d.getFullYear());
  
    //first week start date
    $scope.weekStartDateArray.push($scope.weekStartDate);
    //first week end date
    $scope.weekEndDateArray.push((d.getMonth()+1)+"/"+(d.getDate()+6)+"/"+d.getFullYear());

    //Code for form week
    for(var i=0;i<23;i++)
    {
        var d1 = new Date($scope.weekStartDate); // today!
        var x = (i+1)*7; // go back 5 days!
        d1.setDate(d1.getDate() - x);
        var startDate=(d1.getMonth()+1)+"/"+d1.getDate()+"/"+d1.getFullYear();
        $scope.weekStartDateArray.push(startDate);

        //code for weekEndDate array
        var d2=new Date(d1);  
        var x =6; // go back 5 days!
        d2.setDate(d2.getDate() + x);
        var endDate=(d2.getMonth()+1)+"/"+d2.getDate()+"/"+d2.getFullYear();
        $scope.weekEndDateArray.push(endDate);

    }

    //store fromdate of weeks range
    var myDate=$scope.weekStartDateArray[23];
    var newdate=new Date(myDate);
    fromdate=((newdate.getMonth()+1)+"/"+(newdate.getDate()-1)+"/"+newdate.getFullYear());
    
	//call web-service to get last 5 timesheet 
	$scope.last5TimesheetList=[];
	$scope.day="";

    $scope.init=function()
    {
        $scope.last5TimesheetList=[];
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
    	 var parameters={
    					"id":"customsearch_aavz_35",
    					"customer_type":"timebill",
    					"fromdate":fromdate,
    					"todate":todate, 
    					"lastfivetimesheet":"1"
    					};  


        //Request for get data
        AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=110&deploy=1",parameters,null,function(results) 
    	{ //get result from json

            var mainArray=results;
            for(item in mainArray)
            {
                var innerObject=mainArray[item];
                for(i in innerObject)    
                {
                    var object=innerObject[i];

                    var dt=new Date(object.date);
                    var date=dt.getDate();
                    var month=(dt.getMonth()+1);
                    if(date < 10)
                    {
                        date=("0"+date);
                    }
                    if(month < 10)
                    {
                         month=("0"+month); 
                    }
                    object.date=(month+"/"+date+"/"+dt.getFullYear());

                    var d = new Date(object.date);     
                    switch (d.getDay()) {
                        case 0:
                            $scope.day = "Sunday";        
                            break;
                        case 1:
                            $scope.day = "Monday";        
                            break;
                        case 2:
                            $scope.day = "Tuesday";        
                            break;
                        case 3:
                            $scope.day = "Wednesday";       
                            break;
                        case 4:
                            $scope.day = "Thursday";       
                            break;
                        case 5:
                            $scope.day = "Friday";        
                            break;
                        case 6:
                            $scope.day = "Saturday";       
                            break;
                    }

                    object.day=$scope.day;
                    if(object!="Developer 01")
                    {
                    $scope.last5TimesheetList.push(object);
                    }
                }
            }
        $ionicLoading.hide();
        });//end of sendHttpRequest()
    }//end of init()
    $scope.init();

    $scope.goBack = function()
    {
        $state.go('app.homepage');
    }
});

/*==============================================================================================================================================================*/
/* Last5TimesheetController Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* TimesheetDetails Controller Start */
/*==============================================================================================================================================================*/

app.controller('TimesheetDetailsController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, AppFactory) 
{   
    //array used to store setion array list
    $scope.headerArray=[];
    $scope.primaryInfoArray=[];
   // page name given here..
    $scope.pageName="TIMESHEET DETAILS";
    $scope.sortText="";

    $scope.showPage = false;

    /* SIDE MENU SECTION -- HIDE OR UNHIDE*/
    console.log("customer controller appearing");
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* PAGE CATEGORY FOR --PAGE TITLE*/
    $scope.pageTitle = $stateParams.category;
    AppFactory.setCategory($stateParams.category);

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value)
    {
         $state.go('app.'+value);
    }

    /* LEFT BUTTONS START*/
    $scope.goBack = function() 
    {
        $scope.gotoPage('TimesheetRecord');
    }/* LEFT BUTTONS ends*/

    /* RIGHT BUTTONS START*/
    $scope.goToEdit = function() 
    {
        $state.go('app.EditTimesheet');
    }

     /* RIGHT BUTTONS END*/ 

	//fetch id from factory
    var internalId=AppFactory.getInternalId(); 

    $scope.init=function(){
        $scope.showPage = false;
        $scope.headerArray=[];
        $scope.primaryInfoArray=[];        
        $scope.primaryInfoData=[];
        $scope.headerData=[];

        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        }); 
        var parameters={
    					"id":"customsearch_aavz_34",
    					"groupname":"Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Header,Header,Primary Information",
    					"internalid":internalId,
    					"display":"4,7,3,8,5,2,1,6",
    					"fieldname":"memo,department,hours,item,isbillable,date,employee,customer",
    					"customer_type":"timebill"
    					};

        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=99&deploy=1",null,parameters,function(results)
    	{ 
            var mainArray=results;
            for(item in mainArray)
            {
                var mainObj = mainArray[item];
                for(innerObj in mainObj)
                {
                    var id=innerObj; //hold key 241
                    var keyObj=mainObj[innerObj]; //hold entire object 
                    for(key in keyObj)
                    {
                       var singleObject=keyObj[key];
                       // section wise distribution
                        var sectionHeader=singleObject.section_header;

                        if(sectionHeader=="Header")
                        {   
                            $scope.headerArray.push(keyObj[key]); //hold array of objects having section header is Header
                        }
                
                        if(sectionHeader=="Primary Information")
                        {
                            $scope.primaryInfoArray.push(keyObj[key]); //hold array of objects having section header is Header
                        }  
                    }
            
                }//end of 2nd for   
          
            }//end of 1 st for
      
            $scope.headerData=[];
            
            //Code for display dynamic column values of labels    
            for(i=0;i<$scope.headerArray.length;i++)
            {
                var singleObj=$scope.headerArray[i];
                for(key in singleObj)
                {
                    if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                    {
                        //console.log(key);
                        var object={};
                        object.label=$scope.headerArray[i].label;
                        object.value=singleObj[key];
                        object.index=$scope.headerArray[i].value;
                        $scope.headerData.push(object);
                    }
                }
            }
     
            $scope.primaryInfoData=[];

            for(i=0;i<$scope.primaryInfoArray.length;i++)
            {
                var singleObj=$scope.primaryInfoArray[i];
                for(key in singleObj)
                {
                    if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                    {
                        //console.log(key);
                        var object={};
                        object.label=$scope.primaryInfoArray[i].label;
                        object.value=singleObj[key];
                        object.index=$scope.primaryInfoArray[i].value;
                        $scope.primaryInfoData.push(object);
                    }
                }
            }
      
            //SET data into factory used in edit timesheet Controller 
            var myObject={};
            myObject.header=$scope.headerData;
            myObject.primary=$scope.primaryInfoData;
            AppFactory.setTimesheetData(myObject);
            $scope.showPage = true;
            $ionicLoading.hide();
        });//end of sendHttpRequest
    }//end of init()
    $scope.init();
});

/*==============================================================================================================================================================*/
/* TimesheetDetails Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* AddTimesheetController Controller Start */
/*==============================================================================================================================================================*/

app.controller('AddTimesheetController', function($scope, $rootScope, $http, $ionicModal, $stateParams, $ionicLoading, $ionicActionSheet, $state, AppFactory,$ionicPopup) 
{
	$scope.timesheet={};

	var emp=AppFactory.getEmployeeIdValue();
	$scope.timesheet.employeevalue=emp.employeevalue;
	$scope.timesheet.employee=emp.employee;

	var date=AppFactory.getDayDate();
	$scope.timesheet.date=date.date;

	/* Design Section START */
	$scope.pageHeadTitle="ADD TIMESHEET";

    $scope.sortText="";

    /* SIDE MENU SECTION -- HIDE OR UNHIDE*/
    console.log("customer controller appearing");
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* PAGE CATEGORY FOR --PAGE TITLE*/
    $scope.pageTitle = $stateParams.category;
    AppFactory.setCategory($stateParams.category);

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value)
    {
         $state.go('app.'+value);
    }

    /* LEFT BUTTONS START*/ 
     $scope.showConfirm = function() 
	 {
			$ionicPopup.confirm({
			title: 'Alert',
			content: 'Any unsaved data will be lost. Do you want to continue?'
			}).then(function(res) {
				if(res) 
				{
					 $state.go('app.TimesheetRecord');
				} 
				else 
				{
				 console.log('You are not sure');
				}
			});
     };

    $scope.goTo = function() 
      {
        $scope.showConfirm();      
      }
     /* LEFT BUTTONS END*/

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    
     /* RIGHT BUTTONS START*/
    
    $scope.saveData = function() 
      {
       
        if($scope.timesheet.hours == undefined || $scope.timesheet.hours == "")
        {
             $scope.showAlert("Please fill Duration(Hrs) field.");
             return;
        }
        if($scope.timesheet.itemvalue == undefined || $scope.timesheet.itemvalue == "")
        {
             $scope.showAlert("Please fill Item field.");
             return;
        }

        //Validations for hrs and time validation2
        var hrs=$scope.timesheet.hours;
        console.log("Hrs:"+hrs);

        var pattern = /^([0]?[0-9]|[1][0-2])(:[0-5][0-9])?$/;
        
        if(hrs.match(pattern))  
        {  
            console.log("Duration is Valid");
        }  
        else  
        {  
            $scope.showAlert("Please enter valid Duration(Hrs)");
            return;
        }  


      
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=99&deploy=1",$scope.timesheet,null,function(results) 
              {   
                    //console.log("Response:"+JSON.stringify(results));
                    if(results =="\"Timesheet record saved successfully.\"")
                    {   
                        $ionicLoading.hide(); 
                        //window.plugins.toast.show("Record saved successfully.","short","center");
                        $state.go('app.TimesheetRecord');  
                    }
                    else
                    {   
                        $ionicLoading.hide(); 
                        $scope.showAlert(results);
                        return;
                    }                 
               
              });//End of sentHttpRequest  
        
        
      }
     /* RIGHT BUTTONS END*/

    

	/*Modal for Department START*/

	//Load the Department modal from the given template URL
	  $ionicModal.fromTemplateUrl('Department.html', function(Department) {
		$scope.Department = Department;
	  }, {
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope,
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'
	  });
	  
	$scope.DepartmentList=[];
	$scope.openDepartment = function() 
	{    
		// to show the model window where list of expenseCategory are there.
		 $scope.Department.show();  
		 // loading indicator starts here...
		$ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Department List..",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
		$scope.DepartmentList=[];
		var parameters={
					  "field_name":"department",
					  "field_type":"select",
					  "field_subtext":"",
					  "customer_type":"timebill",
					  "field_dependent":"",
					  "field_dependentvalue":""
					};         
		   
		//Request for get data
		AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
		{ 
			var mainArray=results;
			for(obj in mainArray)
			{
			  var objectArray=mainArray[obj];          
			  var object={};
			  object.index=objectArray[0];
			  object.value=objectArray[1];
			  $scope.DepartmentList.push(object);          
			}
			// to hide the loading indicator 
			$ionicLoading.hide(); 
		}); //end of sendHttpRequest
	
	};
	  $scope.closeDepartment = function() 
	  {
		$scope.Department.hide();
	  };

	  //Be sure to cleanup the modal
	  $scope.$on('$destroy', function() 
	  {
		$scope.Department.remove();
	  });

	 $scope.setDepartment=function(dept)
	  {
		$scope.timesheet.departmentvalue=dept.value; 
		$scope.timesheet.department=dept.index;    
		$scope.Department.hide();
	  }

	/*Modal for Department END*/

	/*Modal for CUSTOMERjOB START*/

	//Load the Department modal from the given template URL
	  $ionicModal.fromTemplateUrl('CustomerJob.html', function(CustomerJob) {
		$scope.CustomerJob = CustomerJob;
	  }, {
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope,
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'
	  });
	 
	 $scope.CustomerJobList=[];
	  $scope.openCustomerJob = function() 
	  {
		// to show the model window where list of customer job are there.
		 $scope.CustomerJob.show();  
	 // loading indicator starts here...
       $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Customer...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
		$scope.CustomerJobList=[];
		 var parameters={
					  "field_name":"customer",
					  "field_type":"select",
					  "field_subtext":"",
					  "customer_type":"timebill",
					  "field_dependent":"",
					  "field_dependentvalue":""
				  };         
			   
			//Request for get data
			AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) 
			{ 
				var mainArray=results;
				for(obj in mainArray)
				{
				  var objectArray=mainArray[obj];          
				 //console.log(JSON.stringify(objectArray));
				  var object={};
				  object.index=objectArray[0];
				  object.value=objectArray[1];
				  $scope.CustomerJobList.push(object);          
				}
				//to stop the loading indicator
				$ionicLoading.hide();
		  }); //end of sendHttpRequest
			
	  };
	  $scope.closeCustomerJob = function() 
	  {
		$scope.CustomerJob.hide();
	  };

	  //Be sure to cleanup the modal
	  $scope.$on('$destroy', function()
	  {
		$scope.CustomerJob.remove();
	  });

	 $scope.setCustomerJob=function(cust)
	  {
		$scope.timesheet.customervalue=cust.value; 
		$scope.timesheet.customer=cust.index;    
		$scope.CustomerJob.hide();
	  }

	/*Modal for CustomerJob END*/


	/*Modal for Item START*/

	//Load the Item modal from the given template URL
	  $ionicModal.fromTemplateUrl('Item.html', function(Item) {
		$scope.Item = Item;
	  }, {
		
		scope: $scope,
		
		animation: 'slide-in-up'
	  });
	  
	  $scope.ItemList=[];
	  $scope.openItem = function() 
	  {
		// to show the model window where list of itemlist are there.
		 $scope.Item.show();  
		// it begins loading indicator 
	    $scope.loadingIndicator = $ionicLoading.show({
				content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Item list...",
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 10
			});
		  $scope.ItemList=[];
		  var parameters={
						  "field_name":"item",
						  "field_type":"select",
						  "field_subtext":"",
						  "customer_type":"timebill",
						  "field_dependent":"",
						  "field_dependentvalue":""
						};         
		   
		//Request for get data
		AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) 
		{ 
			var mainArray=results;
			for(obj in mainArray)
			{
			  var objectArray=mainArray[obj];          
			  var object={};
			  object.index=objectArray[0];
			  object.value=objectArray[1];
			  $scope.ItemList.push(object);          
			}        
			  //to stop the loading indicator
				$ionicLoading.hide();
		}); //end of sendHttpRequest
         
	};

	  $scope.closeItem = function() 
	  {
		$scope.Item.hide();
	  };

	  $scope.$on('$destroy', function() 
	  {
		$scope.Item.remove();
	  });

	 $scope.setItem=function(item)
	  {
		$scope.timesheet.itemvalue=item.value; 
		$scope.timesheet.item=item.index;    
		$scope.Item.hide();
	  }

	/*Modal for Item END*/

	$scope.value2 ="false";
    $scope.timesheet.isbillable="F";

    $scope.checkState=function()
    {

      
      if($scope.value2=="false")
      {
        $scope.value2 = "true";
      }
      else
      {
        $scope.timesheet.isbillable="F";
        $scope.value2 = "false";

      }

      if($scope.value2 == "true")
      {
        console.log("Status : "+$scope.value2);
        $scope.timesheet.isbillable="T";
      }
      else
      {

        console.log("Status : "+$scope.value2);
        $scope.timesheet.isbillable="F";

      }

      
    }//end of checkState()

});

/*==============================================================================================================================================================*/
/* AddTimesheetController Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* EditTimesheetController Controller Start */
/*==============================================================================================================================================================*/

app.controller('EditTimesheetController', function($scope, $rootScope, $http, $ionicModal, $stateParams, $ionicLoading, $ionicActionSheet, $state, AppFactory,$ionicPopup) 
{
	$scope.timesheet={};
    $scope.sortText="";

    /* SIDE MENU SECTION -- HIDE OR UNHIDE*/
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    /* PAGE CATEGORY FOR --PAGE TITLE*/
    $scope.pageTitle = "EDIT TIMESHEET";//$stateParams.category;
    AppFactory.setCategory($stateParams.category);

    /* CHANGE PAGE FROM MENU-- GOTO*/
    $scope.gotoPage = function(value)
    {
         $state.go('app.'+value);
    }

    /* ACTION BAR BUTTONS START*/
	$scope.showConfirm = function() {

                    $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Any unsaved data will be lost. Do you want to continue?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $state.go('app.TimesheetDetails');
                        } 
                        else 
                        {
                         console.log('You are not sure');
                        }
                    });
                };

    /* LEFT BUTTONS START*/ 
    $scope.goTo = function() 
      {
      
        $scope.showConfirm();      
      }
        /* LEFT BUTTONS END*/

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    /* RIGHT BUTTONS START*/ 
   
    $scope.saveData = function() 
      { 
       
        if($scope.timesheet.hours == undefined || $scope.timesheet.hours == "")
        {
             $scope.showAlert("Please fill Duration(Hrs) field.");
             return;
        }
        if($scope.timesheet.itemvalue == undefined || $scope.timesheet.itemvalue == "")
        {
             $scope.showAlert("Please fill Item field.");
             return;
        }

        //Validations for hrs and time valid
        var hrs=$scope.timesheet.hours;
        console.log("Hrs:"+hrs);

        var pattern = /^([0]?[0-9]|[1][0-2])(:[0-5][0-9])?$/;
        
        if(hrs.match(pattern))  
        {  
            console.log("Duration is Valid");
        }  
        else  
        {  
            $scope.showAlert("Please enter valid Duration(Hrs)");
            return;
        }  


        //Call the web-service to updates the timesheet
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=99&deploy=1",$scope.timesheet,null,function(results) 
            {  
                //console.log("Response:"+JSON.stringify(results));
                if(results =="\"Timesheet record edited successfully.\"")
                {
                    $ionicLoading.hide(); 
                    //window.plugins.toast.show("Record edited successfully.","short","center"); 
                    //To navigate the OPPORTUNITY list page  
                    $state.go('app.TimesheetDetails');
                }
                else
                {
                    $ionicLoading.hide(); 
                    $scope.showAlert(results);
                    return;
                }

            });
       }
    /* RIGHT BUTTONS END*/


	/*Modal for Department START*/

	//Load the Department modal from the given template URL
	  $ionicModal.fromTemplateUrl('Department.html', function(Department) {
		$scope.Department = Department;
	  }, {
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope,
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'
	  });
   
	$scope.DepartmentList=[];
	$scope.openDepartment = function() 
	{    
			// to show the model window where list of expenseCategory are there.
		 $scope.Department.show();  
		 // loading indicator starts here...
       $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Department List..",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
		$scope.DepartmentList=[];
		var parameters={
          "field_name":"department",
          "field_type":"select",
          "field_subtext":"",
          "customer_type":"timebill",
          "field_dependent":"",
          "field_dependentvalue":""
              };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
		{ 
			var mainArray=results;
			for(obj in mainArray)
			{
			  var objectArray=mainArray[obj];          
			  var object={};
			  object.index=objectArray[0];
			  object.value=objectArray[1];
			  $scope.DepartmentList.push(object);          
			}
			 //to stop the loading indicator
			$ionicLoading.hide();
		}); //end of sendHttpRequest
		   
    };
   
	  $scope.closeDepartment = function()
	  {
		$scope.Department.hide();
	  };

	  //Be sure to cleanup the modal
	  $scope.$on('$destroy', function() 
	  {
		$scope.Department.remove();
	  });

		$scope.setDepartment=function(dept)
		{
			$scope.timesheet.departmentvalue=dept.value; 
			$scope.timesheet.department=dept.index;    
			$scope.Department.hide();
		}

	/*Modal for Department END*/


	/*Modal for CUSTOMERjOB START*/

	//Load the Department modal from the given template URL
	  $ionicModal.fromTemplateUrl('CustomerJob.html', function(CustomerJob) {
		$scope.CustomerJob = CustomerJob;
	  }, {
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope,
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'
	  });
	  
	 $scope.CustomerJobList=[];
	  $scope.openCustomerJob = function() 
	  {
			// to show the model window where list of customer job are there.
			 $scope.CustomerJob.show();  
			// loading indicator starts here...
		   $ionicLoading.show({
				content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Customer Job list...",
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showdelay: 300
			});
			$scope.CustomerJobList=[];
			 var parameters={
			  "field_name":"customer",
			  "field_type":"select",
			  "field_subtext":"",
			  "customer_type":"timebill",
			  "field_dependent":"",
			  "field_dependentvalue":""
				  };         
			   
			//Request for get data
			AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
			{ 
				var mainArray=results;
				for(obj in mainArray)
				{
				  var objectArray=mainArray[obj];          
				 //console.log(JSON.stringify(objectArray));
				  var object={};
				  object.index=objectArray[0];
				  object.value=objectArray[1];
				  $scope.CustomerJobList.push(object);          
				}
				 //to stop the loading indicator
				$ionicLoading.hide();
			  }); //end of sendHttpRequest
		
	   };
	  
		$scope.closeCustomerJob = function()
		{
			$scope.CustomerJob.hide();
		};

		//Be sure to cleanup the modal
		$scope.$on('$destroy', function()
		{
			$scope.CustomerJob.remove();
		});

		$scope.setCustomerJob=function(cust)
		{
			$scope.timesheet.customervalue=cust.value; 
			$scope.timesheet.customer=cust.index;    
			$scope.CustomerJob.hide();
		}

	/*Modal for CustomerJob END*/


	/*Modal for Item START*/

	//Load the Item modal from the given template URL
	  $ionicModal.fromTemplateUrl('Item.html', function(Item) {
		$scope.Item = Item;
	  }, {
		
		scope: $scope,
		 animation: 'slide-in-up'
	  });
	  
			$scope.ItemList=[];
			$scope.openItem = function() 
			{
				// to show the model window where list of itemlist are there.
				 $scope.Item.show();  
				// it begins loading indicator 
			    $scope.loadingIndicator = $ionicLoading.show({
					content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Item list...",
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 10
				});
				 $scope.ItemList=[];
				 var parameters={
				  "field_name":"item",
				  "field_type":"select",
				  "field_subtext":"",
				  "customer_type":"timebill",
				  "field_dependent":"",
				  "field_dependentvalue":""
					  };         
				   
				//Request for get data
				AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) 
				{ 
					var mainArray=results;
					for(obj in mainArray)
					{
					  var objectArray=mainArray[obj];          
					  var object={};
					  object.index=objectArray[0];
					  object.value=objectArray[1];
					  $scope.ItemList.push(object);          
					}        
					  //to stop the loading indicator
					$ionicLoading.hide();
				}); //end of sendHttpRequest
			};
	   
			$scope.closeItem = function() 
			{
				$scope.Item.hide();
			};

			$scope.$on('$destroy', function() 
			{
				$scope.Item.remove();
			});

			$scope.setItem=function(item)
			{
				$scope.timesheet.itemvalue=item.value; 
				$scope.timesheet.item=item.index;    
				$scope.Item.hide();
			}

	/*Modal for Item END*/
		
	$scope.value2 ="";
    $scope.flag="false";
    
    $scope.checkState=function()
    {
        console.log("value2 : "+$scope.value2);
      
      if($scope.value2=="false")
      {
        $scope.value2 = "true";
      }
      else
      {
        $scope.timesheet.isbillable="F";
        $scope.value2 = "false";

      }

      if($scope.value2 == "true")
      {
        console.log("Status true : "+$scope.value2);
        $scope.timesheet.isbillable="T";
        $scope.flag="true";
      }
      else
      {

        console.log("Status false : "+$scope.value2);
        $scope.timesheet.isbillable="F";
        $scope.flag="false";

      }
    console.log("Inside checkState:"+$scope.timesheet.isbillable);
      
    }//end of checkState()
        

		//get Existing data
		$scope.timesheet.internalid=AppFactory.getInternalId();
		$scope.timesheetObj=AppFactory.getTimesheetData();

		$scope.headerArray=$scope.timesheetObj.header;
		$scope.primaryInfoArray=$scope.timesheetObj.primary;

		  for(obj in $scope.headerArray)
		  {
			  var data=$scope.headerArray[obj];
			  for(key in data)
			  {
				if(data[key]=="Employee")
				{
				  $scope.timesheet.employeevalue=data.value;
				  $scope.timesheet.employee=data.index;
				}
				else if(data[key]=="Date")
				{
				  $scope.timesheet.date=data.value;
				}
			  }
		  }

          
			for(obj in $scope.primaryInfoArray)
			{
				var data=$scope.primaryInfoArray[obj];
				for(key in data)
				{
					if(data[key]=="Billable")
					{
						if(data.index=="T")
						{
							$scope.value2 ="true";
							$scope.timesheet.isbillable=data.index;
                            $scope.flag="true";
						}
						else
						{
							$scope.value2 ="false";
							$scope.timesheet.isbillable=data.index;
                            $scope.flag="false";
						}
					console.log("Existing:"+$scope.timesheet.isbillable);   
                    console.log("value2:"+$scope.value2);  
	                }

					else if(data[key]=="Duration(Hrs)")
					{         
					 $scope.timesheet.hours=data.value;
					}

					else if(data[key]=="Description")
					{         
					  $scope.timesheet.memo=data.value;        
					}

					else if(data[key]=="Customer")
					{
					  $scope.timesheet.customer=data.index;
					  $scope.timesheet.customervalue=data.value;         
					}

					else if(data[key]=="Department")
					{
					  $scope.timesheet.department=data.index;
					  $scope.timesheet.departmentvalue=data.value;         
					}

					else if(data[key]=="Item")
					{
					  $scope.timesheet.item=data.index;
					  $scope.timesheet.itemvalue=data.value;         
					}
				
				}
			}

});

/*==============================================================================================================================================================*/
/* EditTimesheetController Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Expense Controller Start */
/*==============================================================================================================================================================*/

app.controller('ExpenseController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $ionicPopup, $state,AppFactory, $location,$anchorScroll,$ionicScrollDelegate) 
{
	  // to store the objects where expense data is present
    $scope.valueArray = [];
	// to set the title to the Expense 
    $scope.pageTitle = "EXPENSE LIST";
  
	  // for the side bar hidding...
	  $rootScope.sideMenuEnabled = true;

    /* function will be called when any of item will be selected*/
    $scope.gotoPage = function(value)
    {
        AppFactory.setLastObj($scope.lastObj);
		// it sets the expense id 
          AppFactory.setExpenseId(value);
		// it sets that we are coming from the edit page..
		  AppFactory.setType("EDIT EXPENSE");
        // it redirects to the expense details page
          $state.go('app.ExpenseDetails');
    }

    $scope.goTo = function()
	{
         AppFactory.setLastObj($scope.lastObj);
         $state.go('app.ExpenseAdd');
    }

    $scope.allData = [];
    $scope.showMessage = false;
    // function will call on clicking refresh button..
    $scope.init = function()
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        // to empty the array on refresh button....
        $scope.valueArray = [];
        // loading indicator starts here...
        $scope.orderString = "";
       $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

    
     // parameters to send with the requrest of getting data of expense list
            var parameters =
            { 
                "id":"customsearch_aavz_38",
                "customer_type":"expensereport",
                "noofrecords":"100" , 
                "recordcount":"0"  
            };
            // to call the webservice..
            AppFactory.getExpenseList("0",$scope.orderString,function(results)
            {   
                $scope.lastObj = results[results.length-1];    
                //console.log(" result is "+ JSON.stringify($scope.lastObj));
                $scope.allData=results;
                for(mainArray in  results)
                {
                    obj =  results[mainArray];
                    for(objKey in obj)
                    {
                       var objValue = obj[objKey];
                       objValue.id=objKey;
                        // pushing the expense data into the valuearray
                       $scope.valueArray.push(objValue);
                        
                    }
                }
				// to remove the last line of the value array ..
                $scope.valueArray.splice($scope.valueArray.length-2,2);
				// to hide the loading indicator 
                $ionicLoading.hide();
            });
                 
    }
    // to get the data first time...
    $scope.init();

    // function will be useful to dynamically increase the list.. 
    $scope.loadSortedData = function()
        {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        // to empty the array on refresh button....
        $scope.valueArray = [];
        // loading indicator starts here...
       $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

            // to call the webservice..
            AppFactory.getExpenseList("0",$scope.orderString,function(results)
            {   
                $scope.lastObj = results[results.length-1];    
                //console.log(" result is "+ JSON.stringify($scope.lastObj));
                $scope.allData=results;
                for(mainArray in  results)
                {
                    obj =  results[mainArray];
                    for(objKey in obj)
                    {
                       var objValue = obj[objKey];
                       objValue.id=objKey;
                        // pushing the expense data into the valuearray
                       $scope.valueArray.push(objValue);
                        
                    }
                }
                // to remove the last line of the value array ..
                $scope.valueArray.splice($scope.valueArray.length-2,2);
                // to hide the loading indicator 
                $ionicLoading.hide();
            });
        }
    
        $scope.loadNextData1 = function()
        {
                //checking whether index is between last 5
            /*if(index > ($scope.valueArray.length-5) && index <=($scope.valueArray.length-1))
            {
                //if restlet is not called for the range
                if($scope.flag==1)
                {*/
                    $ionicLoading.show({
                        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 300
                    });
                    // count the length of objects in the list..
                    var temp= $scope.valueArray.length;
                    AppFactory.getExpenseList(temp,$scope.orderString,function(results)
                    {
                        $scope.allData = results;
                        console.log("data:"+JSON.stringify($scope.allData))
                        if($scope.allData.length == 0 || $scope.allData.length == 1)
                        {
                            $scope.showMessage = true;
                        }
                        console.log("length:"+$scope.allData.length+" showMessage:"+$scope.showMessage);
                        for(mainArray in  results)
                        {
                            obj =  results[mainArray];

                            for(objKey in obj)
                            {
                               var objValue = obj[objKey];
                               objValue.id=objKey;
                                // pushing the case data into the valuearray
                               $scope.valueArray.push(objValue);
                                
                            }
                        }
                        // to remove the last line of the value array ..
                        $scope.valueArray.splice($scope.valueArray.length-2,2);
                         // to hide the loading indicator 
                        $ionicLoading.hide();
                    });
                    /*$scope.flag=0;
                }
            }
            else
            {
                $scope.flag=1;
            }*/
        }
    // function to sort the list by various ways..
		$scope.showSortList = function() 
		{
			// Show the action sheet
			$ionicActionSheet.show(
			{
				titleText: 'Sort By',
				buttons: [
					{ text: 'Status', },
                    { text: 'Date', },
					{ text: 'Number' }
				],

				/* Cancel Button */
				cancelText: 'Cancel',
				cancel: function() 
				{
					console.log('CANCELLED');
				},

				/* Button Click Functionality */
				buttonClicked: function(index) 
				{

					if (index==0) 
					{
					       $ionicPopup.confirm({
                          title: 'Alert',
                          content: 'Do you want to sort by status?'
                          }).then(function(res) {
                           if(res) 
                           {
                            $scope.orderString = 'Status';
                            $scope.loadSortedData();
                           } 
                           else 
                           {
                            console.log('You are not sure');
                           }
                          });
					};

					if (index==1) 
					{
						  $ionicPopup.confirm({
                          title: 'Alert',
                          content: 'Do you want to sort by Date?'
                          }).then(function(res) {
                           if(res) 
                           {
                            $scope.orderString = 'Date';
                            $scope.loadSortedData();
                           } 
                           else 
                           {
                            console.log('You are not sure');
                           }
                          });
					};
					if (index==2) 
					{
						  $ionicPopup.confirm({
                          title: 'Alert',
                          content: 'Do you want to sort by Number?'
                          }).then(function(res) {
                           if(res) 
                           {
                            $scope.orderString = 'Number';
                            $scope.loadSortedData();
                           } 
                           else 
                           {
                            console.log('You are not sure');
                           }
                          });
					};
				return true;
				}
			});
		};

});

/*==============================================================================================================================================================*/
/* Expense Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* TopExpenseController starts  */
/*==============================================================================================================================================================*/

app.controller('TopExpenseController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory, $location,$anchorScroll,$ionicPopup) 
{
	// to set from which page we are coming..
	AppFactory.setType("TOP EXPENSE");
	
	 // to set the title to the Expense 
	$scope.pageTitle = "LAST 5 EXPENSES";
	  
	// for the side bar hidding...
	$rootScope.sideMenuEnabled = false;

		/* function will be called when any of item will be selected*/
		$scope.gotoPage = function(value)
		{
		 // it sets the expense id 
			  AppFactory.setExpenseId(value);
			// it redirects to the expense details page
			  $state.go('app.ExpenseDetails');
		}

		$scope.goBack = function()
		{
			  $state.go('app.homepage');
		}

	   // to store the objects where expense data is present
		$scope.valueArray = [];

        $scope.init = function()
        {

            // to store the objects where expense data is present
            $scope.valueArray = [];
       
            // loading indicator starts here...
           $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        
         // parameters to send with the requrest of getting data of expense list
                var parameters =
                { 
                    "customer_type":"expensereport",
    				"fromdate":"",
    				"todate":"",
    				"customerid":"",
    				"id":"customsearch_aavz_38",
    				"recordcount":"0",
    				"lastfiveexpense":"1" 
                };
                // to call the webservice..
                AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=104&deploy=1",null,parameters,function(results)  
    			{
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
                            // pushing the expense data into the valuearray
                           $scope.valueArray.push(objValue);
                            
                        }
                    }
    				 // to remove the last line of the value array ..
    				$scope.valueArray.splice($scope.valueArray.length-2,2);
    				// to hide the loading indicator 
                    $ionicLoading.hide();
                });
            }
            $scope.init();    
       

    // function to sort the list by various ways..
        $scope.showSortList = function() 
        {
            // Show the action sheet
            $ionicActionSheet.show(
            {
                titleText: 'Sort By',
                buttons: [
                    { text: 'Status', },
            { text: 'Date', },
                    { text: 'Number' }
                ],

                /* Cancel Button */
                cancelText: 'Cancel',
                cancel: function() 
                {
                    console.log('CANCELLED');
                },

                /* Button Click Functionality */
                buttonClicked: function(index) 
                {

                    if (index==0) 
                    {
                           $ionicPopup.confirm({
                          title: 'Alert',
                          content: 'Do you want to sort by Status?'
                          }).then(function(res) {
                           if(res) 
                           {
                            $scope.orderString = 'Status';
                           } 
                           else 
                           {
                            console.log('You are not sure');
                           }
                          });
                    };

                    if (index==1) 
                    {
                          $ionicPopup.confirm({
                          title: 'Alert',
                          content: 'Do you want to sort by Date?'
                          }).then(function(res) {
                           if(res) 
                           {
                            $scope.orderString = 'Date';
                           } 
                           else 
                           {
                            console.log('You are not sure');
                           }
                          });
                    };
                    if (index==2) 
                    {
                          $ionicPopup.confirm({
                          title: 'Alert',
                          content: 'Do you want to sort by Number?'
                          }).then(function(res) {
                           if(res) 
                           {
                            $scope.orderString = 'Number';
                           } 
                           else 
                           {
                            console.log('You are not sure');
                           }
                          });
                    };
                return true;
                }
            });
        };

});

/*==============================================================================================================================================================*/
/* TopExpense Controller ends */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* ExpenseDetails Controller Start */
/*==============================================================================================================================================================*/

app.controller('ExpenseDetailsController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, $location, $anchorScroll, AppFactory) 
{
		// to set the title... 
		$scope.pageTitle = "EXPENSE DETAIL";
		// to know from which page we have entered here..
	   var Previouspage =AppFactory.getType();
	   
		// to get the id of the expense which was clicked..
		var expenseId = AppFactory.getExpenseId();
		

	   // to hide the sidebar..
	   $rootScope.enableSideBar = true;
	   if ($rootScope.hideSideBar == false) 
	   {
		  $scope.sideMenuController.toggleLeft();
		  $rootScope.hideSideBar = true;
	   }

     /* function will be called when clicked on the expenses..*/
    $scope.gotoPage = function()
    {
      // it redirects to the expense details page
         $state.go('app.ExpenseList');
    }

    // LEFT BUTTON
    $scope.goBack = function() 
      {
		if(Previouspage=="TOP EXPENSE")
		{
			$state.go('app.TopExpense');
		}
		else
		{
        $state.go('app.Expense');
		}
      }

    $scope.goToEdit = function()
    {
      // to set the expense details to get on the next page..
        AppFactory.setExpenseDetails($scope.value);
    // it redirects on the expense edit page..
        $state.go('app.EditExpense');
    }

    $scope.init = function()
    {
        // to store the objects 
        $scope.value = [];
         // to begin the loading indicator ..
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 10
            });
       
            // parameters to set along with the request..
            var parameters=
            { 
                "id":"customsearch_aavz_36",
				"noofrecords":"100",
				"internalid":expenseId,
				"display":"2,5,3,4,1",
				"groupname":"Header,Primary Information,Header,Primary Information,Header",
				"fieldname":"trandate,internalid,memo,duedate,entity",
				"customer_type":"expensereport"
            };
          
		//$scope.expense={};
            //Request for get data of the expense details
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=107&deploy=1",null,parameters,function(results)
            { 
        for(singleObj in  results)
                {
                    obj =  results[singleObj];
            
                    for(innerobj in obj)
                    {
                        obj1 = obj[innerobj];
          
                        for(mainobj in obj1)
                        { 
                            var objValue = obj1[mainobj];
                                                                 
                            for(objkey in objValue)
                            {
								if((objkey !="value")&&(objkey!="priority")&&(objkey!="label")&&(objkey!="section_header"))
                                {
                                    obj1[mainobj].gotValue = objValue[objkey];
                                }
                            } 
                            // to store the data of expense details
                            $scope.value.push(objValue);
                         }
						// to sort the expense details data on the basis of priority .. 
                        $scope.value.sort(function(a, b)
                        {
                            return a.priority - b.priority;
                        });
            
                    }
                } 
                // to remove the last line of the value array ..
                $scope.value.splice($scope.value.length-1,1);
                // to stop the loading indicator ..
                $ionicLoading.hide();
            });
        }
        $scope.init();

});

/*==============================================================================================================================================================*/
/* ExpenseDetails Controller End */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* ExpenseListController starts */
/*==============================================================================================================================================================*/

app.controller('ExpenseListController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory, $location,$anchorScroll) 
{
	// to set the title to the Expense 
    $scope.pageTitle = "EXPENSE LIST";

	// to get the id of the expense which was clicked..
    var expenseId = AppFactory.getExpenseId();
	// to store the objects where expense data is present
    $scope.valueArray = [];
	
	// to hide sidebar..
	$rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
    $scope.sideMenuController.toggleLeft();
    $rootScope.hideSideBar = true;
    }

	// LEFT BUTTON
    $scope.goBack = function() 
      {
        $state.go('app.ExpenseDetails');
      }   
  
    // function will call on clicking refresh button..
    $scope.init = function()
    {
        // loading indicator starts here...
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 10
            });
    
       // parameters to send with the requrest of getting data of expense list
            var parameters =
            { 
                "id": "customsearch_aavz_37",
				"internalid": expenseId,
				"recordcount": "0",
				"customer_type": "expensereport2" 
            };
            
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=117&deploy=1",null,parameters,function(results)
            { 

			for(mainArray in  results)
                {
                    obj =  results[mainArray];

                    for(objKey in obj)
                    {
                       var objValue = obj[objKey];
                       objValue.id=objKey;
                        // pushing the expense data into the valuearray
                       $scope.valueArray.push(objValue);
                        
                    }
                }
				// to hide the loading indicator 
                $ionicLoading.hide();
            });
                 
        }
        // to get the data first time...
        $scope.init();
});
/*==============================================================================================================================================================*/
/* ExpenseListController ends */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* EditExpenseController starts */
/*==============================================================================================================================================================*/

app.controller('EditExpenseController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, $location, $anchorScroll, AppFactory,$ionicModal,$ionicPopup) 
{
	// to get the id of the expense which was clicked..
     var caseId = AppFactory.getExpenseId();
        
          $scope.showConfirm = function() 
		  {

                    $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Any unsaved data will be lost. Do you want to continue?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $state.go('app.ExpenseDetails');
                        } 
                        else 
                        {
                         console.log('You are not sure');
                        }
                    });
           };


	// to give heading to page ...
     $scope.pageTitle="EDIT EXPENSE";
     
     $scope.goTo = function ()
     {
          $scope.showConfirm();
     }

		$scope.showAlert = function(message) 
		{
            $ionicPopup.alert({
              title: 'Validation Error',
              content: message
            }).then(function(res) {

            });
        };

     $scope.saveData = function()
     {

        if($scope.expense.trandate == undefined || $scope.expense.trandate == "")
        {
            $scope.showAlert("Please fill Date field");
            return;
        }

        //AppFactory.setType("EDIT EXPENSE");
        AppFactory.setExpense($scope.expense);
        $state.go('app.ExpenseListEdit');
     }

   // to begin the loading indicator ..
     $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 10
            });
    
    // to get the expense details to edit it.
    $scope.expenseDetails = AppFactory.getExpenseDetails();
    console.log(""+JSON.stringify($scope.expenseDetails));  
  // object which holds the data to be edited
    $scope.expense = {};
    $scope.expense.internalid=caseId;
    
  // to get the simple object to be edited from the array of objects ..
    for( i=0;i<$scope.expenseDetails.length;i++)
    {
		 var obj= $scope.expenseDetails[i];
 
		 for( a in obj)
		 {
		  if( a=="trandate")
		   {
			 $scope.expense.trandate=obj[a];
           var myDate = new Date(obj[a]);
           var month = myDate.getMonth()+1;
           
           $scope.no1=month;
           var day = myDate.getDate();
        
           $scope.no=day;
           var year = myDate.getFullYear();
           
           $scope.no2=year;
           $scope.expense.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
           }
			else if( a=="duedate")
                {
						$scope.expense.duedate=obj[a];
						var myDate = new Date(obj[a]);
						var month = myDate.getMonth()+1;
					
						$scope.no1=month;
						var day = myDate.getDate();
					  
						$scope.no=day;
						var year = myDate.getFullYear();
					
						$scope.no2=year;
						$scope.expense.duedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
        
                }
                else if( a=="memo")
                {
                    $scope.expense.memo=obj[a];
                }
                else if( a=="entity")
                {
					$scope.expense.entityvalue=obj['value'];
                    $scope.expense.entity=obj[a];
                } 
        }
    }
      // to stop the loading indicator..
         $ionicLoading.hide();
            
      /*------------------------- code for the "date" is started here----------------------------------*/
          $ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
          $scope.datemodal = modal;

        },
        {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
        });
        
        $scope.$on('$destroy', function() 
        {
            $scope.datemodal.remove();
        });
        $scope.opendateModal = function(str) 
        {
        
          $scope.dateType=str;
          $scope.datemodal.show();
        };
		$scope.cancel = function()
		{
			$scope.datemodal.hide();
		};
        $scope.closedateModal = function(model)
        {
			if(model == undefined || model == "")
			{
				$scope.datemodal.hide();
			}
			else
			{
			  $scope.datemodal.hide();
			  $scope.date = model;
			  var myDate = new Date($scope.date);
			  var month = myDate.getMonth()+1;
			  $scope.no1=month;
			  
			  var day = myDate.getDate();
			  $scope.no=day;
			  var year = myDate.getFullYear();
			  
			  $scope.no2=year;
			  if($scope.dateType=="trandate"){
			  $scope.expense.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			  }
			  if($scope.dateType=="duedate"){
			  $scope.expense.duedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			  }
		  }
        };
      
      /*------------------------- code for the "date" is ended here----------------------------------*/
  
});
/*==============================================================================================================================================================*/
/* EditExpenseController ends */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Expense ListEdit Controller starts */
/*==============================================================================================================================================================*/
app.controller('ExpenseListEditController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory, $location,$anchorScroll,$ionicPopup) 
{
		  /***************START CODE FOR DELETE checked selected Items ***************************/
    $scope.lengthOfArray= 0;      
    $scope.selectedItemsId=[];
    $scope.counter=0;
    $scope.flag=false;
    $scope.selectItems=function(id,itemvalue)
    {
        console.log("ID:"+id+"State:"+itemvalue);
		
        if(itemvalue==true)
        {               
            $scope.counter+=1;
            $scope.selectedItemsId.push(id);
        }   
		
        if(itemvalue==false)
        {   
            $scope.counter-=1;
            for(var i=0;i<$scope.selectedItemsId.length;i++)
            {
                if( $scope.selectedItemsId[i]==id)
                {
					$scope.selectedItemsId.splice(i, 1);
                }
            }
        }
    }

    $scope.deleteItems=function()
    {
		for(var i=0;i<$scope.valueArray.length;i++)
		{
			for(var j=0;j<$scope.selectedItemsId.length;j++)
			{
				if($scope.valueArray[i].id == $scope.selectedItemsId[j])
				{
					$scope.valueArray.splice(i, 1);
					$scope.counter-=1;
				}
			}                     
		}
        // to count length of array..
        $scope.lengthOfArray = $scope.valueArray.length;
		$scope.selectedItemsId=[];	
    }

    /************************************************CODE END OF ITEM DELETE **********/
		// store the list of items..
	   $scope.valueArray = [];
      
       // to know from which page we have entered here..
	   $scope.Previouspage =AppFactory.getType();
	   console.log(" page is "+ $scope.Previouspage);
	   if($scope.Previouspage=="ADD EXPENSE")
	   {
		 $scope.valueArray = [];
	   }
	   
	  // to get the id of the expense which was clicked..
      var expenseId = AppFactory.getExpenseId();
	  
	  $scope.valueArray=AppFactory.getNewItem();

       // to count length of array..
       $scope.lengthOfArray = $scope.valueArray.length;
       console.log(" lengthOfArray "+ $scope.lengthOfArray);
		
	  // the edited list item will get back here..
	  $scope.obj1=AppFactory.getData();
	  console.log(" edited object"+ JSON.stringify($scope.obj1));
	  
	  /* logic of editing the expense list item and updating it in the valuearray.....*/
    
    for(singleobj in $scope.valueArray)
    {
        var objValue= $scope.valueArray[singleobj];
                      
        // logic to update data which is edited ....
        if($scope.obj1.id == objValue.id)
        {
            objValue.trandate=$scope.obj1.trandate;
            objValue.amount=$scope.obj1.amount;
            objValue.expensecategoryvalue=$scope.obj1.expensecategoryvalue;
            objValue.expensecategory=$scope.obj1.expensecategory;
        }
          
    }
		   
	  // to set the title to the Expense 
	  $scope.pageTitle = "EXPENSE LIST";
	  $scope.ExpenseObj1={};
	  // it gets expense objects which is to be add at first in parameters..
	  $scope.ExpenseObj1=AppFactory.getExpense();
	  
	  // it will hide side bar..
	  $rootScope.enableSideBar = true;
		if ($rootScope.hideSideBar == false) 
		{
		$scope.sideMenuController.toggleLeft();
		$rootScope.hideSideBar = true;
		}

		
  
    $scope.goBack = function()
    {
        var emptyObject={};
        AppFactory.setNewItem(emptyObject);
      
        if($scope.Previouspage == "ADD EXPENSE")
        {
            $state.go('app.ExpenseAdd');
        }
        if($scope.Previouspage == "EDIT EXPENSE")
        {
            $state.go('app.EditExpense');
        }   
        if($scope.Previouspage == "TOP EXPENSE")
        {
            $state.go('app.EditExpense');
        }                           

	
    }

    /* it will execute when any of the list item will be clicked..*/
    $scope.gotoPage = function(value)
    {
		// it sets the expense id 
        AppFactory.setItem(value);
        // it redirects to the expense list item edit page
        $state.go('app.ExpenseListChange');
    }
  
    $scope.showEdit = function()
    {
        var length = $scope.valueArray.length;
        AppFactory.setLength(length);
        // it redirects to the edit page..
        $state.go('app.EditExpenseListItem');
    }
    $scope.showAlert = function(message) 
    {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

     $scope.saveExpenseData = function()
     {
        if($scope.lengthOfArray == 0)
        {
            $scope.showAlert("Please enter atleast one Expense");
            return;
        }
        else
        {    
            // add the expense object to the modified array.
            var parameters=[];
            parameters.push($scope.ExpenseObj1);
                    
            for( key in $scope.valueArray)
            {
              
              delete $scope.valueArray[key].id;
              delete $scope.valueArray[key].$$hashKey;
              parameters.push($scope.valueArray[key]);
            }
            
            // to begin the loading indicator ..
                  $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 10
                });
             if($scope.Previouspage == "ADD EXPENSE")
             {
                       // send the request to save the data.. 
                        AppFactory.sendHttpRequest("post","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=107&deploy=1",parameters,null,function(results)  
                        { 
    						if(results == "\"Expense record saved successfully.\"")
                            {
                                // to stop the loading indicator..
                                $ionicLoading.hide();
                                window.plugins.toast.show("Expense record saved successfully","short","center");
                                
                                var emptyObject={};
                                AppFactory.setNewItem(emptyObject);
                                // redirects on the expense page..
                                $state.go('app.Expense');
                            }
                            else
                            {
                                $scope.showAlert(results);
                                $ionicLoading.hide();
                                return;
                            }
                       });
            }
            if($scope.Previouspage == "EDIT EXPENSE")
             {
                        var emptyObject={};
                        AppFactory.setNewItem(emptyObject);
                       // send the request to save the data.. 
                        AppFactory.sendHttpRequest("put","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=107&deploy=1",parameters,null,function(results)  
                        { 
    						 if(results == "\"Record edited successfully.\"")
                            {
                                // to stop the loading indicator..
                                $ionicLoading.hide();
                                 window.plugins.toast.show("Record edited successfully","short","center");
                                 // redirects on the expense page..
                                $state.go('app.ExpenseDetails');
                            }    
                            else
                            {
                                $scope.showAlert(results);
                                $ionicLoading.hide();
                                return;
                            }
                       });
    				  
            }
    		if($scope.Previouspage == "TOP EXPENSE")
             {

    					var emptyObject={};
                        AppFactory.setNewItem(emptyObject);
                       // send the request to save the data.. 
                        AppFactory.sendHttpRequest("put","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=107&deploy=1",parameters,null,function(results)  
                        { 
    					    if(results == "\"Record edited successfully.\"")
                            {
                                // to stop the loading indicator..
                                $ionicLoading.hide();
                                 window.plugins.toast.show("Record edited successfully","short","center");
                                 // redirects on the expense page..
                                $state.go('app.ExpenseDetails');
                            }    
                            else
                            {
                                $scope.showAlert(results);
                                $ionicLoading.hide();
                                return;
                            }
                       });
    				
            }
		}
        
     }
		console.log(" value array length"+ $scope.valueArray.length); 		
  if($scope.valueArray.length==0 && ($scope.Previouspage=="TOP EXPENSE" || $scope.Previouspage=="EDIT EXPENSE"))
    {
			console.log(" web service called");
		    // loading indicator starts here...
           $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 10
            });
      
	 // parameters to send with the requrest of getting data of expense list
            var parameters =
            { 
                "id": "customsearch_aavz_37",
				"internalid": expenseId,
				"recordcount": "0",
				"customer_type": "expensereport2" 
            };
            // call the web service..
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=117&deploy=1",null,parameters,function(results)
            { 
				for(mainArray in  results)
                {
                    obj =  results[mainArray];
                    for(objKey in obj)
                    {
                       var objValue = obj[objKey];
                       objValue.id=mainArray;
                        // pushing the expense list data into the valuearray
                       $scope.valueArray.push(objValue);
                    }
                }
				console.log(" valueArray is"+ JSON.stringify($scope.valueArray));
                $scope.lengthOfArray = $scope.valueArray.length;
            });
            // to stop the loading indicator..
             $ionicLoading.hide();
            
    }   
     
   
});
/*==============================================================================================================================================================*/
/* Expense ListEdit Controller ends */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Expense ListChange Controller  starts */
/*==============================================================================================================================================================*/

app.controller('ExpenseListChangeController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory, $location,$anchorScroll,$ionicModal,$ionicPopup) 
{
    // to set the title to the Expense 
    $scope.pageTitle = "EDIT EXPENSE";

	// to get the expense list item which was clicked..
    $scope.expenseObject={};
    $scope.expenseObject= AppFactory.getItem(); 
	
  // to hide the sidebar...
  $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
    $scope.sideMenuController.toggleLeft();
    $rootScope.hideSideBar = true;
    }
   
    /* LEFT BUTTONS START*/ 
  
    $scope.goTo = function()
    {
        $state.go('app.ExpenseListEdit');
    }

		$scope.showAlert = function(message) {
            $ionicPopup.alert({
              title: 'Validation Error',
              content: message
            }).then(function(res) {

            });
          };

    /* RIGHT BUTTONS START*/ 

    $scope.saveData = function()
    {

		if($scope.expenseObject.expensecategoryvalue == undefined || $scope.expenseObject.expensecategoryvalue == "")
		{
			$scope.showAlert("Please fill Expense Category field");
			return;
		}
		else if($scope.expenseObject.trandate == undefined || $scope.expenseObject.trandate == "")
		{
			$scope.showAlert("Please fill Date field");
			return;
		}

       AppFactory.setData($scope.expenseObject);
        $state.go('app.ExpenseListEdit');
    }
      
    /*------------------------- code for the "date" is started here----------------------------------*/
          $ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
          $scope.datemodal = modal;

        },
        {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'

        });
        
        $scope.$on('$destroy', function() 
        {
            $scope.datemodal.remove();
        });
        
        $scope.opendateModal = function() 
		{
            $scope.datemodal.show();
        };
		$scope.cancel = function()
		{
			$scope.datemodal.hide();
		};
        $scope.closedateModal = function(model) 
		{
			if(model == undefined || model == "")
			{
				$scope.datemodal.hide();
			}
			else
			{
				$scope.datemodal.hide();
				$scope.date = model;
								
				var myDate = new Date($scope.date);
				var month = myDate.getMonth()+1;
				$scope.no1=month;
				
				var day = myDate.getDate();
				$scope.no=day;
				var year = myDate.getFullYear();
				
				$scope.no2=year;
				$scope.expenseObject.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			}
        };
      
      /*------------------------- code for the "date" is ended here----------------------------------*/
      
       /*------------------------- code for the "expenseCategory list" is started here----------------------------------*/
                
                // it retains data of the expenseCategory list
                $scope.expenseCategory = [];
                 //Load the modal from the given template URL
                $ionicModal.fromTemplateUrl('expenseCategory.html', function(expenseCategoryWindow) 
                {
                    $scope.expenseCategoryWindow = expenseCategoryWindow;
                }, 
                {
                    // Use our scope for the scope of the modal to keep it simple
                    scope: $scope,
                    // The animation we want to use for the modal entrance
                    animation: 'slide-in-up'
                });
                  // this function will be called when user clicks on the expenseCategorylist dropdown
                $scope.openexpenseCategoryWindow = function() 
                {
					// to show the model window where list of expenseCategory are there.
                    $scope.expenseCategoryWindow.show(); 
                    // to avoid duplication of data...
                    $scope.expenseCategoryList = [];
                    // it begins loading indicator 
                    $ionicLoading.show({
                            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching company list...",
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showDelay: 10
                        });
               
                        // expenseCategory list is inintialised as empty because it should not contain duplicated data.
                        $scope.expenseCategoryList= [];
                        // parameters to send with the requrest to get the list of expenseCategory ...
                        var parameters =
                            {
                              "field_name":"expensecategory",
							  "field_type":"select",
							  "field_subtext":"",
							  "customer_type":"expensereport2",
							  "field_dependent":"",
							  "field_dependentvalue":""
                            };
                            // it send the request to get data of expenseCategory..
                            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
                            { 
						for(mainArray in  results)
                                {
                                    obj =  results[mainArray];
                                     var temp={ };
                                     temp.index= obj[0];
                                     temp.value= obj[1];
                                     // push every object into the array named expenseCategorylist
                                     $scope.expenseCategoryList.push(temp);
                    
                                }
                
                                //to stop the loading indicator
                                $ionicLoading.hide();
                                                
                            });

                };
                
                 // this will be called when any of the expenseCategory will be selected ..
                $scope.expenseCategorySelected = function(expenseCategorydata)
                {
					// it hides the expenseCategoryWindow view
                    $scope.expenseCategoryWindow.hide();
                    // it holds the value of expenseCategory selected..
                    $scope.expenseObject.expensecategoryvalue = expenseCategorydata.value;
                  
                    // it holdes the index of the selected expenseCategory..
                    $scope.expenseObject.expensecategory = expenseCategorydata.index;
                }
                
                // this function will be called when cancel button will be clicked.
                $scope.closeexpenseCategoryWindow = function() 
                {
                    // it hides the expenseCategoryWindow view
                    $scope.expenseCategoryWindow.hide();
                };
                   //Be sure to cleanup the modal
                $scope.$on('$destroy', function() 
                {
                    $scope.expenseCategoryWindow.remove();
                });
                
    /* -------------------code of "expenseCategory list" is ended here --------------------- */
            
  
});
/*==============================================================================================================================================================*/
/* Expense ListChange Controller  ends */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Edit ExpenseList Item Controller  starts */
/*==============================================================================================================================================================*/

app.controller('EditExpenseListItemController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state,AppFactory, $location,$anchorScroll,$ionicModal,$ionicPopup) 
{

    // to set the title to the Expense ..
    $scope.pageTitle = "EDIT EXPENSE";
	
	// object which will hold the edited data..
	$scope.expenseObject={};
	// to get the length of expense list items....
    var length = AppFactory.getLength();
	// to know from which page we came..
	var Previouspage =AppFactory.getType(); 
	// to set the id for the object to be passed..
	$scope.expenseObject.id = length;
  
	// to hide sidebar..
	$rootScope.enableSideBar = true;
	if ($rootScope.hideSideBar == false) 
	{
		$scope.sideMenuController.toggleLeft();
		$rootScope.hideSideBar = true;
	}

	if(Previouspage == "EDIT EXPENSE")
	{
		$scope.expenseObject.line = (length+1).toString();
	}
    // LEFT BUTTON
     $scope.goTo = function()
    {
      $state.go('app.ExpenseListEdit');
    }

    $scope.showAlert = function(message) {
            $ionicPopup.alert({
              title: 'Validation Error',
              content: message
            }).then(function(res) {

            });
          };

    $scope.saveData = function()
    {

		if($scope.expenseObject.expensecategoryvalue == undefined || $scope.expenseObject.expensecategoryvalue == "")
		{
          $scope.showAlert("Please fill Expense Category field");
          return;
		}
		else if($scope.expenseObject.trandate == undefined || $scope.expenseObject.trandate == "")
		{
          $scope.showAlert("Please fill Date field");
          return;
		}

		AppFactory.setNewItem($scope.expenseObject);
		$state.go('app.ExpenseListEdit');
    }
     
    /*------------------------- code for the "date" is started here----------------------------------*/
          $ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
          $scope.datemodal = modal;

        },
        {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'

        });
        
        $scope.$on('$destroy', function() 
        {
            $scope.datemodal.remove();
        });
        
        $scope.opendateModal = function() {
        
          $scope.datemodal.show();
        };
		$scope.cancel = function()
		 {
			$scope.datemodal.hide();
		 };
        $scope.closedateModal = function(model)
		{
			if(model == undefined || model == "")
			{
				$scope.datemodal.hide();
			}
			else
			{
				  $scope.datemodal.hide();
				  $scope.date = model;
	
				var myDate = new Date($scope.date);
				var month = myDate.getMonth()+1;
				$scope.no1=month;
				
				var day = myDate.getDate();
				$scope.no=day;
				var year = myDate.getFullYear();
				
				$scope.no2=year;
				$scope.expenseObject.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			}	
        };
      
      /*------------------------- code for the "date" is ended here----------------------------------*/
      
      /*------------------------- code for the "expenseCategory list" is started here----------------------------------*/
                
                // it retains data of the expenseCategory list
                $scope.expenseCategory = [];
                 //Load the modal from the given template URL
                $ionicModal.fromTemplateUrl('expenseCategory.html', function(expenseCategoryWindow) 
                {
                    $scope.expenseCategoryWindow = expenseCategoryWindow;
                }, 
                {
                    // Use our scope for the scope of the modal to keep it simple
                    scope: $scope,
                    // The animation we want to use for the modal entrance
                    animation: 'slide-in-up'
                });
                  // this function will be called when user clicks on the expenseCategorylist dropdown
                $scope.openexpenseCategoryWindow = function() 
                {
					// to show the model window where list of expenseCategory are there.
                     $scope.expenseCategoryWindow.show();  
                    // to avoid duplication of data...
                    $scope.expenseCategoryList = [];
                    // it begins loading indicator 
                    $scope.loadingIndicator = $ionicLoading.show({
                            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching company list...",
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showDelay: 10
                        });
               
                        // expenseCategory list is inintialised as empty because it should not contain duplicated data.
                        $scope.expenseCategoryList= [];
                        // parameters to send with the requrest to get the list of expenseCategory ...
                        var parameters =
                            {
                              "field_name":"expensecategory",
							  "field_type":"select",
							  "field_subtext":"",
							  "customer_type":"expensereport2",
							  "field_dependent":"",
							  "field_dependentvalue":""
                            };
                            // it send the request to get data of expenseCategory..
                            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
                            { 
								for(mainArray in  results)
                                {
                                    obj =  results[mainArray];
                                     var temp={ };
                                     temp.index= obj[0];
                                     temp.value= obj[1];
                                     // push every object into the array named expenseCategorylist
                                     $scope.expenseCategoryList.push(temp);
                    
                                }
                
                                //to stop the loading indicator
                                $scope.loadingIndicator.hide();
                                               
                            });



                };
                
                   // this will be called when any of the expenseCategory will be selected ..
                $scope.expenseCategorySelected = function(expenseCategorydata)
                {
				// it hides the expenseCategoryWindow view
                    $scope.expenseCategoryWindow.hide();
                    // it holds the value of expenseCategory selected..
                    $scope.expenseObject.expensecategoryvalue = expenseCategorydata.value;
                  
                    // it holdes the index of the selected expenseCategory..
                    $scope.expenseObject.expensecategory = expenseCategorydata.index;
                }
                
                // this function will be called when cancel button will be clicked.
                $scope.closeexpenseCategoryWindow = function() 
                {
                    // it hides the expenseCategoryWindow view
                    $scope.expenseCategoryWindow.hide();
                };
                   //Be sure to cleanup the modal
                $scope.$on('$destroy', function() 
                {
                    $scope.expenseCategoryWindow.remove();
                });
                
    /* -------------------code of "expenseCategory list" is ended here --------------------- */
});
/*==============================================================================================================================================================*/
/* Edit ExpenseList Item Controller  ends */
/*==============================================================================================================================================================*/

/*==============================================================================================================================================================*/
/* Expense Add Controller starts */
/*==============================================================================================================================================================*/

/*------------------------------------------------- Add Expense Controller starts here----------- */
app.controller('ExpenseAddController', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $ionicActionSheet, $state, $location, $anchorScroll, AppFactory,$ionicModal,$ionicPopup) 
{

     // to give heading to page ...
    $scope.pageTitle="ADD EXPENSE";

    // object which holds the data to be edited
    $scope.expense = {};
    $lastObj = AppFactory.getLastObj();
    $scope.expense.entityvalue=$lastObj.entity;
    $scope.expense.entity="965";

    /*------------------------- code for the "date" is started here----------------------------------*/
    $ionicModal.fromTemplateUrl('datemodal.html', function(modal) {
            $scope.datemodal = modal;
        },
        {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope, 
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
    });

    $scope.$on('$destroy', function() 
    {
       $scope.datemodal.remove();
    });
    
    $scope.opendateModal = function(str)
    {   
        $scope.dateType=str;
        $scope.datemodal.show();
    };
	$scope.cancel = function()
	 {
		$scope.datemodal.hide();
	 };
    $scope.closedateModal = function(model)
    {
		if(model == undefined || model == "")
		{
			$scope.showAlert("Please fill Date field");
		}
		else
		{
			$scope.datemodal.hide();
			$scope.date = model;

			var myDate = new Date($scope.date);
			var month = myDate.getMonth()+1;
			$scope.no1=month;

			var day = myDate.getDate();
			$scope.no=day;
			var year = myDate.getFullYear();

			$scope.no2=year;
			if($scope.dateType=="trandate")
			{
				$scope.expense.trandate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			}
			if($scope.dateType=="duedate")
			{
				$scope.expense.duedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
			}
		}
    };
  
  /*------------------------- code for the "date" is ended here----------------------------------*/

    $scope.showConfirm = function() {
        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.Expense');
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };

    $scope.goTo = function()
	{
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) 
	{
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };
	// this will be called when the save button will be clicked..
    $scope.saveData = function()
    {
        if($scope.expense.entityvalue == undefined || $scope.expense.entityvalue == "")
        {
            $scope.showAlert("Please fill Name field");
            return;
        }
        else if($scope.expense.trandate == undefined || $scope.expense.trandate == "")
        {
            $scope.showAlert("Please fill Date field");
            return;
        }
        else if($scope.expense.duedate == undefined || $scope.expense.duedate == "")
        {
            $scope.showAlert("Please fill Due Date field");
            return;
        }
        

        // to set the type from which we came....
        AppFactory.setType("ADD EXPENSE");
        // to set the form data into the service..
        AppFactory.setExpense($scope.expense);
        $state.go('app.ExpenseListEdit');
    }   
});
/*==============================================================================================================================================================*/
/* Expense Add Controller ends */
/*==============================================================================================================================================================*/


