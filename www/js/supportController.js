/*-------------------------------------------------------Case list  Controller------------------------------------------- */
app.controller('ModelWindowController', function($scope)
{

    $scope.searchString ="";
    $scope.clearSearch = function () 
    {
        $scope.searchString="";
    }

});

app.controller('CaseListController', function($scope, $rootScope, $http, $ionicLoading, AppFactory, $stateParams, $state, $ionicActionSheet, $location, $anchorScroll, $timeout, $filter, $ionicPopup, $ionicScrollDelegate, $rootScope) 
{

    /*Clear Search Box*/
    $scope.clearSearch = function () 
    {
        $scope.searchString="";
    }    
    // to back to home..
    $scope.goHome = function ()
    {
        $state.go('app.homepage');
    }

    // to know from which page we are coming..
    var PreviousPage = AppFactory.getType();
	// to disable sidebar when page loads
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }
    
    if($stateParams.category == "TOPCASE")
    {
        // to set the title to the case 
        $scope.pageTitle = "LAST 5 CASES";
        $rootScope.sideMenuEnabled = false;
    }
    else if($stateParams.category == "CASE")
    {
         // to set the title to the case 
        $scope.pageTitle = "CASE";
        $rootScope.sideMenuEnabled = true;
    }
    else if($stateParams.category == "OPENCASE")
    {
        // to set the title to the case 
        $scope.pageTitle = "All OPEN CASES";
        $rootScope.sideMenuEnabled = false;
    }
     // to store the objects where case data is present
    $scope.valueArray = [];
    $scope.allData = [];
    $scope.showMessage = false;
    $scope.orderString = "";

    /*----------functions---------------*/
    // function will call on clicking refresh button..
    $scope.init = function()
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        $scope.orderString = "";
        // to store the objects where case data is present
        $scope.valueArray = [];
	    // loading indicator starts here...
       $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
            
            if($stateParams.category == "TOPCASE")
            {
                // parameters to send with the requrest of getting data of case list
                var parameters =
                { 
                    "id":"customsearch_aavz_32",
                    "customer_type":"supportcase",
                    "lastfivecase":"1"  
                };
                
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=94&deploy=1",null,parameters,function(results) 
                //get result from json
                {
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
                            // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                           
                        }
                    }
					// to hide the loading indicator 
					$ionicLoading.hide();
                });
            }
            else if($stateParams.category == "CASE")
            {
                // parameters to send with the requrest of getting data of case list
                var parameters =
                { 
                    "id":"customsearch_aavz_32",
                    "customer_type":"supportcase",
                    "recordcount":"0"  
                };
                
                AppFactory.getCaseList("0", $scope.orderString,function(results)
                {
                    $scope.allData = results;
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
						   $scope.subject = objValue.Subject;
						   //console.log("subject is "+$scope.subject);
                           // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                           
                        }
                    }
                    $scope.arrayLength=$scope.valueArray.length;//get valueArray length used for server-search
					// to hide the loading indicator 
					$ionicLoading.hide();
                });
            }
            else if($stateParams.category == "OPENCASE")
            {
                // parameters to send with the requrest of getting data of case list
                var parameters =
                { 
                    "id":"customsearch_aavz_32",
                    "customer_type":"supportcase",
                    "opencase":"1"  
                };
                
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=94&deploy=1",null,parameters,function(results) 
                //get result from json
                {
                    //$scope.allData = results;
                    console.log(" result of case list ="+JSON.stringify(results));
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
                            // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                           
                        }
                    }
					// to hide the loading indicator 
					$ionicLoading.hide();
                
                });
            }
    }
    // to get the data first time...                
    $scope.init();



    /**************SEARCH FROM SERVER START******************************************/

    //used in server-search
    $scope.arrayLength=0;
    $scope.recordCount=0;
    //Code for Search data filter from server
    $scope.newflag="false";
    $scope.searchString="";
    $scope.listflag =1;
    $scope.counter=$scope.valueArray.length;    
    $scope.messageFlag="false";

    $scope.$watch("searchString", function(query){
    $scope.counter = $filter("filter")($scope.valueArray, query).length;
    console.log("Count : "+$scope.counter);
       
        if($scope.listflag == 1) 
        {
            $scope.listflag = 0;  
            $scope.newflag="false";       
        }
        else if($scope.counter==0 && $scope.listflag ==0)
        {    
            $scope.message="No results found.Continue search on server.";  
            $scope.newflag="true"; 
            $scope.messageFlag="false";
        } 
        else if($scope.counter == 0 && $scope.listflag == 0 && $scope.pageTitle == "LAST 5 CASES")
        {    
            $scope.message="No results found.";  
            $scope.newflag="true";             
        }
        else
        {
            $scope.newflag="false";
            $scope.messageFlag="false";
            $ionicScrollDelegate.scrollTop();
        }

        if($scope.searchString.length == 0)
        {
            console.log("Count : "+$scope.counter+" search string:"+$scope.searchString);
            $scope.counter = 0;
        }

        if($scope.recordCount>0)
        {
            $scope.valueArray.splice($scope.arrayLength,$scope.recordCount);
        }

    });//end of $watch()


    //Calling web services for server search

    $scope.mySearchList=function()
    {
        $scope.recordCount=0;
        //display loading indicator
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        var parameters = 
        {
           "id":"customsearch_aavz_32",
           "customer_type":"supportcase",                
           "search":$scope.searchString,    
           "searchkey":"Subject"               
        };

        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=94&deploy=1",null,parameters,function(results) { //get result from json
            //console.log("Response Data:"+results);
            if(results.length ==0 )
            {
                $scope.message="No results found on server.";
                $scope.newflag="false";
                $scope.messageFlag="true";
            }
            else
            {
                $scope.newflag="false";
                $scope.messageFlag="false";
                $scope.counter = results.length; 
            }

            var mainArray=results;
            for(i in mainArray)
            {
                var mainObj = mainArray[i]; //get single object of array
                //console.log(JSON.stringify(mainObj));
                for(key in mainObj)
                {  
                    $scope.recordCount+=1;
                    //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                    var singleObject=mainObj[key];  //create a single object by using key
                    singleObject.id=key;//Add key as attribute in singleObj
                    $scope.valueArray.push(singleObject); //insert data into array of objects
                } //end of key forloop
            }//end of mainArray                           
            $ionicLoading.hide();
        });//end of sendHttpRequest()
    }//end of mySearchList()


    /**************SEARCH FROM SERVER END******************************************/

    $scope.loadSortedData = function()
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        // to store the objects where case data is present
        $scope.valueArray = [];
        // loading indicator starts here...
       $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
            
            if($stateParams.category == "TOPCASE")
            {
                // parameters to send with the requrest of getting data of case list
                var parameters =
                { 
                    "id":"customsearch_aavz_32",
                    "customer_type":"supportcase",
                    "lastfivecase":"1",
                    "sortfieldname":$scope.orderString  
                };
                
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=94&deploy=1",null,parameters,function(results) 
                //get result from json
                {
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
                            // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                           
                        }
                    }
                    // to hide the loading indicator 
                    $ionicLoading.hide();
                });
            }
            else if($stateParams.category == "CASE")
            {
                // parameters to send with the requrest of getting data of case list
                var parameters =
                { 
                    "id":"customsearch_aavz_32",
                    "customer_type":"supportcase",
                    "recordcount":"0",
                    "sortfieldname":$scope.orderString  
                };
                
                AppFactory.getCaseList("0", $scope.orderString,function(results)
                {
                    $scope.allData = results;
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
                           $scope.subject = objValue.Subject;
                           //console.log("subject is "+$scope.subject);
                           // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                           
                        }
                    }
                    $scope.arrayLength=$scope.valueArray.length;//get valueArray length used for server-search
                    // to hide the loading indicator 
                    $ionicLoading.hide();
                });
            }
            else if($stateParams.category == "OPENCASE")
            {
                // parameters to send with the requrest of getting data of case list
                var parameters =
                { 
                    "id":"customsearch_aavz_32",
                    "customer_type":"supportcase",
                    "opencase":"1",
                    "sortfieldname":$scope.orderString  
                };
                
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=94&deploy=1",null,parameters,function(results) 
                //get result from json
                {
                    //$scope.allData = results;
                    console.log(" result of case list ="+JSON.stringify(results));
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id=objKey;
                            // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                           
                        }
                    }
                    // to hide the loading indicator 
                    $ionicLoading.hide();
                
                });
            }
    }

    $scope.loadNextData1 = function()
    {
        console.log(" next  function called");
        //checking whether index is between last 5
        /*if(index > ($scope.valueArray.length-5) && index <=($scope.valueArray.length-1))
        {
            //if restlet is not called for the range
            if($scope.flag==1)
            {*/
                  $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                });

                var temp = $scope.valueArray.length;
                AppFactory.getCaseList(temp,$scope.orderString,function(results)
                {
                    console.log(" next result of case list ="+JSON.stringify(results));
                    $scope.allData = results;
                    
                    if($scope.allData.length == 0)
                    {
                        $scope.showMessage = true;
                    }

                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];

                        for(objKey in obj)
                        {
                           var objValue = obj[objKey];
                           objValue.id = objKey;
                            // pushing the case data into the valuearray
                           $scope.valueArray.push(objValue);
                        }
                    }
                     // to hide the loading indicator 
                    $ionicLoading.hide();
                });
                $scope.flag=0;
            /*}
        }
        else
        {
            $scope.flag=1;
        }*/
    }

    // to goto top of list ..
    $scope.gotoTop = function ()
    {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        $location.hash('top');

        // call $anchorScroll()
        $anchorScroll();
    };
            
                
    // button to add new case..
    $scope.goTo = function() 
    {
        $state.go('app.caseAdd');
		//AppFactory.setCategory($stateParams.category);
		 if($stateParams.category == "TOPCASE")
        {
            AppFactory.setType('TopCase');
        }
        else if($stateParams.category == "CASE")
        {
            AppFactory.setType('Case');
        }
        else if($stateParams.category == "OPENCASE")
        {
            AppFactory.setType('OpenCase');
        }
    }

    // this fuction will be called when any list item of case will be clicked
    $scope.goto = function(case_id)
    {
        // it sets the case contact id 
        AppFactory.setCaseId(case_id);
        
        if($stateParams.category == "TOPCASE")
        {
            AppFactory.setType('TopCase');
        }
        else if($stateParams.category == "CASE")
        {
            AppFactory.setType('Case');
        }
        else if($stateParams.category == "OPENCASE")
        {
            AppFactory.setType('OpenCase');
        }
        // it redirects to the case details page
        $state.go('app.caseDetail');
    }
            
        // function to sort the list by various ways..
    $scope.showSortList = function() 
    {
        // Show the action sheet
        $ionicActionSheet.show(
        {
            titleText: 'Sort By',
            buttons: [
                { text: 'Status', },
				{ text: 'Company', },
				{ text: 'Case Number', },
                { text: 'Subject' }
            ],

            /* Cancel Button */
            cancelText: 'Cancel',
            cancel: function() 
            {
                console.log('CANCELLED');
            },
			
		    /* Button Click Functionality */
            buttonClicked: function(index) 
            {
				if (index==0) 
                {
						
					$ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Status?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $scope.orderString = 'Status';
                            $scope.loadSortedData();
                        } 
                        else 
                        {
                         console.log('You are not sure');
                        }
                    });
                   
                };
				 if (index==1) 
                {
					$ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Company?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $scope.orderString = 'Company';
                            $scope.loadSortedData();
                        } 
                        else 
                        {
                         console.log('You are not sure');
                        }
                    });
                    
                };
				 if (index==2) 
                {
					$ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Case Number?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $scope.orderString = 'Case Number';
                            $scope.loadSortedData();
                        } 
                        else 
                        {
                         console.log('You are not sure');
                        }
                    });
                    
                };
                if (index==3) 
                {
                    $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Subject?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $scope.orderString = 'Subject';
                            $scope.loadSortedData();
                        } 
                        else 
                        {
                         console.log('You are not sure');
                        }
                    });
                };
				return true;
            }
        });
    };       
});
/*-------------------------------------------------------Case list  Controller end-------------------------------------------------*/

/*-----------------------------------------------Case Detail Controller---------------------------------------*/
app.controller('CaseDetailController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout) 
{
    $scope.showPage = false;
    // to disable sidebar when page loads
    $rootScope.sideMenuEnabled = false;
    
    // to set the title to the case 
    $scope.pageTitle = "CASE DETAIL";
    
    // to get the id of the case which was clicked..
    $scope.caseId = AppFactory.getCaseId();
  
    // to store the keys of the objects 
    $scope.key = [];
    // to store the objects 
    $scope.value = [];

   // to begin the loading indicator ..
    $ionicLoading.show({
        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showdelay: 300
    });

    // parameters to set along with the request..
    var parameters =
    { 
        "id":"customsearch_aavz_30",
        "noofrecords":"100",
        "internalid":$scope.caseId,
        "display":"6,3,5,2,4,11,8,7,9,10,1",
        "groupname":"Incident Information,Incident Information,Incident Information,Incident Information,Incident Information,Default,Primary Information,Primary Information,Primary Information,Primary Information,Incident Information",
        "fieldname":"item,assigned,priority,status,category,casenumber,company,title,email,internalid,startdate",
        "customer_type":"supportcase"
    };
      
     
    //Request for get data of the case details
    AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=101&deploy=1",null,parameters,function(results)
    { 
    
        for(singleObj in  results)
        {
            obj =  results[singleObj];
    
            for(innerobj in obj)
            {
                obj1 = obj[innerobj];
  
                for(mainobj in obj1)
                { 
                    var objValue = obj1[mainobj];
                             
                    for(objkey in objValue)
                    {
                        if((objkey !="value")&&(objkey!="prioritylabel")&&(objkey!="label")&&(objkey!="section_header"))
                        {
                            obj1[mainobj].gotValue = objValue[objkey];
                        }
      
                    } 
                    // to store the data of case details
                    $scope.value.push(objValue);
                    // store the keys of the object..
                    $scope.key.push(mainobj);
 
                }
 
                 // to sort the case details data on the basis of prioritylabel .. 
                $scope.value.sort(function(a, b)
                {
                    return a.prioritylabel - b.prioritylabel;
                });
            }
        }
                
        // to remove the last line of the value array ..
        $scope.value.splice($scope.value.length-1,1);
        // to stop the loading indicator ..
        $scope.showPage = true;
        $ionicLoading.hide();
    });

    // back button to go on the list page..
    $scope.goBack = function() 
    {
		var pageType = AppFactory.getType();
		if(pageType == "TopCase")
		{
			$state.go('app.caseList',{category:'TOPCASE'});
			console.log(" backed successfully");
		}	
		else if(pageType == "Case")
		{
			$state.go('app.caseList',{category:'CASE'});
		}
		else if(pageType == "OpenCase")
		{
			$state.go('app.caseList',{category:'OPENCASE'});
		}
        
    }

    // edit button to go onto the edit page..
    $scope.goToEdit = function() 
    {
        AppFactory.setCaseDetails($scope.value);
        $state.go('app.caseEdit');
    }

    $scope.goto=function()
    {
        $state.go('app.messageList');
    }
});     
/*----------------------------------------------Case Detail Controller end------------------------------------------------------------*/

/*------------------------------------------------Case Add Controller-----------------------------------------------------*/

app.controller('CaseAddController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout,$ionicModal,$ionicPopup) 
{


    // to disable the side bar
    $rootScope.enableSideBar = false;
    // to set the title to the case 
    $scope.myTitle = "ADD CASE";
     // it is empty object where data will be kept which is to add .
    $scope.case = {};
    
    /*------------------------- code for the "date" is started here----------------------------------*/
    $ionicModal.fromTemplateUrl('datemodal.html', function(modal) 
    {
        $scope.datemodal = modal;
    },
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.$on('$destroy', function() 
    {
        $scope.datemodal.remove();
    });

    $scope.opendateModal = function() 
    {
        $scope.datemodal.show();
    };

    $scope.cancel = function()
    {
        $scope.datemodal.hide();
    };

    $scope.closedateModal = function(model) 
	{
        if(model == undefined || model == "")
        {
            $scope.showAlert("please fill Date field");
        }
        else
        {
            $scope.datemodal.hide();
            $scope.date = model;
            var myDate = new Date($scope.date);
            var month = myDate.getMonth()+1;
            console.log(" month is "+ month);
            $scope.no1=month;
            var day = myDate.getDate();
            console.log(" day is "+ day);
            $scope.no=day;
            var year = myDate.getFullYear();
            console.log(" year is "+ year);
            $scope.no2=year;
            $scope.case.startdate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
            console.log("date to be send="+$scope.case.startdate);
        }
    };
            
    /*------------------------- code for the "date" is ended here----------------------------------*/
            
        
    /*------------------------- code for the status list is started here----------------------------------*/
    
    // it retains data of the status list
    $scope.statusList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('status.html', function(statusWindow) 
    {
        $scope.statusWindow = statusWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
      // this function will be called when user clicks on the statuslist dropdown
    $scope.openStatusWindow = function() 
    {
        // to show the model window where list of status are there.
        $scope.statusWindow.show();  
        // to avoid duplication of data...
        $scope.statusList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching status list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // status list is inintialised as empty because it should not contain duplicated data.
        $scope.countryList= [];
        // parameters to send with the requrest to get the list of status ...
        var parameters =
        {
            "field_name":"status",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };
        // it send the request to get data of status..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        { 
                  
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named statuslist
                 $scope.statusList.push(temp);

            }
            // to stop the loading indicator
            $ionicLoading.hide();
                           
        });

    };
    
       // this will be called when any of the status will be selected ..
    $scope.statusSelected = function(statusdata)
    {
        // it hides the statusWindow view
        $scope.statusWindow.hide();
        // it holds the value of status selected..
        $scope.case.statusvalue = statusdata.value;
      
        // it holdes the index of the selected status..
        $scope.case.status = statusdata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeStatusWindow = function() 
    {
        // it hides the countryWindow view
        $scope.statusWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.statusWindow.remove();
    });
    
    /* ---------------------------------code of status list is ended here ----------------------------------------------- */
    
    /*------------------------- code for the "assigned to list" is started here----------------------------------*/
    
    // it retains data of the assigned to list
    $scope.assignedToList = [];
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('assignedTo.html', function(assignedToWindow) 
    {
        $scope.assignedToWindow = assignedToWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    // this function will be called when user clicks on the assigned to list dropdown
    $scope.openAssignedToWindow = function() 
    {
        // to show the model window where list of assigned to are there.
        $scope.assignedToWindow.show();
        // to avoid duplication of data...
        $scope.assignedToList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching assigned list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // assigned to list is inintialised as empty because it should not contain duplicated data.
        $scope.assignedToList= [];
        // parameters to send with the requrest to get the list of assigned to ...
        var parameters =
        {
            "field_name":"assigned",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };

        // it send the request to get data of assigned to..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {      
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named assigned to list
                 $scope.assignedToList.push(temp);

            }
            // to stop the loading indicator
            $ionicLoading.hide();               
        });
    };
    
       // this will be called when any of the assigned to will be selected ..
    $scope.assignedToSelected = function(assignedTodata)
    {
        // it hides the countryWindow view
        $scope.assignedToWindow.hide();
        // it holds the value of assigned to selected..
        $scope.case.assignedToValue = assignedTodata.value;
      
        // it holdes the index of the selected assigned to..
        $scope.case.assignedTo = assignedTodata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeAssignedToWindow = function() 
    {
        // it hides the countryWindow view
        $scope.assignedToWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.assignedToWindow.remove();
    });
    
    /* ---------------------------------code of "assigned to list" is ended here ----------------------------------------------- */
                
    
    /*------------------------- code for the "type list" is started here----------------------------------*/
    
    // it retains data of the type list
    $scope.typeList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('type.html', function(typeWindow) 
    {
        $scope.typeWindow = typeWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
      // this function will be called when user clicks on the typelist dropdown
    $scope.openTypeWindow = function() 
    {
        $scope.typeWindow.show();  
        // to avoid duplication of data...
        $scope.typeList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching type list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
   
        // type list is inintialised as empty because it should not contain duplicated data.
        $scope.typeList= [];
        // parameters to send with the requrest to get the list of type ...
        var parameters =
        {
            "field_name":"category",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };

        // it send the request to get data of type..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {   
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named typelist
                 $scope.typeList.push(temp);
            }
            //to stop the loading indicator
            $ionicLoading.hide();
            // to show the model window where list of type are there.
                           
        });
    };
    
       // this will be called when any of the type will be selected ..
    $scope.typeSelected = function(typedata)
    {
        // it hides the typeWindow view
        $scope.typeWindow.hide();
        // it holds the value of type selected..
        $scope.case.typeValue = typedata.value;
      
        // it holdes the index of the selected type..
        $scope.case.type = typedata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closetypeWindow = function() 
    {
        // it hides the typeWindow view
        $scope.typeWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.typeWindow.remove();
    });
    
    /* ---------------------------------code of "type list" is ended here ----------------------------------------------- */
                
    
    /*------------------------- code for the "priority list" is started here----------------------------------*/
    
    // it retains data of the priority list
    $scope.priorityList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('priority.html', function(priorityWindow) 
    {
        $scope.priorityWindow = priorityWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    // this function will be called when user clicks on the prioritylist dropdown
    $scope.openPriorityWindow = function() 
    {
        $scope.priorityWindow.show();
        // to avoid duplication of data...
        $scope.priorityList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching priority list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
   
        // priority list is inintialised as empty because it should not contain duplicated data.
        $scope.priorityList= [];
        // parameters to send with the requrest to get the list of priority ...
        var parameters =
        {
            "field_name":"priority",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };
        // it send the request to get data of priority..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {     
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                var temp={ };
                temp.index= obj[0];
                temp.value= obj[1];
                // push every object into the array named prioritylist
                $scope.priorityList.push(temp);
            }
            //to stop the loading indicator
            $ionicLoading.hide();
            // to show the model window where list of countries are there.                
        });
    };
    
       // this will be called when any of the country will be selected ..
    $scope.prioritySelected = function(prioritydata)
    {
        // it hides the priorityWindow view
        $scope.priorityWindow.hide();
        // it holds the value of priority selected..
        $scope.case.priorityValue = prioritydata.value;
      
        // it holdes the index of the selected priority..
        $scope.case.priority = prioritydata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closePriorityWindow = function() 
    {
        // it hides the countryWindow view
        $scope.priorityWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.priorityWindow.remove();
    });
    
    /* ---------------------------------code of "priority list" is ended here ----------------------------------------------- */
                
                
    /*------------------------- code for the "item list" is started here----------------------------------*/
    
    // it retains data of the item list
    $scope.itemList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('item.html', function(itemWindow) 
    {
        $scope.itemWindow = itemWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    // this function will be called when user clicks on the itemlist dropdown
    $scope.openItemWindow = function() 
    {
        // to show the model window where list of countries are there.
        $scope.itemWindow.show();  
        // to avoid duplication of data...
        $scope.itemList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching item list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // item list is inintialised as empty because it should not contain duplicated data.
        $scope.itemList= [];
        // parameters to send with the requrest to get the list of item ...
        var parameters =
        {
            "field_name":"item",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };
        // it send the request to get data of item..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        { 
                  
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named countrylist
                 $scope.itemList.push(temp);

            }
            //to stop the loading indicator
            $ionicLoading.hide();
        });
    };
    
       // this will be called when any of the item will be selected ..
    $scope.itemSelected = function(itemdata)
    {
        // it hides the itemWindow view
        $scope.itemWindow.hide();
        // it holds the value of item selected..
        $scope.case.itemValue = itemdata.value;
      
        // it holdes the index of the selected item..
        $scope.case.item = itemdata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeItemWindow = function() 
    {
        // it hides the itemWindow view
        $scope.itemWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.itemWindow.remove();
    });
    
    /* ---------------------------------code of "item list" is ended here ----------------------------------------------- */
                
        
    /*------------------------- code for the "company list" is started here----------------------------------*/
    
    // it retains data of the company list
    $scope.companyList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('company.html', function(companyWindow) 
    {
        $scope.companyWindow = companyWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    // this function will be called when user clicks on the companylist dropdown
    $scope.openCompanyWindow = function() 
    {
        // to show the model window where list of company are there.
        $scope.companyWindow.show();
        // to avoid duplication of data...
        $scope.companyList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching company list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // company list is inintialised as empty because it should not contain duplicated data.
        $scope.companyList= [];
        // parameters to send with the requrest to get the list of company ...
        var parameters =
        {
            "field_name":"company",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };

        // it send the request to get data of company..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {      
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named companylist
                 $scope.companyList.push(temp);

            }
            //to stop the loading indicator
            $ionicLoading.hide();             
        });
    };
    
       // this will be called when any of the company will be selected ..
    $scope.companySelected = function(companydata)
    {
        // it hides the companyWindow view
        $scope.companyWindow.hide();
        // it holds the value of company selected..
        $scope.case.companyValue = companydata.value;
      
        // it holdes the index of the selected company..
        $scope.case.company = companydata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeCompanyWindow = function() 
    {
        // it hides the companyWindow view
        $scope.companyWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.companyWindow.remove();
    });
    
    /* ---------------------------------code of "company list" is ended here ----------------------------------------------- */
    $scope.showConfirm = function() 
    {
        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
               // $state.go('app.caseList');
			   	var pageType = AppFactory.getType();
				if(pageType == "TopCase")
				{
					$state.go('app.caseList',{category:'TOPCASE'});
					console.log(" backed successfully");
				}	
				else if(pageType == "Case")
				{
					$state.go('app.caseList',{category:'CASE'});
				}
				else if(pageType == "OpenCase")
				{
					$state.go('app.caseList',{category:'OPENCASE'});
				}
        
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };         
    
    // cancel button to go to the company list page.
    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }


    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    //  save button to save the filled data on the form ..
    $scope.saveData = function() 
    {

        // Validation for mandatory fields on form
        if($scope.case.title == undefined || $scope.case.title == "")
        {
            $scope.showAlert("Please fill Subject field");
            return;
        }
        else if($scope.case.companyValue == undefined || $scope.case.companyValue == "")
        {
            $scope.showAlert("Please fill Company field");
            return;
        }
        else if($scope.case.startdate == undefined || $scope.case.startdate == "")
        {
            $scope.showAlert("Please fill Incident Date field");
            return;
        }
        else if($scope.case.statusvalue == undefined || $scope.case.statusvalue == "")
        {
            $scope.showAlert("Please fill Status field");
            return;
        }


        // to begin the loading indicator ..
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
        // to set the parameters to send along with the request..
        var parameters = 
        {
            "categoryvalue":$scope.case.typeValue,
            "statusvalue":$scope.case.statusvalue,
            "status":$scope.case.status,
            "assigned":$scope.case.assignedTo,
            "assignedvalue":$scope.case.assignedToValue,
            "itemvalue":$scope.case.itemValue,
            "category":$scope.case.type,
            "title":$scope.case.title,
            "startdate":$scope.case.startdate, 
            "casenumber":$scope.case.casenumber,
            "email":$scope.case.email,
            "priority":$scope.case.priority,
            "companyvalue": $scope.case.companyValue,
            "item":$scope.case.item,
            "company":$scope.case.company,
            "priorityvalue":$scope.case.priorityValue
        };
        // send the request to save the data..                    
        AppFactory.sendHttpRequest("post","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=101&deploy=1",parameters,null,function(results) 
        { 
            if(results == "\"Case record saved successfully.\"")
            {
                // to stop the loading indicator..
                $ionicLoading.hide();
                 window.plugins.toast.show("Case record saved successfully","short","center");
                // redirects on the list page..
                //$state.go('app.caseList');
                 var pageType = AppFactory.getType();
                 console.log("page type is "+pageType);
                if(pageType == "TopCase")
                {
                    $state.go('app.caseList',{category:'TOPCASE'});
                    console.log(" backed successfully");
                }   
                else if(pageType == "Case")
                {
                    $state.go('app.caseList',{category:'CASE'});
                }
                else if(pageType == "OpenCase")
                {
                    $state.go('app.caseList',{category:'OPENCASE'});
                }
            }
            else
            {
                $scope.showAlert(results);
                $ionicLoading.hide();
                return;
            }
        });
    }
});
/*----------------------------------------------Case Add Controller end------------------------------------------------------------*/

/*--------------------------------------------------- Edit case controller starts here--------------------------------------*/

app.controller('CaseEditController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout,$ionicModal,$filter,$ionicPopup) 
{                                               
	

    // to get the id of the case which was clicked..
        var caseId = AppFactory.getCaseId();
    // to give heading to page ...
        $scope.myTitle="EDIT CASE";
    // to begin the loading indicator ..
    $ionicLoading.show({
        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showdelay: 300
    });
    
    // to get the case details to edit it.
    $scope.caseDetails = AppFactory.getCaseDetails();
    // object which holds the data to be edited
    $scope.case = {};
    
    // to get the simple object to be edited from the array of objects ..
    for( i=0;i<$scope.caseDetails.length;i++)
    {
        var obj= $scope.caseDetails[i];
        for( a in obj)
        {
            if( a=="startdate")
            {
            $scope.case.startdate=obj[a];
            var myDate = new Date(obj[a]);
            var month = myDate.getMonth()+1;
                console.log(" month is "+ month);
                $scope.no1=month;
                
                var day = myDate.getDate();
                console.log(" day is "+ day);
                $scope.no=day;
                var year = myDate.getFullYear();
                console.log(" year is "+ year);
                $scope.no2=year;
                $scope.case.startdate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
                console.log("existing date="+$scope.case.startdate);
            
            }
            else if( a=="status")
            {
                $scope.case.statusValue=obj[a];
                $scope.case.status=obj["value"];
            }
            else if( a=="assigned")
            {
                $scope.case.assignedValue=obj[a];
                $scope.case.assigned=obj["value"];
            }
            else if( a=="category")
            {
                $scope.case.typeValue=obj[a];
                $scope.case.type=obj["value"];
            } 
            else if( a=="priority")
            {
                $scope.case.priorityValue=obj[a];
                $scope.case.priority=obj["value"];
            }
            else if( a=="item")
            {
                $scope.case.itemValue=obj[a];
                $scope.case.item=obj["value"];
            }
            else if( a=="title")
            {
                $scope.case.title=obj[a];                                        
            }
            else if( a=="company")
            {
                $scope.case.companyValue=obj[a];
                $scope.case.company=obj["value"];
            }
            else if( a=="email")
            {
                $scope.case.email=obj[a];
            }    
        }
    }
    // to stop the loading indicator..
    $ionicLoading.hide();
            
    /*------------------------- code for the "date" is started here----------------------------------*/
    $ionicModal.fromTemplateUrl('datemodal.html', function(modal) 
    {
        $scope.datemodal = modal;
    },
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    $scope.$on('$destroy', function() 
    {
        $scope.datemodal.remove();
    });
    
    $scope.opendateModal = function() 
    {
        $scope.datemodal.show();
    };

	$scope.cancel = function()
	{
	    $scope.datemodal.hide();
	};

    $scope.closedateModal = function(model) 
	{
		if(model == undefined || model == "")
		{
			$scope.datemodal.hide();
		}
		else
		{
			$scope.datemodal.hide();
			$scope.date = model;
		  
		
			var myDate = new Date($scope.date);
			var month = myDate.getMonth()+1;
			$scope.no1=month;
			
			var day = myDate.getDate();
			$scope.no=day;
			var year = myDate.getFullYear();
			
			$scope.no2=year;
			$scope.case.startdate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
		}
    };
            
    /*------------------------- code for the "date" is ended here----------------------------------*/
    
    
    /*------------------------- code for the "company list" is started here----------------------------------*/
        
    // it retains data of the company list
    $scope.companyList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('company.html', function(companyWindow) 
    {
        $scope.companyWindow = companyWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when user clicks on the companylist dropdown
    $scope.openCompanyWindow = function() 
    {
        // to show the model window where list of company are there.
        $scope.companyWindow.show();   
        // to avoid duplication of data...
        $scope.companyList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching company list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
   
        // company list is inintialised as empty because it should not contain duplicated data.
        $scope.companyList= [];
        // parameters to send with the requrest to get the list of company ...
        var parameters =
        {
            "field_name":"company",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };

        // it send the request to get data of company..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {      
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named companylist
                 $scope.companyList.push(temp);

            }
            //to stop the loading indicator
            $ionicLoading.hide();             
        });
    };
                
    // this will be called when any of the company will be selected ..
    $scope.companySelected = function(companydata)
    {
        // it hides the companyWindow view
        $scope.companyWindow.hide();
        // it holds the value of company selected..
        $scope.case.companyValue = companydata.value;
      
        // it holdes the index of the selected company..
        $scope.case.company = companydata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeCompanyWindow = function() 
    {
        // it hides the companyWindow view
        $scope.companyWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.companyWindow.remove();
    });
                
    /* ---------------------------------code of "company list" is ended here ----------------------------------------------- */
            
    /*------------------------- code for the status list is started here----------------------------------*/
    
    // it retains data of the status list
    $scope.statusList = [];
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('status.html', function(statusWindow) 
    {

        $scope.statusWindow = statusWindow;
      
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

        $scope.searchKishor="";
     $scope.clearSearch = function() 
        {
            
            
            console.log(" clear search called:"+$scope.searchKishor);
            
        }

    // this function will be called when user clicks on the statuslist dropdown
    $scope.openStatusWindow = function() 
    {
         //$scope.searchKishor="";
       
        // to show the model window where list of status are there.
        $scope.statusWindow.show();
        // to avoid duplication of data...
        $scope.statusList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching status list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // parameters to send with the requrest to get the list of status ...
        var parameters =
        {
            "field_name":"status",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };

        // it send the request to get data of status..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {    
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named statuslist
                 $scope.statusList.push(temp);

            }
            // to stop the loading indicator
            $ionicLoading.hide();                 
        });
    };
            
    // this will be called when any of the status will be selected ..
    $scope.statusSelected = function(statusdata)
    {
        // it hides the statusWindow view
        $scope.statusWindow.hide();
        // it holds the value of status selected..
        $scope.case.statusValue = statusdata.value;
      
        // it holdes the index of the selected status..
        $scope.case.status = statusdata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeStatusWindow = function() 
    {
        // it hides the countryWindow view
        $scope.statusWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.statusWindow.remove();
    });
    
    /* ---------------------------------code of status list is ended here ----------------------------------------------- */
    
    /*------------------------- code for the "assigned to list" is started here----------------------------------*/
    
    // it retains data of the assigned to list
    $scope.assignedToList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('assignedTo.html', function(assignedToWindow) 
    {
        $scope.assignedToWindow = assignedToWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when user clicks on the assigned to list dropdown
    $scope.openAssignedToWindow = function() 
    {
        // to show the model window where list of assigned to are there.
        $scope.assignedToWindow.show();  
        // to avoid duplication of data...
        $scope.assignedToList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching assigned list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
   
        // assigned to list is inintialised as empty because it should not contain duplicated data.
        $scope.assignedToList= [];
        // parameters to send with the requrest to get the list of assigned to ...
        var parameters =
            {
                "field_name":"assigned",
                "field_type":"select",
                "field_subtext":"",
                "customer_type":"supportcase",
                "field_dependent":"",
                "field_dependentvalue":""
            };
        // it send the request to get data of assigned to..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        { 
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named assigned to list
                 $scope.assignedToList.push(temp);

            }
            // to stop the loading indicator
            $ionicLoading.hide();                 
        });
    };
            
    // this will be called when any of the assigned to will be selected ..
    $scope.assignedToSelected = function(assignedTodata)
    {
        // it hides the assigned to Window view
        $scope.assignedToWindow.hide();
        // it holds the value of assigned to selected..
        $scope.case.assignedValue = assignedTodata.value;
      
        // it holdes the index of the selected assigned to..
        $scope.case.assigned = assignedTodata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeAssignedToWindow = function() 
    {
        // it hides the assigned toWindow view
        $scope.assignedToWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.assignedToWindow.remove();
    });
            
    /* ---------------------------------code of "assigned to list" is ended here ----------------------------------------------- */
                        
            
    /*------------------------- code for the "type list" is started here----------------------------------*/
            
    // it retains data of the type list
    $scope.typeList = [];
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('type.html', function(typeWindow) 
    {
        $scope.typeWindow = typeWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    // this function will be called when user clicks on the typelist dropdown
    $scope.openTypeWindow = function() 
    {
        // to show the model window where list of countries are there.
        $scope.typeWindow.show(); 
        // to avoid duplication of data...
        $scope.typeList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching type list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
           
        // type list is inintialised as empty because it should not contain duplicated data.
        $scope.typeList= [];
        // parameters to send with the requrest to get the list of type ...
        var parameters =
        {
            "field_name":"category",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };
        // it send the request to get data of type..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {      
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named typelist
                 $scope.typeList.push(temp);
            }
            //to stop the loading indicator
            $ionicLoading.hide();  
        });
    };
            
    // this will be called when any of the type will be selected ..
    $scope.typeSelected = function(typedata)
    {
        // it hides the typeWindow view
        $scope.typeWindow.hide();
        // it holds the value of type selected..
        $scope.case.typeValue = typedata.value;
      
        // it holdes the index of the selected type..
        $scope.case.type = typedata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closetypeWindow = function() 
    {
        // it hides the typeWindow view
        $scope.typeWindow.hide();
    };
       //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.typeWindow.remove();
    });
    
    /* ---------------------------------code of "type list" is ended here ----------------------------------------------- */
                
    
    /*------------------------- code for the "priority list" is started here----------------------------------*/
    
    // it retains data of the priority list
    $scope.priorityList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('priority.html', function(priorityWindow) 
    {
        $scope.priorityWindow = priorityWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when user clicks on the prioritylist dropdown
    $scope.openPriorityWindow = function() 
    {
        // to show the model window where list of priority are there.
        $scope.priorityWindow.show(); 
        // to avoid duplication of data...
        $scope.priorityList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching priority list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
   
        // priority list is inintialised as empty because it should not contain duplicated data.
        $scope.priorityList= [];
        // parameters to send with the requrest to get the list of priority ...
        var parameters =
        {
            "field_name":"priority",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };
        
        // it send the request to get data of priority..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {       
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                var temp={ };
                temp.index= obj[0];
                temp.value= obj[1];
                // push every object into the array named prioritylist
                $scope.priorityList.push(temp);
            }
            //to stop the loading indicator
            $ionicLoading.hide();
                            
        });
    };
            
    // this will be called when any of the priority will be selected ..
    $scope.prioritySelected = function(prioritydata)
    {
        // it hides the priorityWindow view
        $scope.priorityWindow.hide();
        // it holds the value of priority selected..
        $scope.case.priorityValue = prioritydata.value;
      
        // it holdes the index of the selected priority..
        $scope.case.priority = prioritydata.index;
    }
            
    // this function will be called when cancel button will be clicked.
    $scope.closePriorityWindow = function() 
    {
        // it hides the priorityWindow view
        $scope.priorityWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.priorityWindow.remove();
    });
            
    /* ---------------------------------code of "priority list" is ended here ----------------------------------------------- */
                        
                        
    /*------------------------- code for the "item list" is started here----------------------------------*/
            
    // it retains data of the item list
    $scope.itemList = [];
     //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('item.html', function(itemWindow) 
    {
        $scope.itemWindow = itemWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    // this function will be called when user clicks on the itemlist dropdown
    $scope.openItemWindow = function() 
    {
        // to show the model window where list of item are there.
        $scope.itemWindow.show();
        // to avoid duplication of data...
        $scope.itemList = [];
        // it begins loading indicator 
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching item list...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // item list is inintialised as empty because it should not contain duplicated data.
        $scope.itemList= [];
        // parameters to send with the requrest to get the list of item ...
        var parameters =
        {
            "field_name":"item",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"supportcase",
            "field_dependent":"",
            "field_dependentvalue":""
        };
        // it send the request to get data of item..
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
        {   
            for(mainArray in  results)
            {
                obj =  results[mainArray];
                 var temp={ };
                 temp.index= obj[0];
                 temp.value= obj[1];
                 // push every object into the array named itemlist
                 $scope.itemList.push(temp);
            }
            //to stop the loading indicator
            $ionicLoading.hide();                   
        });

    };
            
    // this will be called when any of the item will be selected ..
    $scope.itemSelected = function(itemdata)
    {
        // it hides the itemWindow view
        $scope.itemWindow.hide();
        // it holds the value of item selected..
        $scope.case.itemValue = itemdata.value;
      
        // it holdes the index of the selected item..
        $scope.case.item = itemdata.index;
    }
    
    // this function will be called when cancel button will be clicked.
    $scope.closeItemWindow = function() 
    {
        // it hides the itemWindow view
        $scope.itemWindow.hide();
    };
    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.itemWindow.remove();
    });
            
    /* ---------------------------------code of "item list" is ended here ----------------------------------------------- */
                
    $scope.showConfirm = function() 
    {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.caseDetail');
            } 
            else 
            {
             console.log('You are not sure');
            }
        });
    };
    
    // cancel button to go to the case details page.
    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    //  save button to save the filled data on the form ..
    $scope.saveData = function() 
    {

        // Validations for mandatory fields on forms
        if($scope.case.title == undefined || $scope.case.title == "")
        {
            $scope.showAlert("Please fill Subject field");
            return;
        }
        else if($scope.case.companyValue == undefined || $scope.case.companyValue == "")
        {
            $scope.showAlert("Please fill Company field");
            return;
        }
        else if($scope.case.startdate == undefined || $scope.case.startdate == "")
        {
            $scope.showAlert("Please fill Incident Date field");
            return;
        }
        else if($scope.case.statusValue == undefined || $scope.case.statusValue == "")
        {
            $scope.showAlert("Please fill Status field");
            return;
        }


        // to begin the loading indicator ..
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Updating...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        var addressId = AppFactory.getAddressContactId();
            
        // to set the parameters to send along with the request..
        var parameters=
        {
            "categoryvalue": $scope.case.typeValue,
            "statusvalue": $scope.case.statusValue,
            "status": $scope.case.status,
            "assigned": $scope.case.assigned,
            "assignedvalue": $scope.case.assignedValue,
            "itemvalue": $scope.case.itemValue,
            "category": $scope.case.type,
            "title": $scope.case.title,
            "startdate": $scope.case.startdate,
            "casenumber": "10",
            "email": $scope.case.email,
            "priority": $scope.case.priority,
            "companyvalue": $scope.case.companyValue,
            "item": $scope.case.item,
            "company": $scope.case.company,
            "internalid": caseId,
            "priorityvalue": $scope.case.priorityValue
        } 

		// send the request to save the data.. 
		AppFactory.sendHttpRequest("put","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=101&deploy=1",parameters,null,function(results)  
		{ 
			if(results == "\"Case record edited successfully.\"")
            {    
                // to stop the loading indicator..
                $ionicLoading.hide();
                 window.plugins.toast.show("Case record edited successfully","short","center");
                // redirects on the case detail page..
                $state.go('app.caseDetail');
            }
                    else
                    {
                        $scope.showAlert(results);
                        $ionicLoading.hide();
                        return;
                    }
        	   });   
    }        
}); 
/*--------------------------------------------------- Edit case controller ends here--------------------------------------*/        


/*----------------------------------------Message List Controller Start here Shivaji--------------------------------------*/    
app.controller('MessageListController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout) 
{

       /*Clear Search Box*/
     $scope.clearSearch = function () {
        $scope.searchString="";
    } 
    
    var caseId = AppFactory.getCaseId();
    // to disable sidebar when page loads
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }

    // to set the title to the case 
    $scope.myTitle = "MESSAGE LIST";
    // to store the index of the objects 
    $scope.keyArray = [];
    // to store the objects where case data is present
    $scope.MessageList = [];
    // function will call on clicking refresh button..
    $scope.init = function()
    {
        // to store the objects where case data is present
        $scope.MessageList = [];
        // loading indicator starts here...
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        // parameters to send with the requrest of getting data of case list
        var parameters =
        { 
            "id":"customsearch_aavz_33",
            "noofrecords":"100",
            "internalid":caseId,
            "customer_type":"supportcase" 
        };
        

        // sending the request to the server to get data 
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=119&deploy=1",null,parameters,function(results)
        { 
            
            for(mainArray in  results)
            {
                obj =  results[mainArray];

                for(objKey in obj)
                {
                    var objValue = obj[objKey];
                    objValue.id=objKey;
                     // pushing the case data into the valuearray
                    $scope.MessageList.push(objValue);
                     // pushing the indexes of objects into the keyarray
                    $scope.keyArray.push(objKey);
                }
            }
            // to hide the loading indicator 
            $ionicLoading.hide();
        });
    }   
	// to get the data first time...
	$scope.init();

	// to goto top of list ..
	$scope.gotoTop = function ()
	{
	   // set the location.hash to the id of
	   // the element you wish to scroll to.
	   $location.hash('top'); 
	   // call $anchorScroll()
	   $anchorScroll();
	};

	$scope.goBack = function()
	{
		$state.go('app.caseDetail');
	}
   
	$scope.goToAdd = function()
	{
		$state.go('app.messageAdd');
	}
	   
    // this fuction will be called when any list item of case will be clicked
	$scope.goto = function(id)
	{
	   //alert(id);
	   // it sets the message contact id 
        AppFactory.setMessageId(id);
		// it redirects to the message details page
		$state.go('app.messageDetail');
	}
	
});
/*-----------------------------------------------Message List Controller end---------------------------------------*/

/*-----------------------------------------------Message Detail Controller---------------------------------------*/
app.controller('MessageDetailController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout) 
{
    $scope.showPage = false;
    // to get the id of the message which was clicked..
    var id = AppFactory.getMessageId();
    //console.log("ID:"+id);
    // to store the keys of the objects 
    $scope.keyArray = [];
    $scope.headerArray=[];
    $scope.primaryInfoArray=[];

    $scope.messageData={};

    // to disable sidebar when page loads
    $rootScope.enableSideBar = true;
    if ($rootScope.hideSideBar == false) 
    {
        $scope.sideMenuController.toggleLeft();
        $rootScope.hideSideBar = true;
    }
    
    // to set the title to the case 
    $scope.pageTitle = "MESSAGE DETAIL";
    
    // to begin the loading indicator ..
    $ionicLoading.show({
        content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showdelay: 300
    });


    // parameters to set along with the request..
    var parameters =
    { 
        "id":"customsearch_aavz_31",
        "noofrecords":"100",
        "internalid":id,
        "display":"1,4,3,5,2",
        "groupname":"Header,Primary Information,Primary Information,Primary Information,Header",
        "fieldname":"author,message,messagedate,internalid,subject",
        "customer_type":"message"
    };
          
         
    //Request for get data of the message details
    AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=103&deploy=1",null,parameters,function(results)
    { 
        //console.log("Response Data:"+JSON.stringify(results));
    
        var mainArray=results;
    
        for(item in mainArray)
        {
            var mainObj = mainArray[item]; //get single object of array
            //console.log(JSON.stringify(mainObj));        
            for(innerObj in mainObj)
            {
                var internalId=innerObj; //hold key 353
                var keyObj=mainObj[innerObj]; //hold entire object       
                for(key in keyObj)
                {
                    $scope.keyArray.push(key); //hold key 0 to 9
                
                    // section wise distribution
                    var sectionHeader=keyObj[key].section_header; //get the section header
                    var priority=keyObj[key].priority;
                    //console.log("Priority:"+JSON.stringify(priority));
                    if(sectionHeader=="Header")
                    {
                        $scope.headerArray.push(keyObj[key]); //hold array of objects having section header is Header
                    }    
                    if(sectionHeader=="Primary Information")
                    {
                        $scope.primaryInfoArray.push(keyObj[key]); //hold array of objects having section header is Header
                    }
                }//end of 3rd for   
            }//end of 2nd for 
        }//end of 1st for

        //Sorting PrimaryInfoArray
        $scope.primaryInfoArray.sort(function(a, b)
        {
            return a.priority - b.priority;
        });


        //Sorting PrimaryInfoArray
        $scope.headerArray.sort(function(a, b)
        {
            return a.priority - b.priority;
        });

        $scope.headerData=[];
        //Code for display dynamic column values of labels    
        for(i=0;i<$scope.headerArray.length;i++)
        {
            var obj=$scope.headerArray[i];
            for(key in  obj)
            {
                if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                {
                    var object={}; //create new object and store into an array
                    object.label=$scope.headerArray[i].label;
                    object.value=obj[key];
                    object.index=$scope.headerArray[i].value;
                    $scope.headerData.push(object);
                    //console.log("Column="+JSON.stringify(object.value));  
                }
            }
        }
        //console.log("Header="+JSON.stringify($scope.headerData));



        $scope.primaryInfoData=[];
        //Code for display dynamic column values of labels    
        for(i=0;i<$scope.primaryInfoArray.length;i++)
        {
            var obj=$scope.primaryInfoArray[i];
            for(key in  obj)
            {
                if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                {
                    var object={}; //create new object and store into an array
                    object.label=$scope.primaryInfoArray[i].label;
                    object.value=obj[key];
                    $scope.primaryInfoData.push(object);
                    //console.log("Column="+JSON.stringify(object.value));  
                }
            }
        }
        //console.log("Primary Info="+JSON.stringify($scope.primaryInfoData));

        //SET data used for Edit Message time
        $scope.messageData.header=$scope.headerData;
        $scope.messageData.primary=$scope.primaryInfoData;
        $scope.showPage = true;
        $ionicLoading.hide();
    }); //end of sendHttpRequest


    $scope.goBack = function()
    {
        $state.go('app.messageList');
    }
           
    $scope.goToEdit = function()
    {
        AppFactory.setMessageEditData($scope.messageData);
        $state.go('app.messageEdit');
    }

    $scope.goto=function()
    {
        //$state.go('app.messageList');
    }
});     
/*------------------------------------------------Message Details Controller End-----------------------------------------------------*/

/*-----------------------------------------------Message Add Controller start---------------------------------------*/

app.controller('MessageAddController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout,$ionicModal,$ionicPopup) 
{

    $scope.message = {};
    //fetch case id as internal id
    var internalid=AppFactory.getCaseId();
    $scope.message.internalid=internalid;
    //console.log("ID:"+internalid);

    // to disable the side bar
    $rootScope.enableSideBar = false;
    // to set the title to the case 
    $scope.myTitle = "ADD MESSAGE";
     // it is empty object where data will be kept which is to add .
   
    $scope.showConfirm = function()
    {
        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.messageList');
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };

    $scope.goTo = function()
    {
        $scope.showConfirm();
    }


    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };
	
	/*------------------------- code for the "date" is started here----------------------------------*/
    $ionicModal.fromTemplateUrl('datemodal.html', function(modal) 
    {
        $scope.datemodal = modal;
    },
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.$on('$destroy', function() 
    {
        $scope.datemodal.remove();
    });

    $scope.opendateModal = function() 
    {
        $scope.datemodal.show();
    };

    $scope.cancel = function()
    {
        $scope.datemodal.hide();
    };

    $scope.closedateModal = function(model) 
    {
        if(model == undefined || model == "")
        {
            $scope.showAlert("please fill Date field");
        }
        else
        {
            $scope.datemodal.hide();
            $scope.date = model;
      

            var myDate = new Date($scope.date);
            var month = myDate.getMonth()+1;
            $scope.no1=month;

            var day = myDate.getDate();
            $scope.no=day;
            var year = myDate.getFullYear();

            $scope.no2=year;
            $scope.message.messagedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
        }
    };
			
	/*------------------------- code for the "date" is ended here----------------------------------*/
    $scope.saveData = function ()
    {
        if($scope.message.subject == undefined || $scope.message.subject == "")
        {
            $scope.showAlert("Please fill Subject field");
            return;
        }

        // to begin the loading indicator ..
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        console.log(JSON.stringify($scope.message));
        //Send http request for save new message
            
        AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=103&deploy=1",$scope.message,null,function(results) 
        {   //get result from json
            console.log("Response Data:"+JSON.stringify(results));
            //To navigate the OPPORTUNITY list page  
            if(results == "\"Message saved successfully.\"")
            {
                window.plugins.toast.show("Message saved successfully","short","center");                                           
                $state.go('app.messageList');
                $ionicLoading.hide();
            }
            else
            {
                $scope.showAlert(results);
                $ionicLoading.hide();
                return;
            }
        });//end of sendHttpRequest
    }


	//code for display author list
	//Load the modal from the given template URL
	$ionicModal.fromTemplateUrl('Author.html', function(modal1) 
    {
		$scope.modal1 = modal1;
	},
    {
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope,
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'
	});

	$scope.AuthorList=[];

	$scope.fetchAuthorList = function() 
    {
        $scope.modal1.show();
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
		//call webservice for display drop-down list of forecasttype
        $scope.AuthorList=[];
        var parameters = {
            "field_name":"author",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"message",
            "field_dependent":"",
            "field_dependentvalue":""
        };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) 
        { //get result from json
            //console.log("Response Data:"+JSON.stringify(results));
            var mainArray=results;
            for(obj in mainArray)
            {
                var objectArray=mainArray[obj];          
                //console.log(JSON.stringify(objectArray));
                var object={};
                object.index=objectArray[0];
                object.value=objectArray[1];
                $scope.AuthorList.push(object);          
            }
            // stop loading indicator
             $ionicLoading.hide();
        }); //end of sendHttpRequest
    };

	$scope.closeAuthorWindow = function() 
    {
		$scope.modal1.hide();
	};

	//Be sure to cleanup the modal
	$scope.$on('$destroy', function() 
    {
		$scope.modal1.remove();
	});


	$scope.setAuthorName=function(typedata)
	{
		$scope.message.authorvalue=typedata.value;
		$scope.message.author=typedata.index;
		$scope.modal1.hide();
	}
});
/*-----------------------------------------------Message Add Controller End---------------------------------------*/

/*-----------------------------------------------Message Edit Controller start---------------------------------------*/

app.controller('MessageEditController', function($scope, $rootScope, $http, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $location, $anchorScroll, $timeout,$ionicModal,$ionicPopup) 
{
    $scope.message = {};
    //fetch case id as internal id
    var internalid=AppFactory.getCaseId();
    $scope.message.internalid=internalid;
    //console.log("ID:"+internalid);

    // to disable the side bar
    $rootScope.enableSideBar = false;
    // to set the title to the case 
    $scope.myTitle = "EDIT MESSAGE";
     // it is empty object where data will be kept which is to add .
   
    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.messageDetail');
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };


    // cancel button to go to the company list page.
    $scope.goTo = function()
    {
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    $scope.saveData = function(){

        if($scope.message.subject == undefined || $scope.message.subject == "")
        {
            $scope.showAlert("Please fill Subject field");
            return;
        }

        // to begin the loading indicator ..
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
               
        //console.log("Final Obj:"+JSON.stringify($scope.message));
        //Send http request for save new message 
             
            
		AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=103&deploy=1",$scope.message,null,function(results) 
		{   //get result from json
			  
			if(results == "\"Message edited successfully.\"")
            {
                window.plugins.toast.show("Message edited successfully","short","center");
              //  $scope.showAlert("Message edited successfully");
                $state.go('app.messageList');
                $ionicLoading.hide();
                
            }
            else
            {   
                $scope.showAlert(results);
                $ionicLoading.hide();
                return;
           }   
			
		});//end of sendHttpRequest
    }
 
    /*------------------------- code for the "date" is started here----------------------------------*/
    $ionicModal.fromTemplateUrl('datemodal.html', function(modal) 
    {
        $scope.datemodal = modal;
    },
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope, 
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
    
    $scope.$on('$destroy', function() 
    {
        $scope.datemodal.remove();
    });
    
    $scope.opendateModal = function() 
    {
        $scope.datemodal.show();
    };

	$scope.cancel = function()
	{
		$scope.datemodal.hide();
	};

    $scope.closedateModal = function(model)
	{
		if(model == undefined || model == "")
		{
			$scope.datemodal.hide();
		}
		else
		{
			$scope.datemodal.hide();
			$scope.date = model;
			  
			
			var myDate = new Date($scope.date);
			var month = myDate.getMonth()+1;
			$scope.no1=month;
			
			var day = myDate.getDate();
			$scope.no=day;
			var year = myDate.getFullYear();
			
			$scope.no2=year;
			$scope.message.messagedate=$scope.no1 + "/" + $scope.no + "/" +$scope.no2;
		}	
    };
  
    /*------------------------- code for the "date" is ended here----------------------------------*/


	//code for display author list
	//Load the modal from the given template URL
	$ionicModal.fromTemplateUrl('Author.html', function(modal1) 
    {
		$scope.modal1 = modal1;
	}, 
    {
		// Use our scope for the scope of the modal to keep it simple
		scope: $scope,
		// The animation we want to use for the modal entrance
		animation: 'slide-in-up'
	});

	$scope.AuthorList=[];
	$scope.fetchAuthorList = function() 
    {
        $scope.modal1.show();
        // loading indicator starts...
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
		//call webservice for display drop-down list of forecasttype
        $scope.AuthorList=[];
        var parameters = 
        {
            "field_name":"author",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"message",
            "field_dependent":"",
            "field_dependentvalue":""
        };         
           
        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results) 
        { 
            //get result from json
            //console.log("Response Data:"+JSON.stringify(results));

            var mainArray=results;
            for(obj in mainArray)
            {
                var objectArray=mainArray[obj];          
                //console.log(JSON.stringify(objectArray));
                var object={};
                object.index=objectArray[0];
                object.value=objectArray[1];
                $scope.AuthorList.push(object);          
            }
            // stop loading indicator..
            $ionicLoading.hide();
      }); //end of sendHttpRequest
	};

    $scope.closeAuthorWindow = function() 
    {
        $scope.modal1.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.modal1.remove();
    });


    $scope.setAuthorName = function(typedata)
    {
        $scope.message.authorvalue=typedata.value;
        $scope.message.author=typedata.index;
        $scope.modal1.hide();
    }


	//get Existing data
	$scope.messageData=AppFactory.getMessageEditData();
	//console.log("Data:"+JSON.stringify($scope.messageData));

	$scope.headerArray=$scope.messageData.header;
	$scope.primaryInfoArray=$scope.messageData.primary;
		  

	//console.log("obj:"+JSON.stringify($scope.headerArray));
  
    for(obj in $scope.headerArray)
    {
	    var data=$scope.headerArray[obj];
	    for(key in data)
	    {
            // console.log(JSON.stringify(data[key]));
            if(data[key]=="Author")
            {
                $scope.message.authorvalue=data.value;
                $scope.message.author=data.index;       
            }

            if(data[key]=="Subject")
            {
                $scope.message.subject=data.value;         
            }

	    }
    }



	for(obj in $scope.primaryInfoArray)
	{
		var data=$scope.primaryInfoArray[obj];
		for(key in data)
		{
			// console.log(JSON.stringify(data[key]));
			if(data[key]=="Date")
			{
			    $scope.message.messagedate=data.value;
			    //console.log("Title:"+data.value);
			}
			else if(data[key]=="Message")
			{
			    $scope.message.message=data.value;         
			}
		}
	}
});
/*-----------------------------------------------Message Edit Controller End---------------------------------------*/


