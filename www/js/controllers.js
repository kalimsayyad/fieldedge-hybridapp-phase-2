var app = angular.module('FieldEdge.controllers', ['pickadate']);


/*----------------------------------------------------controller for sidemenu--------------------------------------------------------*/
app.controller('AppCtrl', function($scope, AppFactory, $ionicPopup, $state, $timeout, $rootScope) 
{
	console.log("User:"+AppFactory.getUsername());
    console.log("Role:"+AppFactory.getUserRole());

    /*----------variables---------------*/
    $scope.userRole = AppFactory.getUserRole();     //to store userRole
	$scope.username = AppFactory.getUsername();     //to store userName
    $rootScope.sideMenuEnabled = true;               //to enable side menu
    /*----------functions---------------*/
    $scope.logout = function ()
    {
        $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Are you sure, you want to log-out?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $rootScope.showRelationship = false;
                                $rootScope.addRelationship = false;
                                $rootScope.editRelationship = false;
                            $rootScope.showOpportunity = false;
                                $rootScope.addOpportunity = false;
                                $rootScope.editOpportunity = false;
                            $rootScope.showQuote = false;
                                $rootScope.addQuote = false;
                                $rootScope.editQuote = false;
                            $rootScope.showOrder = false;
                                $rootScope.addOrder = false;
                                $rootScope.editOrder = false;
                            $rootScope.showCase = false;
                                $rootScope.addCase = false;
                                $rootScope.editCase = false;
                            $rootScope.showExpense = false;
                                $rootScope.addExpense = false;
                                $rootScope.editExpense = false;
                            $rootScope.showTimesheet = false;
                                $rootScope.addTimesheet = false;
                                $rootScope.editTimesheet = false;
                            $rootScope.showCall = false;

                            AppFactory.emptyRoles();
                            
                            $timeout(function () 
                            {
                                $state.go('login');
                            }, 1000);                           
                        } 
                        else 
                        {
                            console.log('You are not sure');
                        }
                    });
    }	
});
/*----------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------Login Controller-----------------------------------------------------------*/

// A simple controller that fetches a list of data from a service
app.controller('LoginController', function($scope, $state, AppFactory, $ionicLoading, $ionicPopup, $rootScope) 
{
    /*----------variables---------------*/
    $scope.loginCredential={};      //object to store login credential

	console.log("logincontroller appearing");

	$scope.loginCredential.username = "developer01@proquestsolutions.com";
    $scope.loginCredential.password = "1234Demo!";
    /*$scope.loginCredential.username = "salesp@veloziti.com";
	$scope.loginCredential.password = "Netsuite123!";*/
    $scope.loginCredential.accountno = "TSTDRV919831";


    /*----------functions---------------*/
    //function called when login button is clicked
  	$scope.checkLoginCredential = function()
	{
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Authenticating...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        //funcrtion called to validate the user
        AppFactory.isValidUser($scope.loginCredential.username, $scope.loginCredential.password,$scope.loginCredential.accountno,function(result)
        {
            //if user is valid result is "true" or else result contains error
            if (result == "true")
            {
                $ionicLoading.hide();

                $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching Roles...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                });
                //AppFactory.setUserRole($scope.loginCredential.role.name);

                AppFactory.sendHttpRequest("GET","https://rest.netsuite.com/rest/roles",null,null,function(responseData)  
                { 
                    console.log("responseData:"+JSON.stringify(responseData));
                    var rawData = responseData;
                    for(item in rawData)
                    {
                        var innerData = rawData[item];
                        for(object in innerData)
                        {
                            var objectData = innerData[object];
                            console.log("object:"+object+" objectData:"+JSON.stringify(objectData));
                            if(object == "role")
                            {
                                console.log("setting values");
                                AppFactory.setUserRole(objectData);
                                for(key in objectData)
                                {
                                    console.log("key:"+objectData[key]);
                                    if(objectData[key] == "Administrator")
                                    {
                                        $ionicLoading.hide();

                                        $ionicLoading.show({
                                            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Setting Permissions...",
                                            animation: 'fade-in',
                                            showBackdrop: true,
                                            maxWidth: 200,
                                            showdelay: 300
                                        });

                                        var parameters =
                                        {
                                            "employeeid":$scope.loginCredential.username,
                                            "roleid":objectData.internalId
                                        };

                                        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=customscript_aavz_52&deploy=customdeploy1",null,parameters,function(responseData)  
                                        { 
                                            console.log("responseData:"+JSON.stringify(responseData));
                                            var rawData = JSON.parse(JSON.parse(responseData));
                                            for(item in rawData)
                                            {
                                                var innerData = rawData[item];
                                                //console.log("item in rawData:"+JSON.stringify(statusData[i]));
                                                for(array in innerData)
                                                {
                                                    var arrayItem = innerData[array];
                                                    //console.log("arrayItem:"+JSON.stringify(arrayItem));
                                                    //console.log("item:"+arrayItem[0]+" value:"+arrayItem[2]);
                                                    if(arrayItem[0] == "LIST_CUSTJOB")
                                                    {
                                                        console.log("LIST_CUSTJOB found with "+arrayItem[2]+" permission")
                                                        $rootScope.showRelationship = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editRelationship = true;
                                                            $rootScope.addRelationship = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addRelationship = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "TRAN_OPPRTNTY")
                                                    {
                                                        console.log("TRAN_OPPRTNTY found with "+arrayItem[2]+" permission");
                                                        $rootScope.showOpportunity = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editOpportunity = true;
                                                            $rootScope.addOpportunity = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addOpportunity = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "TRAN_ESTIMATE")
                                                    {
                                                        console.log("TRAN_ESTIMATE found with "+arrayItem[2]+" permission")
                                                        $rootScope.showQuote = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editQuote = true;
                                                            $rootScope.addQuote = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addQuote = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "TRAN_SALESORD")
                                                    {
                                                        console.log("TRAN_SALESORD found with "+arrayItem[2]+" permission")
                                                        $rootScope.showOrder = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editOrder = true;
                                                            $rootScope.addOrder = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addOrder = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "LIST_CASE")
                                                    {
                                                        console.log("LIST_CASE found with "+arrayItem[2]+" permission")
                                                        $rootScope.showCase = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editCase = true;
                                                            $rootScope.addCase = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addCase = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "TRAN_EXPREPT")
                                                    {
                                                        console.log("TRAN_EXPREPT found with "+arrayItem[2]+" permission")
                                                        $rootScope.showExpense = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editExpense = true;
                                                            $rootScope.addExpense = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addExpense = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "TRAN_TIMEBILL")
                                                    {
                                                        console.log("TRAN_TIMEBILL found with "+arrayItem[2]+" permission")
                                                        $rootScope.showTimesheet = true;
                                                        if(arrayItem[2] == 4 || arrayItem[2] == 3)
                                                        {
                                                            $rootScope.editTimesheet = true;
                                                            $rootScope.addTimesheet = true;
                                                        }
                                                        else if(arrayItem[2] == 2)
                                                        {
                                                            $rootScope.addTimesheet = true;
                                                        }
                                                    }
                                                    if(arrayItem[0] == "LIST_CALL")
                                                    {
                                                        console.log("LIST_CALL found with "+arrayItem[2]+" permission")
                                                        $rootScope.showCall = true;
                                                    }
                                                }
                                            }
                                            $ionicLoading.hide();
                                            $state.go('app.homepage');
                                        });
                                    }
                                    
                                }
                            }
                        }
                    }
                });

                /*$().toastmessage({
                sticky   : false,
                type     : 'success',
                inEffectDuration:  1000,   // in effect duration in miliseconds
                stayTime:         2000
                 });

                $().toastmessage('showSuccessToast', "Login successfull");*/ 
                //$state.go('app.homepage'); 
            }
            else
            {   //parsing of error
                var allData = result;
                for(singleData in allData)
                {
                    var innerData = allData[singleData];
                    for(singleRecord in innerData)
                    {
                        if(singleRecord == "message")
                        {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Alert',
                                content: innerData[singleRecord]
                                }).then(function(res) {
                                    
                                });
                                $ionicLoading.hide();
                                return;
                            //alert(innerData[singleRecord]);
                        }
                    }
                }

                console.log("result:"+result);
                $ionicLoading.hide();
                $ionicPopup.alert({
                                title: 'Alert',
                                content: 'Login failed, Please try again.'
                                }).then(function(res) {
                                    
                                });
                                return;
            }

        });//end of isValidUser()
	}//end of loginCheck()
});
/*-------------------------------------------------------Login Controller end-----------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------------------------------*/
app.controller('RolesController', function($scope, $state, AppFactory, $ionicLoading, $ionicPopup, $rootScope)
{
    $scope.rolesList = AppFactory.getUserRole();
});
/*--------------------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------Home Page Controller-------------------------------------------------------------
                                    this controller for home page displayed after login*/

app.controller('HomePageController', function($scope, $state, AppFactory,$ionicPopup, $rootScope) 
{
    console.log("homepage controller appearing");

    /*----------variables---------------*/
    $scope.userRole = AppFactory.getUserRole();     //to store userRole
    $rootScope.sideMenuEnabled = true;

    /*----------functions---------------*/
    $scope.logout = function ()
    {

        console.log("Logout :");
    }

	$scope.TopCases = function ()
	{
        $state.go('app.caseList');
		AppFactory.setType('TopCases');
    }
    // this function gets called when we click on the home button of the side menu bar
    $scope.goHome = function ()
    {
        $state.go('app.homePage');
    }
	
    //redirect to Last5TimesheetController
    $scope.goNext=function()
    {
        $state.go('app.Last5Timesheet');
    }
});
/*-----------------------------------------------------Home Page Controller end---------------------------------------------------------*/

/*-------------------------------------------------Customer Page Controller-----------------------------------------------------------
                                this is the common controller for lead/prospect/customer page*/

app.controller('CustomerPageController', function($scope, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $filter, $ionicPopup, $ionicScrollDelegate, $rootScope) 
{
    console.log("customer controller appearing");
    
    /*----------variables---------------*/
    $scope.orderString = "";                        //to store sorting value(name/status)
    $scope.newflag = "false";                       //flag to display message
    $scope.searchString = "";                       //to store search string
    $scope.listflag = 1;                            //flag to check whether searched string initially present in customer list
    $scope.customerList = [];                       //to store list of customers
    $scope.counter = $scope.customerList.length;    //to store length of customerList    
    $scope.customerList = [];                       //array to store list of customer
    $scope.pageTitle = "";                          //to store page title

    $scope.clearSearch = function () 
    {
        
        $scope.searchString="";
        cordova.plugins.Keyboard.close();
    }

    if($stateParams.category == "TOP_CUSTOMER")
    {
        $scope.pageTitle = "TOP 5 CUSTOMERS";
        $rootScope.sideMenuEnabled = false;

    }
    else
    {
        $scope.pageTitle = $stateParams.category + " LIST";    //to title of the page(lead/propect/customer)    
        $rootScope.sideMenuEnabled = true;
    }
    

    /*----------functions---------------*/

    //SHIVAJI
    /**************SEARCH FROM SERVER START******************************************/
    
    //used in server-search
    $scope.arrayLength=0;
    $scope.recordCount=0;
    $scope.messageFlag="false";
    //Code for Search data filter from server
    $scope.$watch("searchString", function(query)
    {
        $scope.counter = $filter("filter")($scope.customerList, query).length;
        console.log("Count : "+$scope.counter);
       
        if($scope.listflag == 1) 
        {
            $scope.listflag = 0;  
            $scope.newflag = "false";                  
        }
        else if($scope.counter == 0 && $scope.listflag == 0 && $scope.pageTitle != "TOP 5 CUSTOMERS")
        {    
            $scope.message="No results found.Continue search on server.";  
            $scope.newflag="true";
            $scope.messageFlag="false";             
        } 
        else if($scope.counter == 0 && $scope.listflag == 0 && $scope.pageTitle == "TOP 5 CUSTOMERS")
        {    
            $scope.message="No results found.";  
            $scope.newflag="true";             
        }
        else
        {
            $scope.newflag="false";
            $scope.messageFlag="false";
            $ionicScrollDelegate.scrollTop();
        }

        if($scope.searchString.length == 0)
        {
            console.log("Count : "+$scope.counter+" search string:"+$scope.searchString);
            $scope.counter = 0;
        }

        if($scope.recordCount>0)
        {
            //console.log("Array length:"+$scope.arrayLength); 
            //console.log("New Record Found:"+$scope.recordCount);
            //to remove newly added element from an array       
            $scope.customerList.splice($scope.arrayLength,$scope.recordCount);
        }
    
    });//end of $watch()


    //Calling web services for server search

    $scope.mySearchList = function()
    {
        
        if($scope.pageTitle == "LEAD LIST")
        {   
            $scope.recordCount=0;
            //display loading indicator
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

            var parameters =
            {
               "id":"customsearch_aavz_16",
               "stage":"LEAD",
               "customer_type":"customer",                
               "search":$scope.searchString,    
               "searchkey":"Name"               
            };

            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=105&deploy=1",null,parameters,function(results) 
            { 
                //get result from json
                console.log("Response Data:"+JSON.stringify(results));
                if(results.length == 0 )
                {
                    $scope.message = "No results found on server.";
                    $scope.newflag="false";
                    $scope.messageFlag="true";
                }
                else
                {
                    $scope.newflag = "false";
                    $scope.messageFlag="false"; 
                    $scope.counter = results.length;
                }

                var mainArray = results;
                for(i in mainArray)
                {
                    var mainObj = mainArray[i];                     //get single object of array
                    for(key in mainObj)
                    {  
                        $scope.recordCount+=1;
                        var singleObject = mainObj[key];            //create a single object by using key
                        singleObject.id = key;                      //Add key as attribute in singleObj
                        $scope.customerList.push(singleObject);     //insert data into array of objects
                    } //end of key forloop
                }//end of mainArray                           
                $ionicLoading.hide();
            });//end of sendHttpRequest()
        }           

        else if($scope.pageTitle == "PROSPECT LIST")
        {   
            $scope.recordCount=0;
            //display loading indicator
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

            var parameters = 
            {
               "id":"customsearch_aavz_16",
               "stage":"PROSPECT",
               "customer_type":"customer",                
               "search":$scope.searchString,    
               "searchkey":"Name"               
            };

            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=105&deploy=1",null,parameters,function(results) 
            {
                //get result from json
                console.log("Response Data:"+JSON.stringify(results));
                if(results.length == 0)
                {
                    $scope.message = "No results found on server.";
                    $scope.newflag="false";
                    $scope.messageFlag="true";   
                }
                else
                {
                    $scope.newflag = "false";
                    $scope.messageFlag="false"; 
                    $scope.counter = results.length;
                }

                var mainArray = results;
                for(i in mainArray)
                {
                    var mainObj = mainArray[i]; //get single object of array
                    for(key in mainObj)
                    {  
                        $scope.recordCount+=1;
                        var singleObject = mainObj[key];              //create a single object by using key
                        singleObject.id = key;                        //Add key as attribute in singleObj
                        $scope.customerList.push(singleObject);       //insert data into array of objects
                    } //end of key forloop
                }//end of mainArray                           
                $ionicLoading.hide();
            });//end of sendHttpRequest()
        }           

        else if($scope.pageTitle == "CUSTOMER LIST")
        {   
            $scope.recordCount=0;
            //display loading indicator
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

            var parameters = 
            {
               "id":"customsearch_aavz_16",
               "stage":"CUSTOMER",
               "customer_type":"customer",                
               "search":$scope.searchString,    
               "searchkey":"Name"               
            };

            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=105&deploy=1",null,parameters,function(results) 
            { 
                //get result from json
                console.log("Response Data:"+JSON.stringify(results));
                if(results.length == 0)
                {
                    $scope.message = "No results found on server.";
                    $scope.newflag="false";
                    $scope.messageFlag="true";
                }
                else
                {
                    $scope.newflag = "false";
                    $scope.messageFlag="false";  
                    $scope.counter = results.length;
                }

                var mainArray = results;
                for(i in mainArray)
                {
                    var mainObj = mainArray[i]; //get single object of array
                    for(key in mainObj)
                    {  
                        $scope.recordCount+=1;
                        var singleObject = mainObj[key];            //create a single object by using key
                        singleObject.id = key;                      //Add key as attribute in singleObj
                        $scope.customerList.push(singleObject);     //insert data into array of objects
                    } //end of key forloop
                }//end of mainArray                           
                $ionicLoading.hide();
            });//end of sendHttpRequest()
        }
    }//end of mySearchList()
    /**************SEARCH FROM SERVER END******************************************/
    $scope.allData = [];
    $scope.showMessage = false;
    //function called when page loads
    $scope.init = function()
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        $scope.showMessage1 = false;
        $scope.customerList = [];
        $scope.orderString = "";

        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 300
        });

        //$stateParams is used to retrieve value from url
        AppFactory.setCategory($stateParams.category);     //set category(lead/propect/customer)

        //function in factory to get customer list
        AppFactory.getCustomerList("0",$scope.orderString,function(result)
        {
            console.log("result:"+JSON.stringify(result));
            $scope.allData = result;

            if($scope.allData.length == 0)
            {
                $scope.showMessage1 = true;
            }

            //parsing of the result
            for(singleData in $scope.allData)
            {
                //checking whether result has error
                if(singleData == "error")
                {   //if error occurs
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        content: "<i class='icon ion-alert-circled' style='font-size: 40px;'></i><br>Loading Failed",
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showdelay: 300
                    });

                    $timeout(function(){
                        $ionicLoading.hide();
                    },1000);
                }
                else
                {   //parsing and storing data
                    var innerData = $scope.allData[singleData];

                    for(singleRecord in innerData)
                    {
                      innerData[singleRecord].id = singleRecord;            //adding internal id of customer in innerData object
                      $scope.customerList.push(innerData[singleRecord]);    //adding innerData object to customerList array
                    }
                }
            }
            $scope.arrayLength=$scope.customerList.length;//get customerList array length used for server-search
            $ionicLoading.hide();
        });
    }
    $scope.init();

    $scope.flag = 1;    //flag to avoid repeatative call to same restlet

    //fucntion to load next records when user hovers the last 5 list items
    $scope.loadSortedData = function()   //index stores index of item
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        $scope.showMessage1 = false;
        $scope.customerList = [];

        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 300
        });

        //$stateParams is used to retrieve value from url
        AppFactory.setCategory($stateParams.category);     //set category(lead/propect/customer)

        //function in factory to get customer list
        AppFactory.getCustomerList("0",$scope.orderString,function(result)
        {
            console.log("result:"+JSON.stringify(result));
            $scope.allData = result;

            if($scope.allData.length == 0)
            {
                $scope.showMessage1 = true;
            }

            //parsing of the result
            for(singleData in $scope.allData)
            {
                //checking whether result has error
                if(singleData == "error")
                {   //if error occurs
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        content: "<i class='icon ion-alert-circled' style='font-size: 40px;'></i><br>Loading Failed",
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showdelay: 300
                    });

                    $timeout(function(){
                        $ionicLoading.hide();
                    },1000);
                }
                else
                {   //parsing and storing data
                    var innerData = $scope.allData[singleData];

                    for(singleRecord in innerData)
                    {
                      innerData[singleRecord].id = singleRecord;            //adding internal id of customer in innerData object
                      $scope.customerList.push(innerData[singleRecord]);    //adding innerData object to customerList array
                    }
                }
            }
            $scope.arrayLength=$scope.customerList.length;//get customerList array length used for server-search
            $ionicLoading.hide();
        });
    }

    $scope.loadNextData1 = function()   //index stores index of item
    {
        console.log("length:"+$scope.allData.length);
            /*if($scope.flag==1)
            {*/
                $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                });

                var temp = $scope.customerList.length;
                AppFactory.getCustomerList(temp,$scope.orderString,function(result)
                {                     
                    console.log("Records:"+JSON.stringify(result));
                    if(result == null || result == undefined || result == [])
                    {
                        //alert("no more data");
                    }

                    //parsing of data
                    $scope.allData = result;
                    
                    if($scope.allData.length == 0)
                    {
                        $scope.showMessage = true;
                    }

                    for(singleData in $scope.allData)
                    {
                        var innerData = $scope.allData[singleData];

                        for(singleRecord in innerData)
                        {
                            innerData[singleRecord].id = singleRecord;
                            $scope.customerList.push(innerData[singleRecord]);
                        }
                    }
                    $ionicLoading.hide();
                    //console.log("Records:"+JSON.stringify($scope.customerList));
                });
    }

    //function called when we click on particular customer
    $scope.goToCustomer = function(custId)     //custId contains internalId of the customer
    {
        AppFactory.setCustomerId(custId);     //setting customer internal id so that it can be used further
        $state.go('app.customerDetails');
    }

    $scope.showSortList = function() 
    {

        console.log("show function called()");

        // Show the action sheet
        $ionicActionSheet.show(
        {
            titleText: 'Sort By',
            buttons: [
                { text: 'Name', },
                { text: 'Status' }
            ],

            /* Cancel Button */
            cancelText: 'Cancel',
           
            cancel: function() 
            {
                console.log('CANCELLED');
            },

            /* Button Click Functionality */
            buttonClicked: function(index) 
            {
                if (index==0) 
                {
                    $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Name?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $scope.orderString = 'Name';
                            $scope.loadSortedData();
                        } 
                        else 
                        {
                            console.log('You are not sure');
                        }
                    });
                };

                if (index==1) 
                {
                    $ionicPopup.confirm({
                        title: 'Alert',
                        content: 'Do you want to sort by Status?'
                        }).then(function(res) {
                            if(res) 
                            {
                                $scope.orderString = 'Status';
                                $scope.loadSortedData();
                            } 
                            else 
                            {
                                console.log('You are not sure');
                            }
                        });
                };
            return true;
            }
        });
    };

    //to go to add customer page
    $scope.goTo = function()
    {
        $state.go('app.addCustomer');
    }

    //to back to home page from top 5 customers page
    $scope.goHome = function ()
    {
        $state.go('app.homepage');
    }
});
/*--------------------------------------------------Customer Page Controller end--------------------------------------------------------*/

/*-------------------------------------------------Customer Detail Controller--------------------------------------------------------
                      this is the common controller for page displayed after selecting a lead/prospect/customer */

app.controller('CustomerDetailController', function($scope, $state, AppFactory, $ionicActionSheet, $ionicLoading, $ionicPopup, $rootScope) 
{
    $rootScope.sideMenuEnabled = false;
    $scope.showPage = false;
    /*----------variables---------------*/
    $scope.customerData = {};
    $scope.primaryInfoArray = [];

    /*----------functions---------------*/
    $scope.init = function()
    {
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        AppFactory.getCustomer(function(result)
        {
            console.log(JSON.stringify(result));
            for(item in result)
            {
                var singleData = result[item];
                for(innerData in singleData)
                {
                var data = singleData[innerData];

                    for(record in data)
                    {
                        if (data[record].section_header == "Header")
                        {
                            switch(data[record].priority)
                            {
                            case "1": $scope.customerData.custName = data[record].entityid;
                               break;
                            case "2": $scope.customerData.custStatus = data[record].entitystatus;
                                break;
                            }
                        }
                        else
                        {
                            var keys = data[record];
                            for(key in keys)
                            {
                                if((key !="value")&&(key!="priority")&&(key!="label")&&(key!="section_header"))
                                {
                                    data[record].dislay_value = keys[key];
                                }
                            }
                            $scope.primaryInfoArray.push(data[record]);
                        }
                    }
                }
            }
            console.log(JSON.stringify($scope.primaryInfoArray));
            $scope.primaryInfoArray.sort(function(a, b)
            {
                return a.priority - b.priority;
            });
            $scope.primaryInfoArray.splice($scope.primaryInfoArray.length-1, 1);
            AppFactory.setCustomerNameStatus($scope.customerData);

            $scope.showPage = true;
            $ionicLoading.hide();
        });
    }
    $scope.init();

    $scope.pageTitle = AppFactory.getCategory() + " DETAILS";
    console.log("CustomerDetailController appearing");
    
    $scope.show = function(operation) 
    {
        // Show the action sheet
        if($rootScope.showOpportunity == true)
        {
            $ionicActionSheet.show(
            {
                buttons: [
                    { text: 'Sales Order', },
                    { text: 'Quote' },
                    { text: 'Opportunity' },
                ],
             
             
                cancelText: 'Cancel',
                cancel: function() 
                {
                    console.log('CANCELLED');
                },

                /* Button Click Functionality */
                buttonClicked: function(index) 
                {
                    if (index==0) 
                    {
                        if (operation == "add")
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to Add Order?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setOrderData("");
                                        $state.go('app.addOrder'); 
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });   
                        }
                        else
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to View Order?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setType("ORDER")
                                        $state.go('app.customerTransactionList');
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });
                        }
                    };
                    if (index==1) 
                    {
                        if (operation == "add")
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to Add Quote?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setQuoteData("");
                                        $state.go('app.addQuote'); 
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });   
                        }
                        else
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to View Quote?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setType("QUOTE")
                                        $state.go('app.customerTransactionList');
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                }); 
                        }
                    };
                    if (index==2) 
                    {
                        if (operation == "add")
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to Add Opportunity?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setOpportunityData("");
                                        $state.go('app.addOpportunity'); 
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });    
                        }
                        else
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to View Opportunity?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setType("OPPORTUNITY")
                                        $state.go('app.customerTransactionList');
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });
                        }
                        /*$state.go('AddOpportunity');
                        if(confirm("Do you want to add an Opportunity"))
                        {
                          AppFactory.setCategory("OPPORTUNITY");
                          $state.go('AddOpportunity');
                        }*/
                    };
                    return true;
                }
            });
        }
        else
        {
            $ionicActionSheet.show(
            {
                buttons: [
                    { text: 'Sales Order', },
                    { text: 'Quote' }
                ],
             
             
                cancelText: 'Cancel',
                cancel: function() 
                {
                    console.log('CANCELLED');
                },

                /* Button Click Functionality */
                buttonClicked: function(index) 
                {
                    if (index==0) 
                    {
                        if (operation == "add")
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to Add Order?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setOrderData("");
                                        $state.go('app.addOrder'); 
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });   
                        }
                        else
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to View Order?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setType("ORDER")
                                        $state.go('app.customerTransactionList');
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });
                        }
                    };
                    if (index==1) 
                    {
                        if (operation == "add")
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to Add Quote?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setQuoteData("");
                                        $state.go('app.addQuote'); 
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                });   
                        }
                        else
                        {
                            $ionicPopup.confirm({
                                title: 'Alert',
                                content: 'Do you want to View Quote?'
                                }).then(function(res) {
                                    if(res) 
                                    {
                                        AppFactory.setType("QUOTE")
                                        $state.go('app.customerTransactionList');
                                    } 
                                    else 
                                    {
                                        console.log('You are not sure');
                                    }
                                }); 
                        }
                    };
                    return true;
                }
            });
        }
       
    }

    $scope.goBack = function()
    {
        if(AppFactory.getPreviousCategory() == "TOP_CUSTOMER")
        {
            $state.go('app.customer',{category:AppFactory.getPreviousCategory()});
        }
        else
        {
            $state.go('app.customer',{category:AppFactory.getCategory()});
        }
    }

    $scope.goto = function(type)
    {
        AppFactory.setType(type);
        $state.go('app.addressContactList');
    }

    $scope.goToEdit = function()
    {
        AppFactory.setPrimaryInfo($scope.primaryInfoArray);
        $state.go('app.editCustomer');
    }
});
/*-------------------------------------------------Customer Detail Controller end------------------------------------------------------*/


/*-------------------------------------------------Add Relationship Controller--------------------------------------------------------
                              this is the common controller for adding new lead/prospect/customer */

app.controller('AddRelationshipController', function($scope, AppFactory, $state, $ionicModal, $ionicLoading,$ionicPopup) 
{
    /*----------variables---------------*/
    $scope.customer = {};
    $scope.statusList = [];
    $scope.categoryList = [];
    $scope.customerType = "";
    $scope.myTitle = "ADD " + AppFactory.getCategory();
    $scope.name = {};

     if(AppFactory.getCategory() == "LEAD")
    {
        $scope.customer.personaltype = "lead";
        $scope.customerType = "LEAD";
    }
    else if(AppFactory.getCategory() == "PROSPECT")
    {
        $scope.customer.personaltype = "prospect";
        $scope.customerType = "PROSPECT";
    }
    else if(AppFactory.getCategory() == "CUSTOMER")
    {
        $scope.customer.personaltype = "customer";
        $scope.customerType = "CUSTOMER";
    }

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('status.html', function(statusWindow)
    {
        $scope.statusWindow = statusWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.openstatusWindow = function() 
    {

        $scope.statusWindow.show();
        $scope.statusList = [];

        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching status...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        var parameters =
            {
                "field_name":"entitystatus",
                "field_type":"select",
                "field_subtext":"",
                "customer_type":$scope.customerType,
                "field_dependent":"",
                "field_dependentvalue":""
            };

    
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(responseData)  
        { 
            for(item in responseData)
            {
                var statusData = responseData[item];
                var statusInfo = {};
                statusInfo.index = statusData[0];
                statusInfo.value = statusData[1];
                $scope.statusList.push(statusInfo);
            }
            $ionicLoading.hide();
        });

    };

    $scope.closestatusWindow = function() 
    {
        $scope.statusWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.statusWindow.remove();
    });

    $scope.statusTypeSelected = function(statusInfo)
    {
        $scope.statusWindow.hide();
        //var statusInfo = JSON.parse(data);
        console.log(JSON.stringify(statusInfo));
        $scope.customer.entitystatusvalue = statusInfo.value;
        $scope.customer.entitystatus = statusInfo.index;
    }

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('category.html', function(categoryWindow) 
    {
        $scope.categoryWindow = categoryWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
  

    $scope.opencategoryWindow = function() 
    {

        $scope.categoryWindow.show();
        $scope.categoryList = [];

        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching category...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        var parameters =
            {
                "field_name":"category",
                "field_type":"select",
                "field_subtext":"",
                "customer_type":$scope.customerType,
                "field_dependent":"",
                "field_dependentvalue":""
            };

    
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(responseData)  
        { 
            for(item in responseData)
            {
                var categoryData = responseData[item];
                var categoryInfo = {};
                categoryInfo.index = categoryData[0];
                categoryInfo.value = categoryData[1];
                $scope.categoryList.push(categoryInfo);
            }
            $ionicLoading.hide();
        });
    };

    $scope.closecategoryWindow = function() 
    {
        $scope.categoryWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.categoryWindow.remove();
    });

    $scope.categoryTypeSelected = function(categorInfo)
    {
        $scope.categoryWindow.hide();
        //var categorInfo = JSON.parse(data);
        console.log(JSON.stringify(categorInfo));
        $scope.customer.categoryvalue = categorInfo.value;
        $scope.customer.category = categorInfo.index;
    }

    $scope.saveData = function()
    {
        console.log("fname:"+$scope.name.firstname+" lname:"+$scope.name.lastname);
        if(AppFactory.getCategory() == 'LEAD')
        {
            if($scope.name.firstname == undefined || $scope.name.firstname == "")
            {
                $scope.showAlert("Please fill First Name field");
                return;
            }
            else
            {
                $scope.customer.firstname = $scope.name.firstname;
            }

            if($scope.name.lastname == undefined || $scope.name.lastname == "")
            {
                $scope.showAlert("Please fill Last Name field");
                return;
            }
            else
            {
                $scope.customer.lastname = $scope.name.lastname;
            }
        }
        else
        {
            if($scope.name.entityid == undefined || $scope.name.entityid == "")
            {
                $scope.showAlert("Please fill Name field");
                return;
            }
            else
            {
                $scope.customer.entityid == $scope.name.entityid;
            }
        }

        if($scope.customer.entitystatusvalue == undefined || $scope.customer.entitystatusvalue == "")
        {
            $scope.showAlert("Please fill Status field");
            return;
        }

        if($scope.customer.companyname == undefined || $scope.customer.companyname == "")
        {
            $scope.showAlert("Please fill Company Name field");
            return;
        }

        if(!($scope.customer.phone == undefined))
        {
            var bracketPattern = /^\(?([0-9]{3})\)?[ ]?([0-9]{3})[-]?([0-9]{4})$/;
            var dotPattern = /^([0-9]{3})?[.]?([0-9]{3})[.]?([0-9]{4})$/;
            var hyphenPattern = /^([0-9]{3})?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            var spacePattern = /^([0-9]{3})?[ ]?([0-9]{3})[ ]?([0-9]{4})$/;
            var digits = /^\d{10}$/;  

            if($scope.customer.phone.match(bracketPattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(dotPattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(hyphenPattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(spacePattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(digits))  
            {  
                console.log("valid");
            }    
            else  
            {  
                $ionicPopup.alert({
                    title: 'Validation Error',
                    content: 'Please enter a valid Phone Number.<br>eg:111-111-1111<br>111 111 1111<br>111.111.1111<br>(111) 111-1111<br>1111111111'
                    }).then(function(res) {
                        
                    });
                    return;
            }
        }

        if(!($scope.customer.email == undefined))
        {
            var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.[a-zA-Z]{2,3})+$/;
            //var pattern = /^([0]?[0-9]|[1][0-2])(:[0-5][0-9])?$/;

            if($scope.customer.email.match(pattern))  
            {  
                console.log("valid");
            }
            else  
            {  
                $ionicPopup.alert({
                    title: 'Validation Error',
                    content: 'Please enter a valid email address.'
                    }).then(function(res) {
                        
                    });
                    return;
            }
        }
       
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });
        console.log("data:"+JSON.stringify($scope.customer));
        
        AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=115&deploy=1",$scope.customer,null,function(results)
        { //get result from json
            if(results == "\"lead record saved successfully.\"" || results == "\"customer record saved successfully.\"" || results == "\"prospect record saved successfully.\"")
            {
                console.log("Results : "+results);
                $ionicLoading.hide();
                window.plugins.toast.show("Record is successfully saved","short","center");
                $state.go('app.customer',{category:AppFactory.getCategory()});
            }
            else
            {
                $scope.showAlert(results);
                $ionicLoading.hide();
            }

        });
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
            title: 'Validation Error',
            content: message
            }).then(function(res) {
        
        });
    };

    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.customer',{category:AppFactory.getCategory()});
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };

    $scope.goTo = function()
    {
        $scope.showConfirm();
    }
});
/*-------------------------------------------------Add Relationship Controller end-----------------------------------------------------*/


/*-------------------------------------------------Edit Relationship Controller------------------------------------------------------
                                this is the common controller for editing lead/prospect/customer */

app.controller('EditRelationshipController', function($scope, AppFactory, $state, $ionicModal, $ionicLoading,$ionicPopup) 
{
    /*----------variables---------------*/
    $scope.customer = {};
    $scope.statusList = [];
    $scope.categoryList = [];
    $scope.primaryInfoArray = AppFactory.getPrimaryInfo();
    $scope.customerData = AppFactory.getCustomerNameStatus();
    $scope.myTitle = "EDIT " + AppFactory.getCategory();

    if(AppFactory.getCategory() == "LEAD")
    {
        $scope.customer.personaltype = "lead";
        $scope.customerType = "LEAD";
    }
    else if(AppFactory.getCategory() == "PROSPECT")
    {
        $scope.customer.personaltype = "prospect";
        $scope.customerType = "PROSPECT";
    }
    else if(AppFactory.getCategory() == "CUSTOMER")
    {
        $scope.customer.personaltype = "customer";
        $scope.customerType = "CUSTOMER";
    }

    /*----------functions---------------*/
    $scope.init = function()
    {
        $scope.customer.entityid = $scope.customerData.custName;
        $scope.customer.entitystatusvalue = $scope.customerData.custStatus;
        $scope.customer.entitystatus = $scope.customerData.statusIndex;
        $scope.customer.internalid = $scope.customerData.internalId;
        for(object in $scope.primaryInfoArray)
        {
            var singleObject = $scope.primaryInfoArray[object];
            for(item in singleObject)
            {
                //console.log("key: "+JSON.stringify(item)+" value: "+JSON.stringify(singleObject[item]));
                if(item == "category")
                {
                    $scope.customer.categoryvalue = singleObject[item];
                    $scope.customer.category = singleObject.value;
                }
                else if(item == "companyname")
                {
                    $scope.customer.companyname = singleObject[item];
                }
                else if(item == "url")
                {
                    $scope.customer.url = singleObject[item];
                }
                else if(item == "email")
                {
                    $scope.customer.email = singleObject[item];
                }
                else if(item == "comments")
                {
                    $scope.customer.comments = singleObject[item];
                }
                else if(item == "phone")
                {
                    $scope.customer.phone = singleObject[item];
                }
            }
        }
    }
    $scope.init();
    
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('status.html', function(statusWindow) 
    {
        $scope.statusWindow = statusWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.openstatusWindow = function() 
    {

        $scope.statusWindow.show();
        $scope.statusList = [];

        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching status...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        var parameters =
            {
                "field_name":"entitystatus",
                "field_type":"select",
                "field_subtext":"",
                "customer_type":$scope.customerType,
                "field_dependent":"",
                "field_dependentvalue":""
            };

    
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(responseData)  
        { 
            for(item in responseData)
            {
                var statusData = responseData[item];
                var statusInfo = {};
                statusInfo.index = statusData[0];
                statusInfo.value = statusData[1];
                $scope.statusList.push(statusInfo);
            }
            $ionicLoading.hide();
        });
    };

    $scope.closestatusWindow = function() 
    {
        $scope.statusWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.statusWindow.remove();
    });

    $scope.statusTypeSelected = function(statusInfo)
    {
        $scope.statusWindow.hide();
        //var statusInfo = JSON.parse(data);
        console.log(JSON.stringify(statusInfo));
        $scope.customer.entitystatusvalue = statusInfo.value;
        $scope.customer.entitystatus = statusInfo.index;
    }

    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('category.html', function(categoryWindow) 
    {
        $scope.categoryWindow = categoryWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.opencategoryWindow = function() 
    {
        $scope.categoryWindow.show();
        $scope.categoryList = [];

        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching category...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        var parameters =
            {
                "field_name":"category",
                "field_type":"select",
                "field_subtext":"",
                "customer_type":$scope.customerType,
                "field_dependent":"",
                "field_dependentvalue":""
            };

    
        AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(responseData)  
        { 
            for(item in responseData)
            {
                var categoryData = responseData[item];
                var categoryInfo = {};
                categoryInfo.index = categoryData[0];
                categoryInfo.value = categoryData[1];
                $scope.categoryList.push(categoryInfo);
            }
            $ionicLoading.hide();
        });
    };

    $scope.closecategoryWindow = function() 
    {
        $scope.categoryWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.categoryWindow.remove();
    });

    $scope.categoryTypeSelected = function(categorInfo)
    {
        $scope.categoryWindow.hide();
        //var categorInfo = JSON.parse(data);
        console.log(JSON.stringify(categorInfo));
        $scope.customer.categoryvalue = categorInfo.value;
        $scope.customer.category = categorInfo.index;
    }
    
    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.customerDetails');
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };

    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }

    $scope.showAlert = function(message) {
        $ionicPopup.alert({
        title: 'Validation Error',
        content: message
        }).then(function(res) {

        });
    };

    $scope.saveData = function() 
    {

        if($scope.customer.entityid == undefined || $scope.customer.entityid == "")
        {
            $scope.showAlert("Please fill Name field");
            return;
        }

        if($scope.customer.entitystatusvalue == undefined || $scope.customer.entitystatusvalue == "")
        {
            $scope.showAlert("Please fill Status field");
            return;
        }

        if($scope.customer.companyname == undefined || $scope.customer.companyname == "")
        {
            $scope.showAlert("Please fill Company Name field");
            return;
        }

        if($scope.customer.phone.length > 0)
        {
            var bracketPattern = /^\(?([0-9]{3})\)?[ ]?([0-9]{3})[-]?([0-9]{4})$/;
            var dotPattern = /^([0-9]{3})?[.]?([0-9]{3})[.]?([0-9]{4})$/;
            var hyphenPattern = /^([0-9]{3})?[-]?([0-9]{3})[-]?([0-9]{4})$/;
            var spacePattern = /^([0-9]{3})?[ ]?([0-9]{3})[ ]?([0-9]{4})$/;
            var digits = /^\d{10}$/;  

            if($scope.customer.phone.match(bracketPattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(dotPattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(hyphenPattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(spacePattern))  
            {  
                console.log("valid");
            }
            else if($scope.customer.phone.match(digits))  
            {  
                console.log("valid");
            }    
            else  
            {  
                $ionicPopup.alert({
                    title: 'Validation Error',
                    content: 'Please enter a valid Phone Number.<br>eg:111-111-1111<br>111 111 1111<br>111.111.1111<br>(111) 111-1111<br>1111111111'
                    }).then(function(res) {
                        
                    });
                    return;
            }
        }

        if($scope.customer.email.length > 0)
        {
            var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.[a-zA-Z]{2,3})+$/;
            //var pattern = /^([0]?[0-9]|[1][0-2])(:[0-5][0-9])?$/;

            if($scope.customer.email.match(pattern))  
            {  
                console.log("valid");
            }
            else  
            {  
                $ionicPopup.alert({
                    title: 'Validation Error',
                    content: 'Please enter a valid email address.'
                    }).then(function(res) {
                        
                    });
                    return;
            }
        }

        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Updating...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=115&deploy=1",$scope.customer,null,function(results)
        { 
            //get result from json
           if(results == "\"lead record edited successfully.\"" || results == "\"customer record edited successfully.\"" || results == "\"prospect record edited successfully.\"")
            {
                $ionicLoading.hide();
                window.plugins.toast.show("Record is successfully updated","short","center");
                $state.go('app.customerDetails');
            }
            else
            {
                $scope.showAlert(results);
                $ionicLoading.hide();
            }
            
        });
    }
});
/*-------------------------------------------------Edit Relationship Controller end----------------------------------------------------*/

/*-----------------------------------------------Address Contact List Controller-----------------------------------------------------
                         this is the common controller for displaying list of address/contact */

app.controller('AddressContactListController', function($scope, $state, $http, AppFactory, $ionicLoading, $filter) 
{
    /*----------variables---------------*/
    $scope.customerData = AppFactory.getCustomerNameStatus();   // to get customer data from the factory
    $scope.keyArray = [];                                       // to store the index of the objects 
    $scope.valueArray = [];                                     // to store the objects where address data is present
    $scope.contactListArray = [];                               // to store list of contacts
    $scope.newflag = "false";                       //flag to display message
    $scope.listflag = 1;                            //flag to check whether searched string initially present in customer list
    $scope.showMessage = false;                            

       /*Clear Search Box*/
     $scope.clearSearch = function () {
        $scope.searchString="";
    }
    
    $scope.init = function()
    {
        $scope.keyArray = [];
        $scope.valueArray = [];
        $scope.contactListArray = [];

        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        // if we have clicked on the address functionality
        if(AppFactory.getType() == "ADDRESS")
        {
            // parameters to send with the requrest of getting data of address list
            var parameters =
            { //declare an object
                id:"customsearch_aavz_17",
                internalid:$scope.customerData.internalId,
                customer_type:"customer"
            };
     
            // sending the request to the server to get data 
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=100&deploy=1",null,parameters,function(results)
            { 
                //get result from json
                     
                for(mainArray in  results)
                {
                    obj =  results[mainArray];

                    for(objKey in obj)
                    {
                       var objValue = obj[objKey];
                       objValue.id=objKey;
                        // pushing the address data into the valuearray
                       $scope.valueArray.push(objValue);
                        // pushing the indexes of objects into the keyarray
                       $scope.keyArray.push(objKey);
                    }
                }
                if($scope.valueArray.length == 0)
                {
                    $scope.showMessage = "true";
                }

                // to hide the loading indicator 
                $ionicLoading.hide();
            });
        }
        else if(AppFactory.getType() == "CONTACT")
        {
            var parameters = {
                "id":"customsearch_aavz_18",
                "internalid":$scope.customerData.internalId,
                "customer_type":"contact"
            }; 

            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=106&deploy=1",null,parameters,function(responseData)    
            { 
                // Get result from json
                //console.log("Success - Contact List :"+responseData);
                for(i=0;i<responseData.length;i++)
                {
                    var obj = {};
                    obj= responseData[i];
                    for(itemKey in obj)
                    {
                            var object = obj[itemKey];
                            object.id=itemKey;
                            $scope.contactListArray.push(object);
                    }
                 }
                if($scope.contactListArray.length == 0)
                {
                    $scope.showMessage = "true";
                }
                $ionicLoading.hide();
                console.log("Data : "+JSON.stringify($scope.contactListArray));
            });
        }
    }
    $scope.init();

    $scope.pageTitle = AppFactory.getType() + " LIST";

    console.log("CustomerDetailController appearing");
    $scope.goBack = function() 
    {
        $state.go('app.customerDetails');
    }

    // button to add the new address
    $scope.goToAdd = function() 
    {
        $state.go('app.addAddressContact');
    }

    // this fuction will be called when any list item of address will be clicked
    $scope.goto = function(address_contact_id)
    {
        // it sets the address contact id 
        AppFactory.setAddressContactId(address_contact_id);
        // it redirects to the address/contact details page
        $state.go('app.addressContactDetail');
    }

    $scope.$watch("searchString", function(query)
    {
        if(AppFactory.getType() == "CONTACT")
            $scope.counter = $filter("filter")($scope.contactListArray, query).length;
        else
            $scope.counter = $filter("filter")($scope.valueArray, query).length;

        console.log("Count : "+$scope.counter);
       
        if($scope.listflag == 1) 
        {
            $scope.listflag = 0;  
            $scope.newflag = "false";       
        }
        else if($scope.counter == 0 && $scope.listflag == 0)
        {    
            $scope.message="No results found.";  
            $scope.newflag="true";
        } 
        else
        {
            $scope.newflag="false";
        }
    
    });//end of $watch()
});
/*------------------------------------------------Address Contact List Controller end-----------------------------------------------------*/


/*------------------------------------------------Add Address Contact Controller-----------------------------------------------------
                               this is the common controller of page for adding new address/contact */

app.controller('AddAddressContactController', function($scope, AppFactory, $state, $ionicModal, $ionicLoading, $ionicPopup) 
{
    /*----------variables---------------*/
    $scope.myTitle = "ADD " + AppFactory.getType();             // it is to set title text
    $scope.customerData = AppFactory.getCustomerNameStatus();   // it gets the customer data from the factory
    $scope.customer = {};                                       // it is empty object where data will be kept which is to add .
    $scope.countryList = [];                                    // it retains data of the countries 
    $scope.stateList = [];                                      // it retains data of the states

    /*----------functions---------------*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('country.html', function(countryWindow) 
    {
        $scope.countryWindow = countryWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when user clicks on the countrylist dropdown
    $scope.opencountryWindow = function() 
    {
        $scope.countryWindow.show(); 
        // it begins loading indicator 
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching country list...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
   
            // counrty list is inintialised as empty because it should not contain duplicated data.
            $scope.countryList= [];
            // parameters to send with the requrest to get the list of countries ...
            var parameters =
                { 
                   "field_name":"country",
                   "field_type":"select",
                   "field_subtext":"",
                   "customer_type":"CUSTOMER",
                   "field_dependent":"",
                   "field_dependentvalue":""
                };
                // it send the request to get data of countries..
                AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
                { 
                          
                    for(mainArray in  results)
                    {
                        obj =  results[mainArray];
                         var temp={ };
                         temp.index= obj[0];
                         temp.value= obj[1];
                         // push every object into the array named countrylist
                         $scope.countryList.push(temp);
        
                    }
                    // to stop the loading indicator
                    $ionicLoading.hide();
                    // to show the model window where list of countries are there.                
                });

      //AppFactory.setStateList($scope.stateList);

    };
    // this function will be called when cancel button will be clicked.
    $scope.closecountryWindow = function() 
    {
        // it hides the countryWindow view
        $scope.countryWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.countryWindow.remove();
    });

    // this will be called when any of the country will be selected ..
    $scope.countrySelected = function(countrydata)
    {
        // it hides the countryWindow view
        $scope.countryWindow.hide();
        // it holds the value of country selected..
        $scope.customer.country = countrydata.value;
        
        // if the selected country is not united states
        if($scope.customer.country!="United States")
        {
            // textbox of state will be set to empty
            $scope.customer.state=null;
            // array of state list is set to null
            $scope.stateList= null;
        }
        // it holdes the index of the selected country..
        $scope.customer.countryIndex = countrydata.index;
    }

    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('state.html', function(stateWindow) 
    {
        $scope.stateWindow = stateWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });
      
    
    // this function will be called when we click on the state on the form .
    $scope.openstateWindow = function() 
    {
        // initially empty the state list to avoid duplications. 
        $scope.stateList=[];
        // if country selected is united states ..

        $scope.stateWindow.show();

        if($scope.customer.country == "United States")
        {
            // to begin loading indicator..
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching state list...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

            // parameters to send with the request to get the list of states..
            var parameter1 =
            { 
            "field_name":"state",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"CUSTOMER",
            "field_dependent":"",
            "field_dependentvalue":""
            };
           
            // send the request to get the data..
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameter1,function(results)
            { 
               // console.log("Response of country list ===:"+JSON.stringify(results));
            
                for(mainArray in  results)
                {
                    obj = results[mainArray];
                    var temp = {};
                    temp.index = obj[0];
                    temp.value = obj[1];
                    // push every object into the statelist array. 
                    $scope.stateList.push(temp);
                }
                // to stop the loading indicator
                $ionicLoading.hide();
                // to populate the list of state view.
                
            });
        } 
    };

    // it will be call when cancel button on the form will be clicked..
    $scope.closestateWindow = function() 
    {
        $scope.stateWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.stateWindow.remove();
    });

    // it will be called when any of the item from the state list will be selected.
    $scope.stateSelected = function(statedata)
    {
        // hide the populated view
        $scope.stateWindow.hide();
        // to show the selected state on the form.
        $scope.customer.state = statedata.value;
        // get the index of the selected state.        
        $scope.customer.stateIndex = statedata.index;
    }
    
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('shipping.html', function(shippingWindow) 
    {
        $scope.shippingWindow = shippingWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when user clicks on the countrylist dropdown
    $scope.openshippingWindow = function() 
    {
        $scope.shippingWindow.show(); 
    };
    // this function will be called when cancel button will be clicked.
    $scope.closeshippingWindow = function() 
    {
        // it hides the countryWindow view
        $scope.shippingWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.shippingWindow.remove();
    });

    // this will be called when any of the country will be selected ..
    $scope.shippingSelected = function(value)
    {
       // hide the populated view
        $scope.shippingWindow.hide();
        // to set the value in customer object.
        $scope.customer.isdefaultshipping = value;
        // show the selected value on the form.   
        if(value == "T")
            $scope.isdefaultshippingValue = "YES";
        else
            $scope.isdefaultshippingValue = "NO";
    }

    // cancel button to go to the address/ contact list page.
    $scope.showConfirm = function() {

    $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.addressContactList');
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };

    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }

    //  save button to save the filled data on the form ..
    
    $scope.saveData = function() 
    {
        if(AppFactory.getType() == "CONTACT")
        {
        // for contact list
            if($scope.customer.name == null)
            {
                $ionicPopup.alert({
                    title: 'Alert',
                    content: 'Please provide name'
                    }).then(function(res) {
                        
                    });
                    return;
            }
            
            if(!($scope.customer.phone == undefined))
            {
                var bracketPattern = /^\(?([0-9]{3})\)?[ ]?([0-9]{3})[-]?([0-9]{4})$/;
                var dotPattern = /^([0-9]{3})?[.]?([0-9]{3})[.]?([0-9]{4})$/;
                var hyphenPattern = /^([0-9]{3})?[-]?([0-9]{3})[-]?([0-9]{4})$/;
                var spacePattern = /^([0-9]{3})?[ ]?([0-9]{3})[ ]?([0-9]{4})$/;
                var digits = /^\d{10}$/;  

                if($scope.customer.phone.match(bracketPattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(dotPattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(hyphenPattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(spacePattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(digits))  
                {  
                    console.log("valid");
                }    
                else  
                {  
                    $ionicPopup.alert({
                        title: 'Validation Error',
                        content: 'Please enter a valid Phone Number.<br>eg:111-111-1111<br>111 111 1111<br>111.111.1111<br>(111) 111-1111<br>1111111111'
                        }).then(function(res) {
                            
                        });
                        return;
                }
            }

            if(!($scope.customer.email == undefined))
            {
                var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.[a-zA-Z]{2,3})+$/;
                //var pattern = /^([0]?[0-9]|[1][0-2])(:[0-5][0-9])?$/;

                if($scope.customer.email.match(pattern))  
                {  
                    console.log("valid");
                }
                else  
                {  
                    $ionicPopup.alert({
                        title: 'Validation Error',
                        content: 'Please enter a valid email address.'
                        }).then(function(res) {
                            
                        });
                        return;
                }
            }
            
            // to begin the loading indicator ..
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

            var custData = AppFactory.getCustomerNameStatus();
            var internalId = custData.internalId;
            console.log("id:"+internalId);
            var jsonobj={
                        "internalid":$scope.customerData.internalId,
                        "phone":$scope.customer.phone,
                        "title":$scope.customer.jobtitle,
                        "email":$scope.customer.email,
                        "entityid":$scope.customer.name
                        };

            var data=jsonobj;
            var parameters = null;
    
            AppFactory.sendHttpRequest("POST","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=102&deploy=1",data,parameters,function(responseData)  
            { 
                //alert(responseData);
                if(responseData == "\"Contact saved successfully.\"")
                {
                    $state.go('app.addressContactList');
                    window.plugins.toast.show("Record is successfully saved","short","center");
                }
                else if(responseData == "\"Address saved successfully.\"")
                {
                    $state.go('app.addressContactList');
                }
                else
                {
                    $ionicPopup.alert({
                        title: 'Alert',
                        content: responseData
                        }).then(function(res) {
                            $ionicLoading.hide();
                        });
                }
                $ionicLoading.hide();
            });
        }

        // when type will be address..
        else if(AppFactory.getType() == "ADDRESS")
        {
            // to begin the loading indicator ..
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Saving...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            // to set the parameters to send along with the request..
            var parameters={
    
                         "isdefaultshipping":$scope.customer.isdefaultshipping,
                          "statevalue":$scope.customer.state,
                          "zip":$scope.customer.zip,
                          "countryvalue":$scope.customer.country,
                          "addr1":$scope.customer.add1,
                          "addr2":$scope.customer.add2,
                          "state":$scope.customer.stateIndex,
                          "addressee":$scope.customer.addressee,
                          "internalid":$scope.customerData.internalId,
                          "personaltype":"customer",
                          "country":$scope.customer.countryIndex,
                          "city":$scope.customer.city
                        };
            // send the request to save the data..                    
            AppFactory.sendHttpRequest("post","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=122&deploy=1",parameters,null,function(responseData) 
            { 
                if(responseData == "\"Contact saved successfully.\"")
                {
                    $state.go('app.addressContactList');
                }
                else if(responseData == "\"Address saved successfully.\"")
                {
                    $state.go('app.addressContactList');
                    window.plugins.toast.show("Record is successfully saved","short","center");
                }
                else
                {
                    $ionicPopup.alert({
                        title: 'Alert',
                        content: responseData
                        }).then(function(res) {
                            $ionicLoading.hide();
                        });
                }
                // to stop the loading indicator..
                $ionicLoading.hide();
                // redirects on the list page..
                $state.go('app.addressContactList');
            });

        }
    }
});
/*------------------------------------------------Add Address Contact Controller end---------------------------------------------------*/


/*-----------------------------------------------Address Contact Detail Controller----------------------------------------------------
                       this is the common controller for displaying details of selected address/contact */

app.controller('AddressContactDetailController', function($scope, $state, $http, AppFactory, $ionicLoading) 
{
    $scope.showPage = false;
    /*----------variables---------------*/
    var addressId = AppFactory.getAddressContactId();           // to get the id of the address which was clicked..
    $scope.customerdata=AppFactory.getCustomerNameStatus();     // to get the customer's data..
    $scope.key = [];                                            // to store the keys of the objects 
    $scope.value = [];                                          // to store the objects 

    /*----------functions---------------*/
    $scope.init = function()
    {
        // to begin the loading indicator ..
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        // if the type is address
        if(AppFactory.getType() == "ADDRESS")
        {
            // parameters to set along with the request..
            var parameters=
            { 
          
                "id":"customsearch_aavz_13",
                "noofrecords":"100",
                "addressinternalid":addressId,
                "internalid":$scope.customerdata.internalId,
                "display":"1,2,10,3,6,5,4,7,8,9",
                "groupname":"Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Primary Information",
                "fieldname":"addressee,addr1,internalid,addr2,state,country,city,zip,isdefaultshipping,addressinternalid",
                "customer_type":"customer"
            };
          
         
            //Request for get data of the address details
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=122&deploy=1",null,parameters,function(results)
            { 
                for(singleObj in  results)
                {
                    obj =  results[singleObj];
            
                    for(innerobj in obj)
                    {
                        obj1 = obj[innerobj];
          
                        for(mainobj in obj1)
                        { 
                            var objValue = obj1[mainobj];
                                     
                            for(objkey in objValue)
                            {
                                if((objkey !="value")&&(objkey!="priority")&&(objkey!="label")&&(objkey!="section_header"))
                                {
                                    obj1[mainobj].gotValue = objValue[objkey];
                                }
              
                            } 
                            // to store the data of address details
                            $scope.value.push(objValue);
                            // store the keys of the object..
                            $scope.key.push(mainobj);
         
                        }
         
                         // to sort the address details data on the basis of priority .. 
                        $scope.value.sort(function(a, b)
                        {
                            return a.priority - b.priority;
                        });
                    }
                }
                // to remove the last line of the value array ..
                $scope.value.splice($scope.value.length-1,1);

                $scope.showPage = true;
                // to stop the loading indicator ..
                $ionicLoading.hide();
            });
        }

        else if(AppFactory.getType() == "CONTACT")
        {
            var parameters = {
                'internalid':addressId,
                'id':'customsearch_aavz_14',
                'display':'4,1,3,2',
                'groupname':'Primary Information,Primary Information,Primary Information,Primary Information',
                'fieldname':'email,entityid,phone,title',
                'noofrecords':'0',
                'customer_type':'contact'
            };  

            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=102&deploy=1",null,parameters,function(responseData) 
            { 
                //get result from json
                console.log("Contact Detail Success :"+responseData);            
                var object = responseData[0];
                var key = [];
                var value = [];
                for(item in object)
                {
                    $scope.id = item;
                    $scope.data = object[item];
                }
              
                for(item in $scope.data)
                {
                    key.push(item);
                    value.push($scope.data[item]);
                }
              
                key.splice(key.length-1,1);
                value.splice(value.length-1,1);
              
                var sectionArray =[];
                var labelArray = [];
                var priorityArray = [];
                var valueArray = [];
              
                for(a in value)
                {
                    var obj = value[a];
                    //console.log("Data1 : "+obj.section_header);
                    priorityArray.push(obj.priority);
                    labelArray.push(obj.label);
                    sectionArray.push(obj.section_header);
                    for(i in obj)
                    {
                        if(i != "section_header" && i !="label" && i !="priority" && i !="value")
                        {
                            valueArray.push(obj.value);
                        }
                    }
                }
                
                console.log("Priority :"+JSON.stringify(priorityArray));
                console.log("Sections :"+JSON.stringify(sectionArray));
                console.log("Values :"+JSON.stringify(valueArray));
                console.log("Labels :"+JSON.stringify(labelArray));
              
                $scope.primaryInformation=[];
                for(i=1;i<=priorityArray.length;i++)
                {
                    for(j=0;j<priorityArray.length;j++)
                    {
                        if(priorityArray[j] == i && sectionArray[j] == "Primary Information")
                        {
                            var obj={};
                            obj.key = labelArray[j];
                            obj.value = valueArray[j];
                            $scope.primaryInformation.push(obj);
                            break;
                        }
                    }
                }
                console.log("Data : "+$scope.primaryInformation.length);
                var object = 
                {
                    data:$scope.primaryInformation
                };
                console.log("Data Obj : "+JSON.stringify(object));

                AppFactory.setContactObject(object);
                $scope.showPage = true;
                // to stop the loading indicator ..
                $ionicLoading.hide();
           });
        }
    }
    $scope.init();
 
    $scope.pageTitle = AppFactory.getType() + " DETAILS";
    $scope.status = AppFactory.getStatus();

    console.log("CustomerDetailController appearing");

    $scope.goBack = function()
    {
        $state.go('app.addressContactList');
    }

    // edit button to go onto the edit page..
    $scope.goToEdit = function() 
    {
        AppFactory.setAddressDetails($scope.value);
        $state.go('app.editAddressContact');
    }
});
/*------------------------------------------------Address Contact Detail Controller end------------------------------------------------*/

/*------------------------------------------------Edit Address Contact Controller-----------------------------------------------------
                               this is the common controller of page for editing address/contact */

app.controller('EditAddressContactController', function($scope, AppFactory, $state, $ionicModal, $ionicLoading,$ionicPopup) 
{
    /*----------variables---------------*/
    $scope.myTitle = "EDIT " + AppFactory.getType();            // to set title text 
    $scope.customerdata = AppFactory.getCustomerNameStatus();   // to get the data of the customers .
    $scope.customer = {};                                       // object where edited data will be stored..
    $scope.countryList = [];                                    // to store list of countries..
    $scope.stateList = [];                                      // to store the list of states..

    /*----------functions---------------*/
    $scope.init = function()
    {
        //to begin loading indicator..
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        if(AppFactory.getType() == "CONTACT")
        {
            $scope.contactData = AppFactory.getContactObject();

            console.log("Edit Contact Data : "+JSON.stringify($scope.contactData));
            console.log("Edit Contact Data Length : "+$scope.contactData.data.length);

            for(i = 0;i < $scope.contactData.data.length;i++)
            {
                var obj = $scope.contactData.data[i];
                if(obj.key == "Name")
                {
                    $scope.customer.name = obj.value;
                }
                else if(obj.key == "Job Title")
                {
                    $scope.customer.jobtitle = obj.value;
                }
                else if(obj.key == "Phone")
                {
                    $scope.customer.phone = obj.value;
                }
                else if(obj.key == "Email")
                {
                    $scope.customer.email = obj.value;
                }
            }
            $ionicLoading.hide();
        }
        // if type is address..
        else if(AppFactory.getType() == "ADDRESS")
        {
            // to get the address details to edit it.
            $scope.addressData = AppFactory.getAddressDetails();
            // object which holds the data to be edited
            $scope.customer = {};
            
            // to get the simple object to be edited from the array of objects ..
            for(i=0;i<$scope.addressData.length;i++)
            {
                var obj= $scope.addressData[i];
                for( a in obj)
                {
                    if( a=="addressee")
                    {
                        $scope.customer.addressee=obj[a];
                    
                    }
                    else if( a=="addr1")
                    {
                        $scope.customer.addr1=obj[a];
                    
                    }
                    else if( a=="addr2")
                    {
                        $scope.customer.addr2=obj[a];
                    
                    }
                    else if( a=="city")
                    {
                        $scope.customer.city=obj[a];
                    
                    } 
                    else if( a=="state")
                    {
                        $scope.customer.state=obj[a];
                    
                    }
                    else if( a=="zip")
                    {
                        $scope.customer.zip=obj[a];
                    
                    }
                    else if( a=="country")
                    {
                        $scope.customer.country=obj[a];
                        $scope.customer.countryIndex=obj["value"];
                    }
                    else if( a=="isdefaultshipping")
                    {
                        // if isdefaultshipping is "no" make it "F"
                        if(obj[a]=="No")
                        {
                            $scope.customer.isdefaultshipping="F";
                            $scope.isdefaultshippingValue="NO";
                        }
                        // if isdefaultshipping is "yes" make it "T"
                        else if (obj[a]=="Yes")
                        {
                            $scope.customer.isdefaultshipping="T";
                            $scope.isdefaultshippingValue="YES";
                        }
                    }    
                }
            }
            // to stop the loading indicator..
            $ionicLoading.hide();
        }
    }
    $scope.init();

    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('country.html', function(countryWindow) 
    {
        $scope.countryWindow = countryWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when we click on the countrylist on the form .
    $scope.opencountryWindow = function() 
    {
        $scope.countryWindow.show();
        // it begins loading indicator 
         $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching country list...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
   
            // counrty list is inintialised as empty because it should not contain duplicated data.
            $scope.countryList= [];
           // parameters to send with the requrest to get the list of countries ...
            var parameters =
                { 
                   "field_name":"country",
                   "field_type":"select",
                   "field_subtext":"",
                   "customer_type":"CUSTOMER",
                   "field_dependent":"",
                   "field_dependentvalue":""
                };
            // it send the request to get data of countries..
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameters,function(results)
            { 
               // console.log("Response of country list ===:"+JSON.stringify(results));
   
                for(mainArray in  results)
                {
                    obj =  results[mainArray];
                     var temp = {};
                     temp.index = obj[0];
                     temp.value = obj[1];
                     $scope.countryList.push(temp);
    
                }
                // to stop the loading indicator
                $ionicLoading.hide();
                // to show the model window where list of countries are there.
                                 
            });

      
    };
    // this function will be called when cancel button will be clicked.
    $scope.closecountryWindow = function() 
    {
        // it hides the countryWindow view
        $scope.countryWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.countryWindow.remove();
    });

    // this will be called when any of the country will be selected ..
    $scope.countrySelected = function(countrydata)
    {
        // it hides the countryWindow view
        $scope.countryWindow.hide();
        // it holds the value of country selected..
        $scope.customer.country = countrydata.value;
        // if the selected country is not united states
        if($scope.customer.country!="United States")
        {
            // textbox of state will be set to empty
            $scope.customer.state=null;
            // array of state list is set to null
            $scope.stateList= null;
        }
        // it holdes the index of the selected country..
        $scope.customer.countryIndex = countrydata.index;
    }

    /* Forcast Type*/
    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('state.html', function(stateWindow) 
    {
        $scope.stateWindow = stateWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when we click on the state on the form .
    $scope.openstateWindow = function() 
    {   
        // initially empty the state list to avoid duplications. 
        $scope.stateList=[];

        $scope.stateWindow.show();

      // if country selected is united states ..
          if($scope.customer.country == "United States")
          {
            // to populate the list of state view.
             
            // to begin loading indicator..
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Fetching state list...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });
            // parameters to send with the request to get the list of states..
           var parameter1 =
            { 
            "field_name":"state",
            "field_type":"select",
            "field_subtext":"",
            "customer_type":"CUSTOMER",
            "field_dependent":"",
            "field_dependentvalue":""
            };
           
           // send the request to get the data..
            AppFactory.sendHttpRequest("GET","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=123&deploy=1",null,parameter1,function(results)
            { 
               // console.log("Response of country list ===:"+JSON.stringify(results));
            
             for(mainArray in  results)
             {
              obj =  results[mainArray];
              var temp = {};
              temp.index = obj[0];
              temp.value = obj[1];
              // push every object into the statelist array. 
              $scope.stateList.push(temp);
             
             }
             // to stop the loading indicator
             $ionicLoading.hide();
             
            });
        }
    };
    // it will be call when cancel button on the form will be clicked..
    $scope.closestateWindow = function() 
    {
        $scope.stateWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.stateWindow.remove();
    });

    // it will be called when any of the item from the state list will be selected.
    $scope.stateSelected = function(statedata)
    {
        // hide the populated view
        $scope.stateWindow.hide();
        // to show the selected state on the form.
        $scope.customer.state = statedata.value;
        // get the index of the selected state. 
        $scope.customer.stateIndex = statedata.index;
    }

    //Load the modal from the given template URL
    $ionicModal.fromTemplateUrl('shipping.html', function(shippingWindow) 
    {
        $scope.shippingWindow = shippingWindow;
    }, 
    {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    // this function will be called when user clicks on the countrylist dropdown
    $scope.openshippingWindow = function() 
    {
        $scope.shippingWindow.show(); 
    };
    // this function will be called when cancel button will be clicked.
    $scope.closeshippingWindow = function() 
    {
        // it hides the countryWindow view
        $scope.shippingWindow.hide();
    };

    //Be sure to cleanup the modal
    $scope.$on('$destroy', function() 
    {
        $scope.shippingWindow.remove();
    });

    // this will be called when any of the country will be selected ..
    $scope.shippingSelected = function(value)
    {
       // hide the populated view
        $scope.shippingWindow.hide();
        // to set the value in customer object.
        $scope.customer.isdefaultshipping = value;
        // show the selected value on the form.   
        if(value == "T")
            $scope.isdefaultshippingValue = "YES";
        else
            $scope.isdefaultshippingValue = "NO";
    }
 
    $scope.showConfirm = function() {

        $ionicPopup.confirm({
        title: 'Alert',
        content: 'Any unsaved data will be lost. Do you want to continue?'
        }).then(function(res) {
            if(res) 
            {
                $state.go('app.addressContactDetail');
            } 
            else 
            {
                console.log('You are not sure');
            }
        });
    };

    // cancel button to go to the address/ contact list page.
    $scope.goTo = function() 
    {
        $scope.showConfirm();
    }

    //  save button to save the filled data on the form ..
    $scope.saveData = function() 
    {
        if(AppFactory.getType() == "CONTACT")
        {   // for contact list
            if($scope.customer.name == null || $scope.customer.name.length == 0)
            {
                $ionicPopup.alert({
                    title: 'Alert',
                    content: 'Please provide name'
                    }).then(function(res) {

                    });
                    return;
            }
            
            if($scope.customer.phone.length > 0)
            {
                var bracketPattern = /^\(?([0-9]{3})\)?[ ]?([0-9]{3})[-]?([0-9]{4})$/;
                var dotPattern = /^([0-9]{3})?[.]?([0-9]{3})[.]?([0-9]{4})$/;
                var hyphenPattern = /^([0-9]{3})?[-]?([0-9]{3})[-]?([0-9]{4})$/;
                var spacePattern = /^([0-9]{3})?[ ]?([0-9]{3})[ ]?([0-9]{4})$/;
                var digits = /^\d{10}$/;  

                if($scope.customer.phone.match(bracketPattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(dotPattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(hyphenPattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(spacePattern))  
                {  
                    console.log("valid");
                }
                else if($scope.customer.phone.match(digits))  
                {  
                    console.log("valid");
                }    
                else  
                {  
                    $ionicPopup.alert({
                        title: 'Validation Error',
                        content: 'Please enter a valid Phone Number.<br>eg:111-111-1111<br>111 111 1111<br>111.111.1111<br>(111) 111-1111<br>1111111111'
                        }).then(function(res) {
                            
                        });
                        return;
                }
            }

            if($scope.customer.email.length > 0)
            {
                var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.[a-zA-Z]{2,3})+$/;
                //var pattern = /^([0]?[0-9]|[1][0-2])(:[0-5][0-9])?$/;

                if($scope.customer.email.match(pattern))  
                {  
                    console.log("valid");
                }
                else  
                {  
                    $ionicPopup.alert({
                        title: 'Validation Error',
                        content: 'Please enter a valid email address.'
                        }).then(function(res) {
                            
                        });
                        return;
                }
            }
        
            // to begin the loading indicator ..
            $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Updating...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

            var internalId = AppFactory.getAddressContactId();
            var jsonobj={
                        "internalid":internalId,
                        "phone":$scope.customer.phone,
                        "title":$scope.customer.jobtitle,
                        "email":$scope.customer.email,
                        "entityid":$scope.customer.name
                        };
    
            var data=jsonobj;
            var parameters = null;
    
            AppFactory.sendHttpRequest("PUT","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=102&deploy=1",data,parameters,function(responseData)  
            { 
                //alert(responseData);
                if(responseData == "\"Contact edited successfully.\"")
                {
                    window.plugins.toast.show("Record is successfully updated","short","center");
                    $state.go('app.addressContactDetail');
                }
                else if(responseData == "\"Address edited successfully.\"")
                {
                    $state.go('app.addressContactDetail');
                }
                else
                {
                    $ionicPopup.alert({
                        title: 'Alert',
                        content: responseData
                        }).then(function(res) {
                            $ionicLoading.hide();
                        });
                }
                $ionicLoading.hide();
            });
        
        }
        // when type will be address..
        else if(AppFactory.getType() == "ADDRESS")
        {
            console.log("customer obj="+ JSON.stringify($scope.customer));
            var addressId = AppFactory.getAddressContactId();

            // to begin the loading indicator ..
                $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Updating...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                });
            
            // to set the parameters to send along with the request..
                var parameters=
                {
                    "isdefaultshipping":$scope.customer.isdefaultshipping,
                    "statevalue":$scope.customer.state,
                    "zip":$scope.customer.zip,
                    "countryvalue":$scope.customer.country,
                    "addr1":$scope.customer.addr1,
                    "addr2":$scope.customer.addr2,
                    "state":$scope.customer.stateIndex,
                    "addressee":$scope.customer.addressee,
                    "internalid":$scope.customerdata.internalId,
                    "addressinternalid":addressId,
                    "personaltype":"customer",
                    "country":$scope.customer.countryIndex,
                    "city":$scope.customer.city
                } 
                // send the request to save the data.. 
                AppFactory.sendHttpRequest("put","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=122&deploy=1",parameters,null,function(responseData)  
                { 
                    if(responseData == "\"Contact edited successfully.\"")
                    {
                        $state.go('app.addressContactDetail');
                    }
                    else if(responseData == "\"Address edited successfully.\"")
                    {
                        window.plugins.toast.show("Record is successfully updated","short","center");
                        $state.go('app.addressContactDetail');
                    }
                    else
                    {
                        $ionicPopup.alert({
                            title: 'Alert',
                            content: responseData
                            }).then(function(res) {
                                $ionicLoading.hide();
                            });
                    }
                    $ionicLoading.hide();
               });
        }
    }
});
/*------------------------------------------------Edit Address Contact Controller end---------------------------------------------------*/


/*------------------------------------------------Customer-Transaction List Controller-----------------------------------------------------
    this is the common controller of page for displaying list of opportunities/quotes/order of a particular lead/prospect/customer */

app.controller('CustomerTransactionListController', function($scope, $state, $rootScope, $stateParams, $http, AppFactory, $ionicLoading, $filter)
{
    /*----------variables---------------*/
    $scope.customerData = AppFactory.getCustomerNameStatus();
    $scope.pageTitle = AppFactory.getType();
    $scope.showMessage = false;

    if($scope.pageTitle == "OPPORTUNITY")
    {
        $scope.title = "OPPORTUNITY";
    }
    else if($scope.pageTitle == "ORDER")
    {
        $scope.title = "ORDERS";
    }
    else if($scope.pageTitle == "QUOTE")
    {
        $scope.title = "QUOTES";
    }

    $scope.ListArray=[];

     /*Clear Search Box*/
     $scope.clearSearch = function () {
        $scope.searchString="";
    }

    /*----------functions---------------*/
    $scope.init = function()
    {
        $scope.firstLoad = 1;
        $scope.showMessage = false;
        console.log("showMessage:"+$scope.showMessage);
        $ionicLoading.show({
                content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showdelay: 300
            });

        if ($scope.pageTitle == "OPPORTUNITY") 
        {
             $scope.ListArray=[];
            //alert("Opportunity list");
            //console.log("ID:"+internalId);
             var parameters={
                "id":"customsearch_aavz_28",
                "customer_type":"opportunity",
                "customerid":$scope.customerData.internalId,
                "search":"",
                "searchtype":"opprtnty"                    
                            };
               
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=95&deploy=1",null,parameters,function(results) 
                {
                     //get result from json
                     //console.log("Response Data:"+JSON.stringify(results));
                    var mainArray=results;
                    for(i in mainArray)
                    {
                        var mainObj = mainArray[i]; //get single object of array
                        //console.log(JSON.stringify(mainObj));
                        for(key in mainObj){  
                        //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                        var singleObject=mainObj[key];  //create a single object by using key
                        singleObject.id=key;//Add key as attribute in singleObj
                        $scope.ListArray.push(singleObject);
                                            }
                    } //end of key forloop
                    if($scope.ListArray.length == 0)
                    {
                        $scope.showMessage = true;
                    }
                    $ionicLoading.hide();
                    //console.log("List:"+JSON.stringify($scope.ListArray));
                });

        }
        else if ($scope.pageTitle == "QUOTE") 
        {
            $scope.ListArray=[];
            //alert("Quote list");
            //console.log("ID:"+internalId);
             var parameters={
                "id":"customsearch_aavz_25",
                "customer_type":"estimate",
                "noofrecords":"100",
                "customerid":$scope.customerData.internalId 
                     
                            };
         
                //Request for get data
                AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=120&deploy=1",null,parameters,function(results) 
                {
                     //get result from json
                     //console.log("Response Data:"+JSON.stringify(results));
                    var mainArray=results;
                    for(i in mainArray)
                    {
                        var mainObj = mainArray[i]; //get single object of array
                        //console.log(JSON.stringify(mainObj));
                        for(key in mainObj){  
                        //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                        var singleObject=mainObj[key];  //create a single object by using key
                        singleObject.id=key;//Add key as attribute in singleObj
                        $scope.ListArray.push(singleObject);
                                            }
                    } //end of key forloop
                    if($scope.ListArray.length == 0)
                    {
                        $scope.showMessage = true;
                    }
                    $ionicLoading.hide();
                    //console.log("List:"+JSON.stringify($scope.ListArray));
                });

        }
        else if ($scope.pageTitle == "ORDER") 
        {
            $scope.ListArray = [];
            //alert("Order list");
            //console.log("ID:"+internalId);
            var parameters = 
            {
                "id":"customsearch_aavz_20",
                "customer_type":"salesorder",
                "customerid":$scope.customerData.internalId,
                "searchtype":"SalesOrd" ,
                "noofrecords":"100"
            };
         
            //Request for get data
            AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=111&deploy=1",null,parameters,function(results) 
            {
                 //get result from json
                 //console.log("Response Data:"+JSON.stringify(results));
                var mainArray=results;
                for(i in mainArray)
                {
                    var mainObj = mainArray[i]; //get single object of array
                    //console.log(JSON.stringify(mainObj));
                    for(key in mainObj){  
                    //console.log(JSON.stringify(mainObj[key])); //get single inner object / record
                    var singleObject=mainObj[key];  //create a single object by using key
                    singleObject.id=key;//Add key as attribute in singleObj
                    $scope.ListArray.push(singleObject);
                                        }
                } //end of key forloop
                if($scope.ListArray.length == 0)
                {
                    $scope.showMessage = true;
                }
                $ionicLoading.hide();
               // console.log("List:"+JSON.stringify($scope.ListArray));
            });
        }
    }
    $scope.init();


    //Function for setting Id of clicked item into factory
    $scope.setListItemId = function(id)
    {
        AppFactory.setItemId(id);
        if($scope.pageTitle == "OPPORTUNITY")
        {
            $state.go('app.transactionDetails');
        }

        else if($scope.pageTitle == "QUOTE")
        {
            $state.go('app.quoteDetails');
        }
        else if($scope.pageTitle == "ORDER")
        {
            $state.go('app.orderDetails');
        }
    }

    $scope.goBack = function()
    {
        $state.go('app.customerDetails');
    }
    
    $scope.$watch("searchString", function(query)
    {

        $scope.counter = $filter("filter")($scope.ListArray, query).length;
        console.log("showMessage:"+$scope.showMessage);
        console.log("Count : "+$scope.counter);
       
        if($scope.firstLoad == 1) 
        {
            $scope.firstLoad = 0;  
            $scope.showMessage = false;       
        }
        else if($scope.counter == 0)
        {     
            $scope.showMessage = true;
        } 
        else
        {
            $scope.showMessage = false;  
        }
    
    });//end of $watch()
});
/*------------------------------------------------Customer-Transaction List Controller end---------------------------------------------*/


/*-------------------------------------------------Customer Page Controller-----------------------------------------------------------
                                this is the common controller for lead/prospect/customer page*/

app.controller('CallLogsController', function($scope, AppFactory, $stateParams, $state, $ionicActionSheet, $ionicLoading, $filter, $ionicPopup, $ionicScrollDelegate, $rootScope) 
{
    console.log("call logs controller appearing");
    
    /*----------variables---------------*/
    $scope.orderString = "";                        //to store sorting value(name/status)
    $scope.newflag = "false";                       //flag to display message
    $scope.searchString = "";                       //to store search string
    $scope.listflag = 1;                            //flag to check whether searched string initially present in customer list
    $scope.callList = [];                       //to store list of customers
    $scope.counter = $scope.callList.length;    //to store length of callList    
    $scope.callList = [];                       //array to store list of customer
    $scope.pageTitle = "";                          //to store page title

    $scope.clearSearch = function () 
    {
        
        $scope.searchString="";
        cordova.plugins.Keyboard.close();
    }

    if($stateParams.category == "TOP_CUSTOMER")
    {
        $scope.pageTitle = "TOP 5 CUSTOMERS";
        $rootScope.sideMenuEnabled = false;

    }
    else
    {
        $scope.pageTitle = $stateParams.category + " LIST";    //to title of the page(lead/propect/customer)    
        $rootScope.sideMenuEnabled = true;
    }
    

    /*----------functions---------------*/

    //SHIVAJI
    /**************SEARCH FROM SERVER START******************************************/
    
    //used in server-search
    $scope.arrayLength=0;
    $scope.recordCount=0;
    $scope.messageFlag="false";
    //Code for Search data filter from server
    $scope.$watch("searchString", function(query)
    {
        $scope.counter = $filter("filter")($scope.callList, query).length;
        console.log("Count : "+$scope.counter);
       
        if($scope.listflag == 1) 
        {
            $scope.listflag = 0;  
            $scope.newflag = "false";                  
        }
        else if($scope.counter == 0 && $scope.listflag == 0)
        {    
            $scope.message="No results found.Continue search on server.";  
            $scope.newflag="true";
            $scope.messageFlag="false";             
        } 
        else if($scope.counter == 0 && $scope.listflag == 0)
        {    
            $scope.message="No results found.";  
            $scope.newflag="true";             
        }
        else
        {
            $scope.newflag="false";
            $scope.messageFlag="false";
            $ionicScrollDelegate.scrollTop();
        }

        if($scope.searchString.length == 0)
        {
            console.log("Count : "+$scope.counter+" search string:"+$scope.searchString);
            $scope.counter = 0;
        }

        if($scope.recordCount>0)
        {
            //console.log("Array length:"+$scope.arrayLength); 
            //console.log("New Record Found:"+$scope.recordCount);
            //to remove newly added element from an array       
            $scope.callList.splice($scope.arrayLength,$scope.recordCount);
        }
    
    });//end of $watch()


    //Calling web services for server search

    $scope.mySearchList = function()
    {
        $scope.recordCount=0;
        //display loading indicator
        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showdelay: 300
        });

        var parameters =
        {
            "id":"customsearch_aavz_40",
            "customer_type":"phonecall",
            "recordcount":"0",
            "search":$scope.searchString,
            "searchkey":"Subject"               
        };

        //Request for get data
        AppFactory.sendHttpRequest("get","https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=132&deploy=1",null,parameters,function(results) 
        { 
            //get result from json
            console.log("Response Data:"+JSON.stringify(results));
            if(results.length == 0 )
            {
                $scope.message = "No results found on server.";
                $scope.newflag="false";
                $scope.messageFlag="true";
            }
            else
            {
                $scope.newflag = "false";
                $scope.messageFlag="false"; 
                $scope.counter = results.length;
            }

            var mainArray = results;
            for(i in mainArray)
            {
                var mainObj = mainArray[i];                     //get single object of array
                for(key in mainObj)
                {  
                    $scope.recordCount+=1;
                    var singleObject = mainObj[key];            //create a single object by using key
                    singleObject.id = key;                      //Add key as attribute in singleObj
                    $scope.callList.push(singleObject);     //insert data into array of objects
                } //end of key forloop
            }//end of mainArray                           
            $ionicLoading.hide();
        });//end of sendHttpRequest()

    }//end of mySearchList()
    /**************SEARCH FROM SERVER END******************************************/
    $scope.allData = [];
    $scope.showMessage = false;
    //function called when page loads
    $scope.init = function()
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        $scope.showMessage1 = false;
        $scope.callList = [];
        $scope.orderString = "";

        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 300
        });

        //$stateParams is used to retrieve value from url
        AppFactory.setCategory($stateParams.category);     //set category(lead/propect/customer)

        //function in factory to get customer list
        AppFactory.getCallList("0",$scope.orderString,function(result)
        {
            console.log("result:"+JSON.stringify(result));
            $scope.allData = result;

            if($scope.allData.length == 0)
            {
                $scope.showMessage1 = true;
            }

            //parsing of the result
            for(singleData in $scope.allData)
            {
                //checking whether result has error
                if(singleData == "error")
                {   //if error occurs
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        content: "<i class='icon ion-alert-circled' style='font-size: 40px;'></i><br>Loading Failed",
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showdelay: 300
                    });

                    $timeout(function(){
                        $ionicLoading.hide();
                    },1000);
                }
                else
                {   //parsing and storing data
                    var innerData = $scope.allData[singleData];

                    for(singleRecord in innerData)
                    {
                      innerData[singleRecord].id = singleRecord;            //adding internal id of customer in innerData object
                      $scope.callList.push(innerData[singleRecord]);    //adding innerData object to callList array
                    }
                }
            }
            $scope.arrayLength=$scope.callList.length;//get callList array length used for server-search
            $ionicLoading.hide();
        });
    }
    $scope.init();

    $scope.flag = 1;    //flag to avoid repeatative call to same restlet

    //fucntion to load next records when user hovers the last 5 list items
    $scope.loadSortedData = function()   //index stores index of item
    {
        $ionicScrollDelegate.scrollTop();
        $scope.allData = [];
        $scope.showMessage = false;
        $scope.showMessage1 = false;
        $scope.callList = [];

        $ionicLoading.show({
            content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading...",
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 300
        });

        //$stateParams is used to retrieve value from url
        AppFactory.setCategory($stateParams.category);     //set category(lead/propect/customer)

        //function in factory to get customer list
        AppFactory.getCallList("0",$scope.orderString,function(result)
        {
            console.log("result:"+JSON.stringify(result));
            $scope.allData = result;

            if($scope.allData.length == 0)
            {
                $scope.showMessage1 = true;
            }

            //parsing of the result
            for(singleData in $scope.allData)
            {
                //checking whether result has error
                if(singleData == "error")
                {   //if error occurs
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        content: "<i class='icon ion-alert-circled' style='font-size: 40px;'></i><br>Loading Failed",
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showdelay: 300
                    });

                    $timeout(function(){
                        $ionicLoading.hide();
                    },1000);
                }
                else
                {   //parsing and storing data
                    var innerData = $scope.allData[singleData];

                    for(singleRecord in innerData)
                    {
                      innerData[singleRecord].id = singleRecord;            //adding internal id of customer in innerData object
                      $scope.callList.push(innerData[singleRecord]);    //adding innerData object to callList array
                    }
                }
            }
            $scope.arrayLength=$scope.callList.length;//get callList array length used for server-search
            $ionicLoading.hide();
        });
    }

    $scope.loadNextData1 = function()   //index stores index of item
    {
        console.log("length:"+$scope.allData.length);
            /*if($scope.flag==1)
            {*/
                $ionicLoading.show({
                    content: "<i class='icon ion-loading-c' style='font-size: 40px;'></i><br>Loading More...",
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showdelay: 300
                });

                var temp = $scope.callList.length;
                AppFactory.getCallList(temp,$scope.orderString,function(result)
                {                     
                    console.log("Records:"+JSON.stringify(result));
                    if(result == null || result == undefined || result == [])
                    {
                        //alert("no more data");
                    }

                    //parsing of data
                    $scope.allData = result;
                    
                    if($scope.allData.length == 0)
                    {
                        $scope.showMessage = true;
                    }

                    for(singleData in $scope.allData)
                    {
                        var innerData = $scope.allData[singleData];

                        for(singleRecord in innerData)
                        {
                            innerData[singleRecord].id = singleRecord;
                            $scope.callList.push(innerData[singleRecord]);
                        }
                    }
                    $ionicLoading.hide();
                    //console.log("Records:"+JSON.stringify($scope.callList));
                });
    }

    //function called when we click on particular customer
    $scope.goToCustomer = function(custId)     //custId contains internalId of the customer
    {
        AppFactory.setCustomerId(custId);     //setting customer internal id so that it can be used further
        $state.go('app.customerDetails');
    }

    $scope.showSortList = function() 
    {

        console.log("show function called()");

        // Show the action sheet
        $ionicActionSheet.show(
        {
            titleText: 'Sort By',
            buttons: [
                { text: 'Phone Call Date' },
                { text: 'Subject' },
                { text: 'Phone Number' }
            ],

            /* Cancel Button */
            cancelText: 'Cancel',
           
            cancel: function() 
            {
                console.log('CANCELLED');
            },

            /* Button Click Functionality */
            buttonClicked: function(index) 
            {
                if (index==0) 
                {
                    $ionicPopup.confirm({
                    title: 'Alert',
                    content: 'Do you want to sort by Phone Call Date?'
                    }).then(function(res) {
                        if(res) 
                        {
                            $scope.orderString = 'Phone Call Date';
                            $scope.loadSortedData();
                        } 
                        else 
                        {
                            console.log('You are not sure');
                        }
                    });
                };

                if (index==1) 
                {
                    $ionicPopup.confirm({
                        title: 'Alert',
                        content: 'Do you want to sort by Subject?'
                        }).then(function(res) {
                            if(res) 
                            {
                                $scope.orderString = 'Subject';
                                $scope.loadSortedData();
                            } 
                            else 
                            {
                                console.log('You are not sure');
                            }
                        });
                };

                if (index==2) 
                {
                    $ionicPopup.confirm({
                        title: 'Alert',
                        content: 'Do you want to sort by Phone Number?'
                        }).then(function(res) {
                            if(res) 
                            {
                                $scope.orderString = 'Phone Number';
                                $scope.loadSortedData();
                            } 
                            else 
                            {
                                console.log('You are not sure');
                            }
                        });
                };
            return true;
            }
        });
    };

    //to go to add customer page
    $scope.goTo = function()
    {
        $state.go('app.addCustomer');
    }

    //to back to home page from top 5 customers page
    $scope.goHome = function ()
    {
        $state.go('app.homepage');
    }
});
/*--------------------------------------------------Customer Page Controller end--------------------------------------------------------*/



/*-----------------------------------------------Custom Directive for touch event---------------------------------------*/
app.directive('ngmTouch', function () {

        return function ($scope, $element, $attrs) {
            $element.bind('touchstart', function () {
                $scope.$apply($attrs['ngmTouch']);
            });
        };
    })


function removeFocus()
{
    var searchField = document.getElementById('searchField');//.blur();
    //alert(searchField.value);
    searchField.blur();
}