var app = angular.module('FieldEdge.services', []);

app.factory('AppFactory', function($http) {

    var userName = "";
    var authorization = "";
    var authorization_without_role = "";
    var customerId = "";
    var category = "";
    var type = "";
    var add_con_id = "";
    var status = "";
    var previousCat = "";
    var customerData = {};
    var listItemId = "";
    var primaryInfoArray = [];
    var contactObject = {};
    var addressDetails = [];
    var casePageId= "";
    var caseDetail= [];
    var opportunityData={};
    var itemData=[];
    var updatedOpportunityData={};
    var editData={};
    var editOrderData={};
    var quoteData={};
    var editQuoteData={};
    var updatedQuoteData={};
    var userRole = [];
    var messageId="";
    var editMessage={};
    var orderData={};
    var updatedOrderData={};

    //timesheet
    var weekStartEndDate="";
    var dateDurationArray=[];
    var weekDayDate={};
    var hrsInternalId="";
    var emp={};
    var editTimesheetData={};
    var fromAndTodate={};

    //expense
    var expenseobj={};
    var item ={};
    var expensePageId="";
    var expenseDetail="";
    var lastObject={};


    return {
    
        /*--------------------------------------------------------for login page------------------------------------------------------------*/

        getUsername: function() //to get the username
        {
            return userName;
        },

        setUserRole: function(role)
        {
            userRole.push(role);
        },

        getUserRole: function()
        {
            return userRole;
        },

        emptyRoles: function()
        {
            userRole = [];
        },

        isValidUser: function(username, password,accountno, callback) //to check the authenticity of user
        {
            userName = username;
            authorization = "NLAuth nlauth_account="+accountno+",nlauth_email="+username+",nlauth_signature="+password;//+",nlauth_role="+role;
            authorization_without_role = "NLAuth nlauth_account="+accountno+",nlauth_email="+username+",nlauth_signature="+password;
            console.log("Authorization:"+authorization);
            $http({
                method:"get",
                url:"https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=130&deploy=1",      
                data:null,
                params:null,
                headers:{"User-Agent-x":"SuiteScript-Call", "Authorization":authorization,"Content-Type":"application/json"}
              
            }).success(callback)

            .error(callback);
        },

        /*----------------------------------------------------------------------------------------------------------------------------------*/


        /*-----------------------------------------------------for relationship pages-------------------------------------------------------*/

        getCustomerList: function(recordCount, sortString, callback) //to get the list of customers(lead/prospect/customer)
        {
            var url = "";
            var parameters = {};
            if(previousCat == "TOP_CUSTOMER")
            {
                url = "https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=93&deploy=1";
                parameters = {"id":"customsearch_aavz_15","customer_type":"customer"};
            }
            else
            {
                url = "https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=105&deploy=1";
                parameters = {"customer_type":"customer","recordcount":recordCount,"searchkey":"","id":"customsearch_aavz_16","search":"","stage":category,"sortfieldname":sortString};
            }
            $http({
            method:"GET",
            url:url,      
            data:null,
            params:parameters,
            headers:{"User-Agent-x":"SuiteScript-Call", "Authorization":authorization,"Content-Type":"application/json"}
          
            }).success(callback)
            .error(callback); //end of  http request
            //return customerList;
        },

        setCustomerNameStatus: function(data)
        {
            customerData = data;
            customerData.internalId = customerId;
        },

        getCustomerNameStatus: function()
        {
            return customerData;
        },

        setCustomerId: function(custId) //to set the customerid(lead/prospect/customer), so that it can accessed
        {
            customerId = custId;
        },

        getAddressContactId: function()
        {
            return add_con_id;
        },

        getCustomer: function(callback) //to get the customer(lead/prospect/customer) whose id is set in setCustomerId function
        {
            $http({
            method:"get",
            url:"https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=115&deploy=1",      
            data:null,
            params:{
                  "id":"customsearch_aavz_12",
                  "customer_type":"customer",
                  "internalid":customerId,
                  "noofrecord":"100",
                  "display":"5,3,8,7,6,2,4,9,1",
                  "groupname":"Primary Information,Primary Information,Primary Information,Primary Information,Primary Information,Header,Primary Information,Primary Information,Header",
                  "fieldname":"url,category,phone,comments,email,entitystatus,companyname,internalid,entityid"
                },
            headers:{"User-Agent-x":"SuiteScript-Call", "Authorization":authorization, "Content-Type":"application/json"}
          
            }).success(callback)
            .error(function(data){      
                console.log(""+JSON.stringify(data));       
            }); //end of  http request
        },

        getStatus: function() //to get the status of the customer(lead/prospect/customer) that has been selected
        {
            return status;
        },

        setAddressContactId: function(id) //to set the addressid that will be used in address_contact detail page
        {
            add_con_id = id;
        },

        setCategory: function(cat)  //set category (lead/prospect/customer) for heading of customer and customer detail page
        {
            if(cat == "TOP_CUSTOMER")
            {
                previousCat = cat;
                category = "CUSTOMER";
            }
            else if(cat == "LAST 5 QUOTES")
            {
                previousCat = cat;
                category = "QUOTE"
            }
            else if(cat == "LAST 5 ORDERS")
            {
                previousCat = cat;
                category = "ORDER"
            }
            else
            {
                previousCat="";
                category = cat;
            }
            console.log("setCategory:"+category+":"+previousCat);
        },

        getCategory: function() //get category (lead/prospect/customer) to set heading of customer and customer detail page
        {
            return category;
            console.log("setCategory:"+category+":"+previousCat);
        },

        getPreviousCategory: function() //get category (lead/prospect/customer) to set heading of customer and customer detail page
        {
            return previousCat;
            console.log("setCategory:"+category+":"+previousCat);
        },

        setType: function(value)  //set type (address/contact) for heading of address contact list and detail page
        {
            type = value;
        },

        getType: function() //get type (address/contact) to set heading of address contact list and detail page
        {
            return type;
        },

        setPrimaryInfo: function(data)
        {
            primaryInfoArray = data;
        },

        getPrimaryInfo: function()
        {
            return primaryInfoArray;
        },

        setContactObject: function(object)
        {
            contactObject = object;
        },

        getContactObject: function()
        {
            return contactObject;
        },

        setAddressDetails: function(addressData)
        {
            addressDetails = addressData;
        },

        getAddressDetails: function()
        {
            return addressDetails;
        },

        /*----------------------------------------------------------------------------------------------------------------------------------*/


        /*-----------------------------------------------------for transaction pages-------------------------------------------------------*/

        getTypeArray: function()
        {
            console.log("Transaction Type List: "+ArrayList);
            return ArrayList;
        },

        setFlag: function(value)
        {
            Flag =value; 
        },

        getFlag: function()
        {
            return Flag;
        },

        setItemId: function(value)  //set item Id when user clicked on opportunity/quote/order LIST Item
        {
            listItemId = value;
        },

        getItemId: function() //get  item Id when need on opportunity/quote/order Details Page
        {
            return listItemId;
        },

        setItemData: function(obj)  //set New item from opportunity/quote/order LIST 
        {
            var flag=0;
            for(i in obj)
            {
                flag=1;
            }
            if(flag==1)
            {
                itemData.push(obj);
            }
            else
            {
                itemData=[];
            }
        },

        getItemData: function() //get New item from opportunity/quote/order LIST 
        {
            return itemData;
        },

        setOpportunityData: function(obj)  //set New opportunity object from opportunity/quote/order LIST 
        {
            opportunityData=obj;
        },

        getOpportunityData: function() //get New opportunity object from opportunity/quote/order LIST 
        {
            return opportunityData;
        },

        setOpportunityEditData: function(obj)  //set dit opportunity object from  transaction details
        {
            editData=obj;
        },

        getOpportunityEditData: function() //get edit opportunity object to edittransactioncontroller
        {
            return editData;
        },


        setUpdatedOpportunityData: function(obj)  //set New opportunity object from opportunity updated data
        {
            updatedOpportunityData=obj;
        },

        getUpdatedOpportunityData: function() //get New opportunity object from  opportunity updated data
        {
            return updatedOpportunityData;
        },

        setQuoteData: function(obj)  //set New quoteData object 
        {
            quoteData=obj;
        },

        getQuoteData: function() //get New quoteData object 
        {
            return quoteData;
        },

        setQuoteEdit: function(obj)  //set quoteData object from QuoteDetailsController
        {
            editQuoteData=obj;
        },

        getQuoteEdit: function() //get quoteData object in EditQuoteController 
        {
            return editQuoteData;
        },

        setUpdatedQuoteData: function(obj)  //set quote final updated  object from EditQuoteController next button
        {
            updatedQuoteData=obj;
        },

        getUpdatedQuoteData: function() //get quote final updated object in summaryController
        {
            return updatedQuoteData;
        },

        setOrderData: function(obj) //set New orderData object 
        {
            orderData=obj;
        },

        getOrderData: function() //get New orderData object
        {
            return orderData;
        },
      
        setOrderEditData: function(obj)  //set dit opportunity object from  transaction details
        {
            editOrderData=obj;
        },

        getOrderEditData: function() //get edit opportunity object to edittransactioncontroller
        {
            return editOrderData;
        },

        setUpdatedOrderData: function(obj)  //set order final updated  object from EditOrderController next button
        {
            updatedOrderData=obj;
        },

        getUpdatedOrderData: function() //get order final updated object in summaryController
        {
            return updatedOrderData;
        },

        /*------------------------------------- case related functions ----------------------------*/
        
        getCaseList: function(recordCount, sortString, callback) //to get the list of customers(lead/prospect/customer)
        {
            $http({
            method:"get",
            url:"https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=94&deploy=1",      
            data:null,
            params:{"id":"customsearch_aavz_32","customer_type":"supportcase","recordcount":recordCount,"sortfieldname":sortString },
            headers:{"User-Agent-x":"SuiteScript-Call", "Authorization":authorization,"Content-Type":"application/json"}
          
            }).success(callback)
            .error(callback); //end of  http request
            
        },
        getExpenseList: function(recordCount, sortString, callback) //to get the list of customers(lead/prospect/customer)
        {
            $http({
            method:"get",
            url:"https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=104&deploy=1",      
            data:null,
            params:{"id":"customsearch_aavz_38","noofrecords":"100","customer_type":"expensereport","recordcount":recordCount, "sortfieldname":sortString},
            headers:{"User-Agent-x":"SuiteScript-Call", "Authorization":authorization,"Content-Type":"application/json"}
          
            }).success(callback)
            .error(callback); //end of  http request
            
        },
        
        setCaseId: function(caseId) //to set the customerid(lead/prospect/customer), so that it can accessed
        {
            casePageId = caseId;
        },

        getCaseId: function()
        {
            return casePageId;
        },

        setCaseDetails: function(caseDetails)
        {
            caseDetail = caseDetails;
        },

        getCaseDetails: function()
        {
            return caseDetail;
        },
     
        setMessageId: function(value)  //set item Id when user clicked on message LIST Item
        {
            messageId = value;
        },

        getMessageId: function() //get  item Id when need on message Details Page
        {
            return messageId;
        },


        setMessageEditData: function(value)  //set message details on message details page
        {
            editMessage = value;
        },

        getMessageEditData: function() //get message data on update message time
        {
            return editMessage;
        },
     
        /*----------------------------------- case related functions ends here -------------------------*/

        /*-----------------------TimeSheet--------------------------*/


        setStartEndDate: function(value)  //set start-end week date from TimeSheet page
        {
            weekStartEndDate = value;
        },

        getStartEndDate: function() //get  start-end week date on TimeSheetList page
        {
            return weekStartEndDate;
        },

        setDateDurationArray: function(array)  //set start-end week date and duration from TimesheetController 
        {
            dateDurationArray=array;
        },

        getDateDurationArray: function() //get  start-end week date and duration on TimesheetListController page
        {
            return dateDurationArray;
        },

        setFromAndTodate: function(obj)  //set fromAndTodate from TimesheetController 
        {
            fromAndTodate=obj;
        },

        getFromAndTodate: function() //get  fromAndTodate in  TimesheetListController 
        {
            return fromAndTodate;
        },


        setDayDate: function(daydate)  //set date when clicked on any week days from TimesheetList page 
        {
            weekDayDate=daydate;
        },

        getDayDate: function() //get date in TimesheetRecordController to display duration
        {
            return weekDayDate;
        },

        setInternalId: function(id)  //set internalid when clicked on any hrs in  TimesheetRecord page 
        {
            hrsInternalId=id;
        },

        getInternalId: function() //get internal id of hrs in TimesheetDetailsController 
        {
            return hrsInternalId;
        },

        setEmployeeIdValue: function(value)  //set employee id and employeevalue from  TimesheetController
        {
            emp=value;
        },

        getEmployeeIdValue: function() //get employee id and employeevalue in Add Timesheetcontroller
        {
            return emp;
        },

        setTimesheetData: function(obj)  //set header and primaryInfo array object from TimesheetDetailsController
        {
            editTimesheetData=obj;
        },

       getTimesheetData: function() //get header and primaryInfo array object in EditTimesheetController
        {
            return editTimesheetData;
        },



        /*-----------------------End TimeSheet---------------------*/

/*----------------------------------- expense related functions starts here -------------------------*/
        
          setExpenseId: function(expenseId) 
        {
            expensePageId = expenseId;
        },

        getExpenseId: function()
        {
            return expensePageId;
        },
        setExpenseDetails: function(expenseDetails)
        {
            expenseDetail = expenseDetails;
        },

        getExpenseDetails: function()
        {
            return expenseDetail;
        },
         setItem: function(item) 
        {
            expenseItem = item;
        },

        getItem: function()
        {
            return expenseItem;
        },
        setData: function(item1) 
        {
            expenseobj = item1;
        },

        getData: function()
        {
            return expenseobj;
        },
        setExpense: function(item2) 
        {
            expenseobj1 = item2;
        },

        getExpense: function()
        {
            return expenseobj1;
        },
        setLength: function(i) 
        {
            length = i;
        },

        getLength: function()
        {
            return length;
        },
        setLastObj: function(a) 
        {
            lastObject = a;
        },

        getLastObj: function()
        {
            return lastObject;
        },
        setNewItem: function(obj) 
        {
        
            var flag=0;
            for(i in obj)
            {
                flag=1;
            }
            if(flag==1)
            {
                itemData.push(obj);
            }
            else
            {
                itemData=[];
            }
        },

        getNewItem: function()
        {
            return itemData;
        },
         /*----------------------------------- expense related functions ends here ------------
         */

        getCallList: function(recordCount, sortString, callback) //to get the list of customers(lead/prospect/customer)
        {
            url = "https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=132&deploy=1";
            parameters = {"id":"customsearch_aavz_40","customer_type":"phonecall","recordcount":recordCount,"search":"","searchkey":"","sortfieldname":sortString};
            
            $http({
            method:"GET",
            url:url,      
            data:null,
            params:parameters,
            headers:{"User-Agent-x":"SuiteScript-Call", "Authorization":authorization,"Content-Type":"application/json"}
          
            }).success(callback)
            .error(callback); //end of  http request
            //return customerList;
        },

        sendHttpRequest: function(method,url,data,parameter,callback) 
        {
            var authorizationString = "";
            if(method == "get" || method == "GET")
                authorizationString = authorization;
            else
                authorizationString = authorization_without_role;
                     

            console.log("authorizationString:"+authorizationString);
              //call json web service and return callback success response
             $http({
             method:method,
             url:url,
             data:data,
             params:parameter,
             headers:
             {     
              'User-Agent-x':'SuiteScript-Call',
              'Authorization':authorization,
              'Content-Type':'application/json'
             } 
             }).success(callback)
             .error(function(data)
             {
                console.log("error:"+JSON.stringify(data));
             });  
         }      
    }
});
